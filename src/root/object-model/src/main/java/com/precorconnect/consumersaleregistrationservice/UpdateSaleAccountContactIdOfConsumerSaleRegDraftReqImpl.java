package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountContactId;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

public class UpdateSaleAccountContactIdOfConsumerSaleRegDraftReqImpl
        implements UpdateSaleAccountContactIdOfConsumerSaleRegDraftReq {

    /*
    fields
     */
    private final ConsumerSaleRegDraftId partnerSaleRegDraftId;

    private final AccountContactId saleAccountContactId;

    /*
    constructors
     */
    public UpdateSaleAccountContactIdOfConsumerSaleRegDraftReqImpl(
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId,
            @Nullable AccountContactId saleAccountContactId
    ) {

    	this.partnerSaleRegDraftId =
                 guardThat(
                         "partnerSaleRegDraftId",
                          partnerSaleRegDraftId
                 )
                         .isNotNull()
                         .thenGetValue();

        this.saleAccountContactId = saleAccountContactId;

    }

    /*
    getter methods
     */
    @Override
    public ConsumerSaleRegDraftId getConsumerSaleRegDraftId() {
        return partnerSaleRegDraftId;
    }

    public Optional<AccountContactId> getSaleAccountContactId() {
        return Optional.ofNullable(saleAccountContactId);
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UpdateSaleAccountContactIdOfConsumerSaleRegDraftReqImpl that = (UpdateSaleAccountContactIdOfConsumerSaleRegDraftReqImpl) o;

        if (!partnerSaleRegDraftId.equals(that.partnerSaleRegDraftId)) return false;
        return !(saleAccountContactId != null ? !saleAccountContactId.equals(that.saleAccountContactId) : that.saleAccountContactId != null);

    }

    @Override
    public int hashCode() {
        int result = partnerSaleRegDraftId.hashCode();
        result = 31 * result + (saleAccountContactId != null ? saleAccountContactId.hashCode() : 0);
        return result;
    }
}
