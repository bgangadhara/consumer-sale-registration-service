package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.math.BigDecimal;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AssetId;
import com.precorconnect.AssetSerialNumber;
import com.precorconnect.ProductGroupId;
import com.precorconnect.ProductGroupName;
import com.precorconnect.ProductLineId;
import com.precorconnect.ProductLineName;

public class ConsumerSaleSimpleLineItemImpl implements ConsumerSaleSimpleLineItem {
	
	 /*
    fields
     */
	private AssetId assetId;

	private AssetSerialNumber serialNumber;

	private ProductLineId productLineId;

	private BigDecimal price;

	private ProductLineName productLineName;

	private ProductGroupId productGroupId;

	private ProductGroupName productGroupName;

    /*
    constructors
     */
    public ConsumerSaleSimpleLineItemImpl(
    		@NonNull AssetId assetId,
			@NonNull AssetSerialNumber serialNumber,
			@NonNull ProductLineId productLineId,
			@Nullable BigDecimal price,
			@NonNull ProductLineName productLineName,
			@Nullable ProductGroupId productGroupId,
			@Nullable ProductGroupName productGroupName
    ) {

    	this.assetId =
                guardThat(
                        "assetId",
                        assetId
                )
                .isNotNull()
                .thenGetValue();

		this.serialNumber =
                guardThat(
                        "serialNumber",
                        serialNumber
                )
                .isNotNull()
                .thenGetValue();

		this.productLineId =
                guardThat(
                        "productLineId",
                        productLineId
                )
                .isNotNull()
                .thenGetValue();

		this.price = price;

		this.productLineName =
                guardThat(
                        "productLineName",
                        productLineName
                )
                .isNotNull()
                .thenGetValue();

		this.productGroupId = productGroupId;

		this.productGroupName = productGroupName;

    }

    @Override
	public AssetId getAssetId() {
		return assetId;
	}

	@Override
	public AssetSerialNumber getSerialNumber() {
		return serialNumber;
	}

	@Override
	public ProductLineId getProductLineId() {
		return productLineId;
	}

	@Override
	public Optional<BigDecimal> getPrice() {
		return Optional.ofNullable(price);
	}

	@Override
	public ProductLineName getProductLineName() {
		return productLineName;
	}

	@Override
	public Optional<ProductGroupId> getProductGroupId() {
		return Optional.ofNullable(productGroupId);
	}

	@Override
	public Optional<ProductGroupName> getProductGroupName() {
		return Optional.ofNullable(productGroupName);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assetId == null) ? 0 : assetId.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((productGroupId == null) ? 0 : productGroupId.hashCode());
		result = prime * result + ((productGroupName == null) ? 0 : productGroupName.hashCode());
		result = prime * result + ((productLineId == null) ? 0 : productLineId.hashCode());
		result = prime * result + ((productLineName == null) ? 0 : productLineName.hashCode());
		result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsumerSaleSimpleLineItemImpl other = (ConsumerSaleSimpleLineItemImpl) obj;
		if (assetId == null) {
			if (other.assetId != null)
				return false;
		} else if (!assetId.equals(other.assetId))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (productGroupId == null) {
			if (other.productGroupId != null)
				return false;
		} else if (!productGroupId.equals(other.productGroupId))
			return false;
		if (productGroupName == null) {
			if (other.productGroupName != null)
				return false;
		} else if (!productGroupName.equals(other.productGroupName))
			return false;
		if (productLineId == null) {
			if (other.productLineId != null)
				return false;
		} else if (!productLineId.equals(other.productLineId))
			return false;
		if (productLineName == null) {
			if (other.productLineName != null)
				return false;
		} else if (!productLineName.equals(other.productLineName))
			return false;
		if (serialNumber == null) {
			if (other.serialNumber != null)
				return false;
		} else if (!serialNumber.equals(other.serialNumber))
			return false;
		return true;
	}
	
}
