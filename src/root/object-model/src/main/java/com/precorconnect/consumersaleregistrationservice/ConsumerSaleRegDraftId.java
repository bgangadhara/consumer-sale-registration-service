package com.precorconnect.consumersaleregistrationservice;

public interface ConsumerSaleRegDraftId {

    Long getValue();

}
