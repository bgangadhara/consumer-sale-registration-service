package com.precorconnect.consumersaleregistrationservice;

import java.math.BigDecimal;
import java.util.Optional;

public interface ConsumerSaleCompositeLineItem extends ConsumerSaleLineItem {
	
	 Iterable<ConsumerSaleLineItemComponent> getComponents();

	    Optional<BigDecimal> getPrice();

}
