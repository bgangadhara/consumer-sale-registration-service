package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class PartnerSaleLineItemDtoImpl implements PartnerSaleLineItemDto  {
		
	private SerialNumber serialNumber;
		
	public PartnerSaleLineItemDtoImpl(
			@NonNull SerialNumber serialNumber
			) {                
		
		this.serialNumber =
                guardThat(
                        "serialNumber",
                        serialNumber
                )
                        .isNotNull()
                        .thenGetValue();
		
		
	}


	@Override
	public SerialNumber getSerialNumber() {
		return serialNumber;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((serialNumber == null) ? 0 : serialNumber.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartnerSaleLineItemDtoImpl other = (PartnerSaleLineItemDtoImpl) obj;
		if (serialNumber == null) {
			if (other.serialNumber != null)
				return false;
		} else if (!serialNumber.equals(other.serialNumber))
			return false;
		return true;
	}

	

}
