package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.UserId;

public class UpdateConsumerSaleRegDraftReqImpl
extends AbstractAddConsumerSaleRegDraftReq
implements UpdateConsumerSaleRegDraftReq {
	
	 ConsumerSaleRegDraftId id;
	 
	 FirstName firstName;
	    
     LastName lastName;

     ConsumerPostalAddress address;

     ConsumerPhoneNumber phoneNumber;
    
     PersonEmail emailAddress;
     
     InvoiceNumber invoiceNumber;
     
     InvoiceUrl invoiceUrl;
     
     IsSubmit isSubmit;
     
     SellDate sellDate;
     
     UserId userId;
     
     Iterable<ConsumerSaleLineItem> saleLineItems;
     
     CreateDate createDate;

	public UpdateConsumerSaleRegDraftReqImpl(
			@NonNull ConsumerSaleRegDraftId id,
			@NonNull FirstName firstName,
	        @NonNull LastName lastName,
	        @NonNull ConsumerPostalAddress address,
	        @NonNull ConsumerPhoneNumber phoneNumber,
	        @NonNull PersonEmail emailAddress,
			@NonNull AccountId partnerAccountId,
            @NonNull InvoiceNumber invoiceNumber,
            @NonNull InvoiceUrl invoiceUrl,
            @NonNull IsSubmit isSubmit,
            @NonNull SellDate sellDate,
            @NonNull UserId userId,
            @NonNull Collection<ConsumerSaleLineItem> saleLineItems,
            @NonNull CreateDate createDate
			) {
		super(partnerAccountId);
		
		 this.id =
	                guardThat(
	                        "id",
	                        id
	                )
	                        .isNotNull()
	                        .thenGetValue();
		 
		 this.firstName = 
	 				guardThat(
	 						"firstName",
	 						firstName
	 				)
	 						.isNotNull()
	 						.thenGetValue();
	    	 
	    	 this.lastName = 
	  				guardThat(
	  						"lastName",
	  						lastName
	  				)
	  						.isNotNull()
	  						.thenGetValue();

	         this.address = 
	 				guardThat(
	 						"address",
	 						address
	 				)
	 						.isNotNull()
	 						.thenGetValue();

	         this.phoneNumber = phoneNumber;
	         
	         this.emailAddress = emailAddress;
	  				
	        this.sellDate =
	                guardThat(
	                        "sellDate",
	                        sellDate
	                )
	                        .isNotNull()
	                        .thenGetValue();

	        this.invoiceNumber =
	                guardThat(
	                        "invoiceNumber",
	                        invoiceNumber
	                )
	                        .isNotNull()
	                        .thenGetValue();


	        this.saleLineItems =
	                guardThat(
	                        "saleLineItems",
	                        saleLineItems
	                )
	                        .isNotNull()
	                        .thenGetValue();


	        this.invoiceUrl = invoiceUrl;

	        this.isSubmit = isSubmit;

	        this.userId = userId;
	        
	        this.createDate =
	                guardThat(
	                        "createDate",
	                        createDate
	                )
	                        .isNotNull()
	                        .thenGetValue();
	        
	       
		
	}
	
	

	@Override
	public ConsumerSaleRegDraftId getId() {
		
		return id;
	}

	@Override
	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

	@Override
	public InvoiceUrl getInvoiceUrl() {
		return invoiceUrl;
	}

	@Override
	public IsSubmit getIsSubmitted() {
		return isSubmit;
	}

	@Override
	public SellDate getSellDate() {
		return sellDate;
	}

	@Override
	public UserId getUserId() {
		return userId;
	}

	@Override
	public Iterable<ConsumerSaleLineItem> getSaleLineItems() {
		return saleLineItems;
	}



	@Override
	public FirstName getFirstName() {
		return firstName;
	}



	@Override
	public LastName getLastName() {
		return lastName;
	}



	@Override
	public ConsumerPostalAddress getAddress() {
		return address;
	}



	@Override
	public ConsumerPhoneNumber getPhoneNumber() {
		return phoneNumber;
	}



	@Override
	public PersonEmail getPersonEmail() {
		return emailAddress;
	}



	@Override
	public CreateDate getCreateDate() {
		return createDate;
	}
/**
 * equality methods
 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime * result + ((invoiceUrl == null) ? 0 : invoiceUrl.hashCode());
		result = prime * result + ((isSubmit == null) ? 0 : isSubmit.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((saleLineItems == null) ? 0 : saleLineItems.hashCode());
		result = prime * result + ((sellDate == null) ? 0 : sellDate.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateConsumerSaleRegDraftReqImpl other = (UpdateConsumerSaleRegDraftReqImpl) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null)
				return false;
		} else if (!invoiceNumber.equals(other.invoiceNumber))
			return false;
		if (invoiceUrl == null) {
			if (other.invoiceUrl != null)
				return false;
		} else if (!invoiceUrl.equals(other.invoiceUrl))
			return false;
		if (isSubmit == null) {
			if (other.isSubmit != null)
				return false;
		} else if (!isSubmit.equals(other.isSubmit))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (saleLineItems == null) {
			if (other.saleLineItems != null)
				return false;
		} else if (!saleLineItems.equals(other.saleLineItems))
			return false;
		if (sellDate == null) {
			if (other.sellDate != null)
				return false;
		} else if (!sellDate.equals(other.sellDate))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
