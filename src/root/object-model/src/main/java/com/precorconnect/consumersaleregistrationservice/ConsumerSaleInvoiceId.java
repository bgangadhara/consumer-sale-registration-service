package com.precorconnect.consumersaleregistrationservice;

public interface ConsumerSaleInvoiceId {
	
	String getValue();

}
