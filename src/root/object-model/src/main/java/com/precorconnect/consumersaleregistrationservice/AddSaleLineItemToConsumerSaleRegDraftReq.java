package com.precorconnect.consumersaleregistrationservice;

public interface AddSaleLineItemToConsumerSaleRegDraftReq {

    ConsumerSaleLineItem getSaleLineItem();

    ConsumerSaleRegDraftId getPartnerSaleRegDraftId();

}
