package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.List;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class ProductSaleRegistrationRuleEngineRequestDtoImpl 
		implements ProductSaleRegistrationRuleEngineRequestDto {
	
	private PartnerCommericalSaleDraftId partnerSaleRegId;
	
	private SellDate sellDate;
	
	private InstallDate installDate;
	
	private SubmittedDate submittedDate;
	
	private Brand brand;
	
	private List<PartnerSaleLineItemDto> simpleLineItems;
	
	private List<PartnerCompositeSaleLineItemDto> compositeLineItems;
	
	private Amount spiffAmount;
	
	public ProductSaleRegistrationRuleEngineRequestDtoImpl(
			@Nullable PartnerCommericalSaleDraftId partnerSaleRegId,
			@NonNull SellDate sellDate,
			@Nullable InstallDate installDate, 
			@Nullable SubmittedDate submittedDate, 
			@Nullable Brand brand,
			@Nullable List<PartnerSaleLineItemDto> simpleLineItems,
			@Nullable List<PartnerCompositeSaleLineItemDto> compositeLineItems,
			@NonNull Amount spiffAmount
	) {

		this.partnerSaleRegId =partnerSaleRegId;
       

		this.sellDate =
	        guardThat(
	                "sellDate",
	                sellDate
	        )
	                .isNotNull()
	                .thenGetValue();
	
		this.installDate = installDate;
	       
		this.submittedDate = submittedDate;
	       
	
		this.brand = brand;
	       
		
		this.simpleLineItems = simpleLineItems;
		       
		
		this.compositeLineItems = compositeLineItems;
		        
		
		this.spiffAmount = spiffAmount;
		        
	}


	@Override
	public Optional<PartnerCommericalSaleDraftId> getPartnerSaleRegId() {
		return Optional.ofNullable(partnerSaleRegId);
	}

	@Override
	public SellDate getSellDate() {
		return sellDate;
	}

	@Override
	public Optional<InstallDate> getInstallDate() {
		return Optional.ofNullable(installDate);
	}

	@Override
	public Optional<SubmittedDate> getSubmittedDate() {
		return Optional.ofNullable(submittedDate);
	}

	@Override
	public Optional<Brand> getBrand() {
		return Optional.ofNullable(brand);
	}

	@Override
	public List<PartnerSaleLineItemDto> getSimpleLineItems() {
		return simpleLineItems;
	}
	
	@Override
	public List<PartnerCompositeSaleLineItemDto> getCompositeLineItems() {
		return compositeLineItems;
	}
	
	@Override
	public Optional<Amount> getSpiffAmount() {
		return Optional.ofNullable(spiffAmount);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brand == null) ? 0 : brand.hashCode());
		result = prime * result + ((installDate == null) ? 0 : installDate.hashCode());
		result = prime * result + ((simpleLineItems == null) ? 0 : simpleLineItems.hashCode());
		result = prime * result + ((compositeLineItems == null) ? 0 : compositeLineItems.hashCode());
		result = prime * result + ((partnerSaleRegId == null) ? 0 : partnerSaleRegId.hashCode());
		result = prime * result + ((sellDate == null) ? 0 : sellDate.hashCode());
		result = prime * result + ((submittedDate == null) ? 0 : submittedDate.hashCode());
		result = prime * result + ((spiffAmount == null) ? 0 : spiffAmount.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductSaleRegistrationRuleEngineRequestDtoImpl other = (ProductSaleRegistrationRuleEngineRequestDtoImpl) obj;
		if (brand == null) {
			if (other.brand != null)
				return false;
		} else if (!brand.equals(other.brand))
			return false;
		if (installDate == null) {
			if (other.installDate != null)
				return false;
		} else if (!installDate.equals(other.installDate))
			return false;
		if (simpleLineItems == null) {
			if (other.simpleLineItems != null)
				return false;
		} else if (!simpleLineItems.equals(other.simpleLineItems))
			return false;
		if (compositeLineItems == null) {
			if (other.compositeLineItems != null)
				return false;
		} else if (!compositeLineItems.equals(other.compositeLineItems))
			return false;
		if (partnerSaleRegId == null) {
			if (other.partnerSaleRegId != null)
				return false;
		} else if (!partnerSaleRegId.equals(other.partnerSaleRegId))
			return false;
		if (sellDate == null) {
			if (other.sellDate != null)
				return false;
		} else if (!sellDate.equals(other.sellDate))
			return false;
		if (submittedDate == null) {
			if (other.submittedDate != null)
				return false;
		} else if (!submittedDate.equals(other.submittedDate))
			return false;
		if (spiffAmount == null) {
			if (other.spiffAmount != null)
				return false;
		} else if (!spiffAmount.equals(other.spiffAmount))
			return false;
		return true;
	}	

}
