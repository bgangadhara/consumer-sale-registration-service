package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class PartnerRepImpl implements PartnerRep {

	 /*
    fields
     */
    private final String value;

    /*
    constructor methods
     */
    public PartnerRepImpl(
    		@NonNull String value
    		){

    	this.value =
                guardThat(
                        "PartnerRep",
                         value
                )
                        .isNotNull()
                        .thenGetValue();

    }
    
    /*
    getter methods
     */
    
	@Override
	public String getValue() {
		return value;
	}
	
	 /*
    equality methods
    */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartnerRepImpl other = (PartnerRepImpl) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	

}
