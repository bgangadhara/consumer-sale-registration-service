package com.precorconnect.consumersaleregistrationservice;

import java.time.Instant;

public interface SellDate {

	Instant getValue();
}
