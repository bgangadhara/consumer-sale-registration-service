package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.math.BigDecimal;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AssetId;
import com.precorconnect.AssetSerialNumber;
import com.precorconnect.ProductGroupId;
import com.precorconnect.ProductGroupName;
import com.precorconnect.ProductLineId;
import com.precorconnect.ProductLineName;

public class ConsumerSaleRegDraftSaleSimpleLineItemImpl
        extends AbstractConsumerSaleRegDraftSaleLineItem
        implements ConsumerSaleRegDraftSaleSimpleLineItem {

    /*
    fields
     */
    private final ConsumerSaleSimpleLineItem consumerSaleSimpleLineItem;

    /*
    constructors
     */
    public ConsumerSaleRegDraftSaleSimpleLineItemImpl(
            @Nullable ConsumerSaleRegDraftSaleLineItemId id,
            @NonNull ConsumerSaleSimpleLineItem consumerSaleSimpleLineItem
    ) {

        super(id);

        this.consumerSaleSimpleLineItem =
                guardThat(
                        "consumerSaleSimpleLineItem",
                        consumerSaleSimpleLineItem
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public AssetId getAssetId() {
        return consumerSaleSimpleLineItem.getAssetId();
    }

    @Override
    public Optional<BigDecimal> getPrice() {
        return consumerSaleSimpleLineItem.getPrice();
    }

    @Override
	public AssetSerialNumber getSerialNumber() {
		return consumerSaleSimpleLineItem.getSerialNumber();
	}

	@Override
	public ProductLineId getProductLineId() {
		return consumerSaleSimpleLineItem.getProductLineId();
	}

	@Override
	public ProductLineName getProductLineName() {
		return consumerSaleSimpleLineItem.getProductLineName();
	}

	@Override
	public Optional<ProductGroupId> getProductGroupId() {
		return consumerSaleSimpleLineItem.getProductGroupId();
	}

	@Override
	public Optional<ProductGroupName> getProductGroupName() {
		return consumerSaleSimpleLineItem.getProductGroupName();
	}
    /*
    equality methods
     */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((consumerSaleSimpleLineItem == null) ? 0 : consumerSaleSimpleLineItem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsumerSaleRegDraftSaleSimpleLineItemImpl other = (ConsumerSaleRegDraftSaleSimpleLineItemImpl) obj;
		if (consumerSaleSimpleLineItem == null) {
			if (other.consumerSaleSimpleLineItem != null)
				return false;
		} else if (!consumerSaleSimpleLineItem.equals(other.consumerSaleSimpleLineItem))
			return false;
		return true;
	}
   
	
	


}
