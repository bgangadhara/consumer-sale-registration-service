package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.PartnerSaleInvoiceId;
import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

public class UpdateSaleInvoiceIdOfConsumerSaleRegDraftReqImpl
        implements UpdateSaleInvoiceIdOfConsumerSaleRegDraftReq {

    /*
    fields
     */
    private final ConsumerSaleRegDraftId partnerSaleRegDraftId;

    private final PartnerSaleInvoiceId saleInvoiceId;

    /*
    constructors
     */
    public UpdateSaleInvoiceIdOfConsumerSaleRegDraftReqImpl(
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId,
            @NonNull PartnerSaleInvoiceId saleInvoiceId
    ) {

    	this.partnerSaleRegDraftId =
                guardThat(
                        "partnerSaleRegDraftId",
                         partnerSaleRegDraftId
                )
                        .isNotNull()
                        .thenGetValue();

        this.saleInvoiceId = saleInvoiceId;

    }

    /*
    getter methods
     */
    @Override
    public Optional<PartnerSaleInvoiceId> getSaleInvoiceId() {
        return Optional.ofNullable(saleInvoiceId);
    }

    @Override
    public ConsumerSaleRegDraftId getPartnerSaleRegDraftId() {
        return partnerSaleRegDraftId;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UpdateSaleInvoiceIdOfConsumerSaleRegDraftReqImpl that = (UpdateSaleInvoiceIdOfConsumerSaleRegDraftReqImpl) o;

        if (!partnerSaleRegDraftId.equals(that.partnerSaleRegDraftId)) return false;
        return !(saleInvoiceId != null ? !saleInvoiceId.equals(that.saleInvoiceId) : that.saleInvoiceId != null);

    }

    @Override
    public int hashCode() {
        int result = partnerSaleRegDraftId.hashCode();
        result = 31 * result + (saleInvoiceId != null ? saleInvoiceId.hashCode() : 0);
        return result;
    }
}
