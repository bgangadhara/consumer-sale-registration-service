package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;
import com.precorconnect.SapAccountNumber;

import java.util.Optional;

public interface AccountView {

    AccountId getId();

    Optional<SapAccountNumber> getSapAccountNumber();

}
