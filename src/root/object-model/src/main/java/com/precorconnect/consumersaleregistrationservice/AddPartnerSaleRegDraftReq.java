package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;

public interface AddPartnerSaleRegDraftReq {

    AccountId getPartnerAccountId();


}
