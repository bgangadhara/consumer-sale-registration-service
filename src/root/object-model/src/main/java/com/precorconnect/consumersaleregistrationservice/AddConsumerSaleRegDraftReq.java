package com.precorconnect.consumersaleregistrationservice;

import java.util.Optional;

import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.PostalAddress;
import com.precorconnect.UserId;

public interface AddConsumerSaleRegDraftReq
extends AddPartnerSaleRegDraftReq {
	
	
	FirstName getFirstName();
	
	LastName getLastName();

    ConsumerPostalAddress getAddress();

    Optional<ConsumerPhoneNumber>  getPhoneNumber();
    
    Optional<PersonEmail> getPersonEmail();
    
     InvoiceNumber getInvoiceNumber();

     Optional<InvoiceUrl> getInvoiceUrl();

     IsSubmit getIsSubmitted();

     SellDate getSellDate();

     Optional<UserId> getUserId();

     Iterable<ConsumerSaleLineItem> getSaleLineItems();

}
