package com.precorconnect.consumersaleregistrationservice;

import java.time.Instant;
import java.util.Collection;
import java.util.Optional;

import com.precorconnect.AccountId;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.UserId;

public interface ConsumerSaleRegDraftView {

    ConsumerSaleRegDraftId getId();
    
    FirstName getFirstName();
	
	LastName getLastName();

	ConsumerPostalAddress getAddress();

    Optional<ConsumerPhoneNumber> getPhoneNumber();
    
    Optional<PersonEmail> getPersonEmail();

    AccountId getPartnerAccountId();

    Instant getSellDate();

    Collection<ConsumerSaleRegDraftSaleLineItem> getSaleLineItems();

    InvoiceNumber getInvoiceNumber();

    Instant getSaleCreatedAtTimestamp();

    Optional<UserId> getSalePartnerRepId();

    IsSubmit isSubmitted();

    Optional<InvoiceUrl> getInvoiceUrl();
    
    Instant getCreateDate();

}
