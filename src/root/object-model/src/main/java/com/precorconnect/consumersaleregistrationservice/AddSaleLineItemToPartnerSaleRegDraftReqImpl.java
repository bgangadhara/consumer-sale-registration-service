package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class AddSaleLineItemToPartnerSaleRegDraftReqImpl
        implements AddSaleLineItemToConsumerSaleRegDraftReq {

    /*
    fields
     */
    private final ConsumerSaleLineItem saleLineItem;

    private final ConsumerSaleRegDraftId consumerSaleRegDraftId;

    /*
    constructors
     */
    public AddSaleLineItemToPartnerSaleRegDraftReqImpl(
            @NonNull ConsumerSaleLineItem saleLineItem,
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId
    ) {

    	this.saleLineItem =
                guardThat(
                        "saleLineItem",
                         saleLineItem
                )
                        .isNotNull()
                        .thenGetValue();

    	this.consumerSaleRegDraftId =
                guardThat(
                        "consumerSaleRegDraftId",
                        consumerSaleRegDraftId
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public ConsumerSaleLineItem getSaleLineItem() {
        return saleLineItem;
    }

    @Override
    public ConsumerSaleRegDraftId getPartnerSaleRegDraftId() {
        return consumerSaleRegDraftId;
    }
    
    /*
    equality methods
     */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((consumerSaleRegDraftId == null) ? 0 : consumerSaleRegDraftId.hashCode());
		result = prime * result + ((saleLineItem == null) ? 0 : saleLineItem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddSaleLineItemToPartnerSaleRegDraftReqImpl other = (AddSaleLineItemToPartnerSaleRegDraftReqImpl) obj;
		if (consumerSaleRegDraftId == null) {
			if (other.consumerSaleRegDraftId != null)
				return false;
		} else if (!consumerSaleRegDraftId.equals(other.consumerSaleRegDraftId))
			return false;
		if (saleLineItem == null) {
			if (other.saleLineItem != null)
				return false;
		} else if (!saleLineItem.equals(other.saleLineItem))
			return false;
		return true;
	}

    
   
}
