package com.precorconnect.consumersaleregistrationservice;


/**
 * a decorator that adds a unique identifier to
 * {@link PartnerSaleSimpleLineItem}
 * to allow external reference
 */
public interface ConsumerSaleRegDraftSaleSimpleLineItem
        extends ConsumerSaleSimpleLineItem, ConsumerSaleRegDraftSaleLineItem {

}
