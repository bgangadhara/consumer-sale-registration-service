package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class ConsumerSaleRegIdImpl implements ConsumerSaleRegId {
	

    /*
    fields
     */
    private final Long value;

    /*
    constructors
    */
    public ConsumerSaleRegIdImpl(
            @NonNull Long value
    ) throws IllegalArgumentException {

        this.value =
                guardThat(
                        "ConsumerSaleRegId",
                        value
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Long getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsumerSaleRegIdImpl other = (ConsumerSaleRegIdImpl) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
