package com.precorconnect.consumersaleregistrationservice;

public interface SpiffSerialExclusionId {

	Long getValue();
}
