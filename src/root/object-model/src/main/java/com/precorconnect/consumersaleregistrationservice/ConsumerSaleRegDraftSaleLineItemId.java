package com.precorconnect.consumersaleregistrationservice;

/**
 * represents a unique identifier for a sale line item of a partner sale registration draft
 */
public interface ConsumerSaleRegDraftSaleLineItemId {

    Long getValue();

}
