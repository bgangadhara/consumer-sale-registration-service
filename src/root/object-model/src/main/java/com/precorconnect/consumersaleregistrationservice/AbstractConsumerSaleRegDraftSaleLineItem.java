package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.Nullable;

public abstract class AbstractConsumerSaleRegDraftSaleLineItem
        implements ConsumerSaleRegDraftSaleLineItem {

    /*
    fields
     */
    private final ConsumerSaleRegDraftSaleLineItemId id;

    /*
    constructors
     */
    public AbstractConsumerSaleRegDraftSaleLineItem(
            @Nullable ConsumerSaleRegDraftSaleLineItemId id
    ) {

    	 this.id = id;

    }

    /*
    getter methods
     */
    @Override
    public ConsumerSaleRegDraftSaleLineItemId getId() {
        return id;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        AbstractConsumerSaleRegDraftSaleLineItem that = (AbstractConsumerSaleRegDraftSaleLineItem) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
