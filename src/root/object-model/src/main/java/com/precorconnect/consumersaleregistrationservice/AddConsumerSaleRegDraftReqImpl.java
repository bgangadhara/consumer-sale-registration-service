package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AccountId;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.PostalAddress;
import com.precorconnect.UserId;

public class AddConsumerSaleRegDraftReqImpl
        extends AbstractAddConsumerSaleRegDraftReq
        implements AddConsumerSaleRegDraftReq {
	
	
	 FirstName firstName;
    
     LastName lastName;

     ConsumerPostalAddress address;

     ConsumerPhoneNumber phoneNumber;
    
     PersonEmail emailAddress;
      
     InvoiceNumber invoiceNumber;
     
     InvoiceUrl invoiceUrl;
     
     IsSubmit isSubmit;
     
     SellDate sellDate;
     
     UserId userId;
     
     Iterable<ConsumerSaleLineItem> saleLineItems;

    public AddConsumerSaleRegDraftReqImpl(
			@NonNull FirstName firstName,
	        @NonNull LastName lastName,
	        @NonNull ConsumerPostalAddress address,
	        @Nullable ConsumerPhoneNumber phoneNumber,
	        @Nullable PersonEmail emailAddress,
            @NonNull AccountId partnerAccountId,
            @NonNull InvoiceNumber invoiceNumber,
            @Nullable InvoiceUrl invoiceUrl,
            @NonNull IsSubmit isSubmit,
            @NonNull SellDate sellDate,
            @Nullable UserId userId,
            @NonNull Collection<ConsumerSaleLineItem> saleLineItems
    ) {

        super(partnerAccountId);
        
        this.firstName = 
 				guardThat(
 						"firstName",
 						firstName
 				)
 						.isNotNull()
 						.thenGetValue();
    	 
    	 this.lastName = 
  				guardThat(
  						"lastName",
  						lastName
  				)
  						.isNotNull()
  						.thenGetValue();

         this.address = 
 				guardThat(
 						"address",
 						address
 				)
 						.isNotNull()
 						.thenGetValue();

         this.phoneNumber = phoneNumber; 
 				
         
         this.emailAddress = emailAddress; 
  				

        this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

        this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                        invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();


        this.saleLineItems =
                guardThat(
                        "saleLineItems",
                        saleLineItems
                )
                        .isNotNull()
                        .thenGetValue();


        this.invoiceUrl = invoiceUrl;

        this.isSubmit = isSubmit;

        this.userId = userId;

      

    }


	@Override
	public InvoiceNumber getInvoiceNumber() {

		return invoiceNumber;
	}

	@Override
	public Optional<InvoiceUrl> getInvoiceUrl() {

		return Optional.ofNullable(invoiceUrl);
	}

	@Override
	public IsSubmit getIsSubmitted() {

		return isSubmit;
	}

	
	@Override
	public SellDate getSellDate() {
		return sellDate;
	}

	@Override
	public Optional<UserId> getUserId() {

		return Optional.ofNullable(userId);
	}

	@Override
	public Iterable<ConsumerSaleLineItem> getSaleLineItems() {

		return saleLineItems;
	}


	@Override
	public FirstName getFirstName() {
		return firstName;
	}


	@Override
	public LastName getLastName() {
		return lastName;
	}


	@Override
	public ConsumerPostalAddress getAddress() {
		return address;
	}


	@Override
	public Optional<ConsumerPhoneNumber> getPhoneNumber() {
		return Optional.ofNullable(phoneNumber);
	}


	@Override
	public Optional<PersonEmail> getPersonEmail() {
		return Optional.ofNullable(emailAddress);
	}

}
