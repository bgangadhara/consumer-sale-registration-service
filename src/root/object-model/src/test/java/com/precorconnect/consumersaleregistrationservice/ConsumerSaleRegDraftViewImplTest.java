package com.precorconnect.consumersaleregistrationservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThatThrownBy;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;

import org.junit.Ignore;
import org.junit.Test;

import com.precorconnect.AccountId;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.PostalAddress;
import com.precorconnect.UserId;

public class ConsumerSaleRegDraftViewImplTest {

    /*
    fields
     */
    private final Dummy dummy =
            new Dummy();

    /*
    test methods
     */
    @Test
    public void constructor_NullId_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftViewImpl(
                        null,
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant()
                        )
        );

    }

    @Test
    public void constructor_SetsId() {

        /*
        arrange
         */
        ConsumerSaleRegDraftId expectedId =
                dummy.getConsumerSaleRegDraftId();

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                        expectedId,
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant());

        /*
        assert
         */
        ConsumerSaleRegDraftId actualId =
                objectUnderTest.getId();

        assertThat(actualId)
                .isEqualTo(expectedId);

    }

    @Test
    public void constructor_NullFirstName_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftViewImpl(
                        dummy.getConsumerSaleRegDraftId(),
                        null,
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant())
        );

    }

    @Test
    public void constructor_SetsFirstName() {

        /*
        arrange
         */
        FirstName expectedFirstName =
                dummy.getFirstName();

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                        dummy.getConsumerSaleRegDraftId(),
                        expectedFirstName,
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant());

        /*
        assert
         */
        FirstName actualFirstName =
                objectUnderTest.getFirstName();

        assertThat(actualFirstName)
                .isEqualTo(expectedFirstName);

    }

    @Test
    public void constructor_NullLastName_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftViewImpl(
                        dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        null,
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant())
        );

    }

    @Test
    public void constructor_SetsLastName() {

        /*
        arrange
         */
        LastName expectedLastName =
                dummy.getLastName();

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                        dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        expectedLastName,
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant());

        /*
        assert
         */
        LastName actualLastName =
                objectUnderTest.getLastName();

        assertThat(actualLastName)
                .isEqualTo(expectedLastName);

    }

    @Test
    public void constructor_NullSaleLineItems_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftViewImpl(
                        dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        null,
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant())
        );

    }

    @Test
    public void constructor_SetsSaleLineItems() {

        /*
        arrange
         */
        Collection<ConsumerSaleRegDraftSaleLineItem> expectedSaleLineItems =
                Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem());

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                		dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        expectedSaleLineItems,
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant());

        /*
        assert
         */
        Collection<ConsumerSaleRegDraftSaleLineItem> actualSaleLineItems =
                objectUnderTest.getSaleLineItems();

        assertThat(actualSaleLineItems)
                .isEqualTo(expectedSaleLineItems);

    }

    @Test
    public void constructor_NullPostalAddress_Throw() {

    	assertThatThrownBy(() ->
        new ConsumerSaleRegDraftViewImpl(
        		dummy.getConsumerSaleRegDraftId(),
                dummy.getFirstName(),
                dummy.getLastName(),
                null,
                dummy.getPhoneNumber(),
                dummy.getPersonEmail(),
                dummy.getAccountId(),
                Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                dummy.getInstant(),
                dummy.isSubmitted(),
                dummy.getInvoiceUrl(),
                dummy.getInvoiceNumber(),
                dummy.getInstant(),
                dummy.getUserId(),
                dummy.getInstant())
        );

    }

    @Test
    public void constructor_SetsPostalAddress() {

        /*
        arrange
         */
    	ConsumerPostalAddress expectedPostalAddress =
                dummy.getPostalAddress();

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                		dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        expectedPostalAddress,
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant());

        /*
        assert
         */
        ConsumerPostalAddress actualPostalAddress =
                objectUnderTest.getAddress();

        assertThat(actualPostalAddress)
                .isEqualTo(expectedPostalAddress);

    }

    @Test
    public void constructor_NullPhoneNumber_DoesNotThrow() {

        new ConsumerSaleRegDraftViewImpl(
        		dummy.getConsumerSaleRegDraftId(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getPostalAddress(),
                null,
                dummy.getPersonEmail(),
                dummy.getAccountId(),
                Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                dummy.getInstant(),
                dummy.isSubmitted(),
                dummy.getInvoiceUrl(),
                dummy.getInvoiceNumber(),
                dummy.getInstant(),
                dummy.getUserId(),
                dummy.getInstant());

    }

    @Test
    public void constructor_SetsPhoneNumber() {

        /*
        arrange
         */
    	ConsumerPhoneNumber expectedPhoneNumber =
                dummy.getPhoneNumber();

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                		dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        expectedPhoneNumber,
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant());

        /*
        assert
         */
        ConsumerPhoneNumber actualPhoneNumber =
                objectUnderTest.getPhoneNumber().orElse(null);
                

        assertThat(actualPhoneNumber)
                .isEqualTo(expectedPhoneNumber);

    }

    @Ignore
    public void constructor_NullSaleCreatedAtTimestamp_DoesNotThrow() {

        new ConsumerSaleRegDraftViewImpl(
                dummy.getConsumerSaleRegDraftId(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getPostalAddress(),
                dummy.getPhoneNumber(),
                dummy.getPersonEmail(),
                dummy.getAccountId(),
                Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                dummy.getInstant(),
                dummy.isSubmitted(),
                dummy.getInvoiceUrl(),
                dummy.getInvoiceNumber(),
                null,
                dummy.getUserId(),
                dummy.getInstant());

    }

    @Test
    public void constructor_SetsSaleCreatedAtTimestamp() {

        /*
        arrange
         */
        Instant expectedSaleCreatedAtTimestamp =
                dummy.getInstant();

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                		 dummy.getConsumerSaleRegDraftId(),
                         dummy.getFirstName(),
                         dummy.getLastName(),
                         dummy.getPostalAddress(),
                         dummy.getPhoneNumber(),
                         dummy.getPersonEmail(),
                         dummy.getAccountId(),
                         Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                         dummy.getInstant(),
                         dummy.isSubmitted(),
                         dummy.getInvoiceUrl(),
                         dummy.getInvoiceNumber(),
                         expectedSaleCreatedAtTimestamp,
                         dummy.getUserId(),
                         dummy.getInstant());

        /*
        assert
         */
        Instant actualSaleCreatedAtTimestamp =
                objectUnderTest.getSaleCreatedAtTimestamp();

        assertThat(actualSaleCreatedAtTimestamp)
                .isEqualTo(expectedSaleCreatedAtTimestamp);

    }

    @Test
    public void constructor_NullSalePartnerRepId_DoesNotThrow() {

        new ConsumerSaleRegDraftViewImpl(
        		 dummy.getConsumerSaleRegDraftId(),
                 dummy.getFirstName(),
                 dummy.getLastName(),
                 dummy.getPostalAddress(),
                 dummy.getPhoneNumber(),
                 dummy.getPersonEmail(),
                 dummy.getAccountId(),
                 Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                 dummy.getInstant(),
                 dummy.isSubmitted(),
                 dummy.getInvoiceUrl(),
                 dummy.getInvoiceNumber(),
                 dummy.getInstant(),
                 null,
                 dummy.getInstant());

    }

    @Test
    public void constructor_SetsSalePartnerRepId() {

        /*
        arrange
         */
        UserId expectedSalePartnerRepId =
                dummy.getUserId();

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                		dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        expectedSalePartnerRepId,
                        dummy.getInstant());

        /*
        assert
         */
        UserId actualSalePartnerRepId =
        		objectUnderTest.getSalePartnerRepId().isPresent()
                ?objectUnderTest.getSalePartnerRepId().get():null;

        assertThat(actualSalePartnerRepId)
                .isEqualTo(expectedSalePartnerRepId);

    }

    @Test
    public void constructor_NullPersonEmail_DoesNotThrow() {

        new ConsumerSaleRegDraftViewImpl(
        		dummy.getConsumerSaleRegDraftId(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getPostalAddress(),
                dummy.getPhoneNumber(),
                null,
                dummy.getAccountId(),
                Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                dummy.getInstant(),
                dummy.isSubmitted(),
                dummy.getInvoiceUrl(),
                dummy.getInvoiceNumber(),
                dummy.getInstant(),
                dummy.getUserId(),
                dummy.getInstant());

    }

    @Test
    public void constructor_SetsPersonEmail() {

        /*
        arrange
         */
        PersonEmail expectedPersonEmail =
                dummy.getPersonEmail();

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                		dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        expectedPersonEmail,
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant());

        /*
        assert
         */
        PersonEmail actualPersonEmail =
        		objectUnderTest.getPersonEmail().isPresent()
        		?objectUnderTest.getPersonEmail().get():null;

        assertThat(actualPersonEmail)
                .isEqualTo(expectedPersonEmail);

    }

    @Ignore
    public void constructor_NullSaleInvoiceId_DoesNotThrow() {

        new ConsumerSaleRegDraftViewImpl(
        		dummy.getConsumerSaleRegDraftId(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getPostalAddress(),
                dummy.getPhoneNumber(),
                dummy.getPersonEmail(),
                dummy.getAccountId(),
                Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                dummy.getInstant(),
                dummy.isSubmitted(),
                dummy.getInvoiceUrl(),
                null,
                dummy.getInstant(),
                dummy.getUserId(),
                dummy.getInstant());

    }

    @Test
    public void constructor_SetsSaleInvoiceId() {

        /*
        arrange
         */
        InvoiceNumber expectedInvoiceNumber =
                dummy.getInvoiceNumber();

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                		dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        dummy.getAccountId(),
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        expectedInvoiceNumber,
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant());

        /*
        assert
         */
        InvoiceNumber actualInvoiceNumber =
                objectUnderTest.getInvoiceNumber();

        assertThat(actualInvoiceNumber)
                .isEqualTo(expectedInvoiceNumber);

    }
    
    @Test
    public void constructor_NullPartnerAccountId_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftViewImpl(
                		dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        null,
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant())
        );

    }

    @Test
    public void constructor_SetsPartnerAccountId() {

        /*
        arrange
         */
        AccountId expectedPartnerAccountId =
                dummy.getAccountId();

        /*
        act
         */
        ConsumerSaleRegDraftView objectUnderTest =
                new ConsumerSaleRegDraftViewImpl(
                		dummy.getConsumerSaleRegDraftId(),
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        expectedPartnerAccountId,
                        Collections.singleton(dummy.getConsumerSaleRegDraftSaleLineItem()),
                        dummy.getInstant(),
                        dummy.isSubmitted(),
                        dummy.getInvoiceUrl(),
                        dummy.getInvoiceNumber(),
                        dummy.getInstant(),
                        dummy.getUserId(),
                        dummy.getInstant());

        /*
        assert
         */
        AccountId actualPartnerAccountId =
                objectUnderTest.getPartnerAccountId();

        assertThat(actualPartnerAccountId)
                .isEqualTo(expectedPartnerAccountId);

    }

}