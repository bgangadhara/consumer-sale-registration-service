package com.precorconnect.consumersaleregistrationservice;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThatThrownBy;

public class AddSaleLineItemToConsumerSaleRegDraftReqImplTest {

    /*
    fields
     */
    private final Dummy dummy =
            new Dummy();

    /*
    test methods
     */
    @Test
    public void constructor_NullSaleLineItem_Throws() {

        assertThatThrownBy(() ->
                new AddSaleLineItemToPartnerSaleRegDraftReqImpl(
                        null,
                        dummy.getConsumerSaleRegDraftId()
                )
        );

    }

    @Test
    public void constructor_SetsSaleLineItem() {

        /*
        arrange
         */
    	ConsumerSaleLineItem expectedSaleLineItem =
                dummy.getConsumerSaleLineItem();

        /*
        act
         */
        AddSaleLineItemToConsumerSaleRegDraftReq objectUnderTest =
                new AddSaleLineItemToPartnerSaleRegDraftReqImpl(
                        expectedSaleLineItem,
                        dummy.getConsumerSaleRegDraftId()
                );

        /*
        assert
         */
        ConsumerSaleLineItem actualSaleLineItem =
                objectUnderTest.getSaleLineItem();

        assertThat(actualSaleLineItem)
                .isEqualTo(expectedSaleLineItem);

    }

    @Test
    public void constructor_NullPartnerSaleRegDraftId_Throws() {

        assertThatThrownBy(() ->
                new AddSaleLineItemToPartnerSaleRegDraftReqImpl(
                        dummy.getConsumerSaleLineItem(),
                        null
                )
        );

    }

    @Test
    public void constructor_SetsPartnerSaleRegDraftId() {

        /*
        arrange
         */
        ConsumerSaleRegDraftId expectedPartnerSaleRegDraftId =
                dummy.getConsumerSaleRegDraftId();

        /*
        act
         */
        AddSaleLineItemToConsumerSaleRegDraftReq objectUnderTest =
                new AddSaleLineItemToPartnerSaleRegDraftReqImpl(
                        dummy.getConsumerSaleLineItem(),
                        expectedPartnerSaleRegDraftId
                );

        /*
        assert
         */
        ConsumerSaleRegDraftId actualConsumerSaleRegDraftId =
                objectUnderTest.getPartnerSaleRegDraftId();

        assertThat(actualConsumerSaleRegDraftId)
                .isEqualTo(expectedPartnerSaleRegDraftId);

    }

}