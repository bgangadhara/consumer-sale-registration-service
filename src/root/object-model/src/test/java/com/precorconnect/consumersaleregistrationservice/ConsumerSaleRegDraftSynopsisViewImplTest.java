package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;
import org.junit.Test;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThatThrownBy;

public class ConsumerSaleRegDraftSynopsisViewImplTest {

    /*
    fields
     */
    private final Dummy dummy =
            new Dummy();

    /*
    test methods
     */
    @Test
    public void constructor_NullId_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftSynopsisViewImpl(
                        null,
                        dummy.getAccountId()
                )
        );

    }

    @Test
    public void constructor_SetsId_Throws() {
        
        /*
        arrange
         */
        ConsumerSaleRegDraftId expectedId =
                dummy.getConsumerSaleRegDraftId();
        
        /*
        act
         */
        ConsumerSaleRegDraftSynopsisView objectUnderTest =
                new ConsumerSaleRegDraftSynopsisViewImpl(
                        expectedId,
                        dummy.getAccountId()
                );
        
        /*
        assert
         */
        ConsumerSaleRegDraftId actualId =
                objectUnderTest.getId();

        assertThat(actualId)
                .isEqualTo(expectedId);

    }

    @Test
    public void constructor_NullPartnerAccountId_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftSynopsisViewImpl(
                        dummy.getConsumerSaleRegDraftId(),
                        null
                )
        );

    }

    @Test
    public void constructor_SetsPartnerAccountId_Throws() {
        
        /*
        arrange
         */
        AccountId expectedPartnerAccountId =
                dummy.getAccountId();
        
        /*
        act
         */
        ConsumerSaleRegDraftSynopsisView objectUnderTest =
                new ConsumerSaleRegDraftSynopsisViewImpl(
                        dummy.getConsumerSaleRegDraftId(),
                        expectedPartnerAccountId
                );
        
        /*
        assert
         */
        AccountId actualPartnerAccountId =
                objectUnderTest.getPartnerAccountId();

        assertThat(actualPartnerAccountId)
                .isEqualTo(expectedPartnerAccountId);

    }

  

}