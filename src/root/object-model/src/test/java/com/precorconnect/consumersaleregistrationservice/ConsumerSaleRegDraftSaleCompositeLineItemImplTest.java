package com.precorconnect.consumersaleregistrationservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThatThrownBy;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.Ignore;
import org.junit.Test;

public class ConsumerSaleRegDraftSaleCompositeLineItemImplTest {

    /*
    fields
     */
    private final Dummy dummy =
            new Dummy();

    /*
    test methods
     */
    @Ignore
    public void constructor_NullId_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
                        null,
                        dummy.getConsumerSaleCompositeLineItem()
                )
        );

    }

    @Test
    public void constructor_SetsId() {

        /*
        arrange
         */
        ConsumerSaleRegDraftSaleLineItemId expectedId =
                dummy.getConsumerSaleRegDraftSaleLineItemId();

        /*
        act
         */
        ConsumerSaleRegDraftSaleCompositeLineItem objectUnderTest =
                new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
                        expectedId,
                        dummy.getConsumerSaleCompositeLineItem()
                );

        /*
        assert
         */
        ConsumerSaleRegDraftSaleLineItemId actualId =
                objectUnderTest.getId();

        assertThat(actualId)
                .isEqualTo(expectedId);

    }

    @Test
    public void constructor_NullConsumerSaleCompositeLineItem_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
                        dummy.getConsumerSaleRegDraftSaleLineItemId(),
                        null
                )
        );

    }

    @Test
    public void constructor_SetsPrice() {

        /*
        arrange
         */
        ConsumerSaleCompositeLineItem partnerSaleCompositeLineItem =
                dummy.getConsumerSaleCompositeLineItem();

        Optional<BigDecimal> expectedPrice =
                partnerSaleCompositeLineItem.getPrice();

        /*
        act
         */
        ConsumerSaleRegDraftSaleCompositeLineItem objectUnderTest =
                new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
                        dummy.getConsumerSaleRegDraftSaleLineItemId(),
                        partnerSaleCompositeLineItem
                );

        /*
        assert
         */
        Optional<BigDecimal> actualPrice =
                objectUnderTest.getPrice();

        assertThat(actualPrice)
                .isEqualTo(expectedPrice);

    }

    @Test
    public void constructor_SetsComponents() {

        /*
        arrange
         */
        ConsumerSaleCompositeLineItem partnerSaleCompositeLineItem =
                dummy.getConsumerSaleCompositeLineItem();

        Iterable<ConsumerSaleLineItemComponent> expectedComponents =
                partnerSaleCompositeLineItem.getComponents();

        /*
        act
         */
        ConsumerSaleRegDraftSaleCompositeLineItem objectUnderTest =
                new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
                        dummy.getConsumerSaleRegDraftSaleLineItemId(),
                        partnerSaleCompositeLineItem
                );

        /*
        assert
         */
        Iterable<ConsumerSaleLineItemComponent> actualComponents =
                objectUnderTest.getComponents();

        assertThat(actualComponents)
                .isEqualTo(expectedComponents);

    }

}