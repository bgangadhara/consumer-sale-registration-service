package com.precorconnect.consumersaleregistrationservice;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.precorconnect.AccountContactId;
import com.precorconnect.AccountContactIdImpl;
import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.AccountName;
import com.precorconnect.AccountNameImpl;
import com.precorconnect.AssetId;
import com.precorconnect.AssetIdImpl;
import com.precorconnect.AssetSerialNumber;
import com.precorconnect.AssetSerialNumberImpl;
import com.precorconnect.CityNameImpl;
import com.precorconnect.CustomerBrandName;
import com.precorconnect.CustomerBrandNameImpl;
import com.precorconnect.CustomerSourceId;
import com.precorconnect.CustomerSourceIdImpl;
import com.precorconnect.FirstName;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.Iso31661Alpha2CodeImpl;
import com.precorconnect.Iso31662CodeImpl;
import com.precorconnect.LastName;
import com.precorconnect.LastNameImpl;
import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.PartnerSaleInvoiceIdImpl;
import com.precorconnect.PostalCodeImpl;
import com.precorconnect.ProductGroupId;
import com.precorconnect.ProductGroupIdImpl;
import com.precorconnect.ProductGroupName;
import com.precorconnect.ProductGroupNameImpl;
import com.precorconnect.ProductLineId;
import com.precorconnect.ProductLineIdImpl;
import com.precorconnect.ProductLineName;
import com.precorconnect.ProductLineNameImpl;
import com.precorconnect.StreetAddressImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;


public class Dummy {

    /*
    fields
     */

	private ConsumerSaleRegDraftSaleLineItemId id=
			new ConsumerSaleRegDraftSaleLineItemIdImpl(1L);

    private final UserId userId =
            new UserIdImpl("0000000000");

    private final BigDecimal positiveBigDecimal =
            new BigDecimal(1L);

    private final AccountId accountId =
            new AccountIdImpl("000000000000000000");

    private final AccountName accountName =
            new AccountNameImpl("accName");

    private final CustomerBrandName customerBrandName =
            new CustomerBrandNameImpl("accName");

    private final AccountContactId accountContactId =
            new AccountContactIdImpl("000000000000000000");

    private final AssetId assetId =
            new AssetIdImpl("000000000000000000");

    private final InvoiceUrl invoiceUrl =
            new InvoiceUrlImpl("http://www.test.com");

    private final IsSubmit isSubmitted =
            new IsSubmitImpl(false);

    private final AssetSerialNumber serialNumber =
            new AssetSerialNumberImpl("000000000000000000");

    private final ProductLineId productLineId =
            new ProductLineIdImpl(1);

    private final ProductGroupId productGroupId =
            new ProductGroupIdImpl(1);

    private final ProductLineName productLineName =
            new ProductLineNameImpl("name");

    private final ProductGroupName productGroupName =
            new ProductGroupNameImpl("gname");

    private final BigDecimal price = new BigDecimal(1L);

    private final ConsumerSaleRegDraftId consumerSaleRegDraftId =
            new ConsumerSaleRegDraftIdImpl(1L);

    private final ConsumerSaleLineItem consumerSaleLineItem =
            new ConsumerSaleSimpleLineItemImpl(
            		assetId,
            		serialNumber,
            		productLineId,
            		price,
            		productLineName,
            		productGroupId,
            		productGroupName
            		);

    private final PartnerSaleInvoiceId partnerSaleInvoiceId =
            new PartnerSaleInvoiceIdImpl("8a8046f0-75f7-11e5-8bcf-feff819cdc9f");

    private final InvoiceNumber invoiceNumber =
            new InvoiceNumberImpl("1234345");

    private final Instant instant =
            Instant.now();

    private final BuyingGroupId buyingGroupId =
            new BuyingGroupIdImpl("1");

    private final CustomerSourceId customerSourceId =
            new CustomerSourceIdImpl(1);

    private final ManagementCompanyId managementCompanyId =
            new ManagementCompanyIdImpl("1");

    private final ConsumerSaleRegDraftSaleLineItemId consumerSaleRegDraftSaleLineItemId =
            new ConsumerSaleRegDraftSaleLineItemIdImpl(1L);

    private final ConsumerSaleSimpleLineItem consumerSaleSimpleLineItem =
            new ConsumerSaleSimpleLineItemImpl(
            		assetId, serialNumber, productLineId, price, productLineName, productGroupId,productGroupName
            );

    private final ConsumerSaleCompositeLineItem consumerSaleCompositeLineItem =
            new ConsumerSaleCompositeLineItemImpl(
                    Collections.singletonList(
                            new ConsumerSaleLineItemComponentImpl(
                            		consumerSaleRegDraftSaleLineItemId,
                            		assetId,
                                    serialNumber, productLineId, positiveBigDecimal, productLineName, productGroupId,productGroupName
                            )
                    ),
                    null
            );

    private final ConsumerSaleRegDraftSaleLineItem consumerSaleRegDraftSaleLineItem =
            new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
                    consumerSaleRegDraftSaleLineItemId,
                    consumerSaleCompositeLineItem
            );
    
    private final FirstName firstName=
    				new FirstNameImpl("firstName");
    
    private final LastName lastName=
    		        new LastNameImpl("lastName");
    
    private final ConsumerPostalAddress postalAddress =
            new ConsumerPostalAddressImpl(
                    new StreetAddressImpl("streetAddress"),
                    new AddressLineImpl("streetAddress"),
                    new CityNameImpl("cityName"),
                    new Iso31662CodeImpl("WA"),
                    new PostalCodeImpl("postalCode"),
                    new Iso31661Alpha2CodeImpl("US")
            );
    
    private final ConsumerPhoneNumber phoneNumber =
            new ConsumerPhoneNumberImpl("2222222222");
    
    private final PersonEmail personEmail=
    					new PersonEmailImpl("test123@precor.com");
    
    private final SellDate sellDate=
    		       new SellDateImpl(instant);
    
    private  List<ConsumerSaleLineItem> saleLineItems=
    											new ArrayList<ConsumerSaleLineItem>();		
    
    
    /*
    constructors
    */
    
    public Dummy() {
    	saleLineItems.add(consumerSaleLineItem);
	}
    

    /*
    getter methods
    */
	public PersonEmail getPersonEmail() {
		return personEmail;
	}

	public ConsumerSaleRegDraftSaleLineItemId getId() {
		return id;
	}

	public void setId(ConsumerSaleRegDraftSaleLineItemId id) {
		this.id = id;
	}

	public UserId getUserId() {
        return userId;
    }

    public AccountId getAccountId() {
        return accountId;
    }
    public AccountName getAccountName() {
        return accountName;
    }

    public AccountContactId getAccountContactId() {
        return accountContactId;
    }

    public ConsumerSaleRegDraftId getConsumerSaleRegDraftId() {
        return consumerSaleRegDraftId;
    }

    public ConsumerSaleLineItem getConsumerSaleLineItem() {
        return consumerSaleLineItem;
    }

    public PartnerSaleInvoiceId getPartnerSaleInvoiceId() {
        return partnerSaleInvoiceId;
    }

    public InvoiceNumber getInvoiceNumber() {
        return invoiceNumber;
    }

    public Instant getInstant() {
        return instant;
    }

    public CustomerBrandName getCustomerBrandName() {
        return customerBrandName;
    }

    public BuyingGroupId getBuyingGroupId() {
        return buyingGroupId;
    }

    public InvoiceUrl getInvoiceUrl() {
        return invoiceUrl;
    }

    public IsSubmit isSubmitted() {
        return isSubmitted;
    }

    public CustomerSourceId getCustomerSourceId() {
        return customerSourceId;
    }

    public ManagementCompanyId getManagementCompanyId() {
        return managementCompanyId;
    }

    public ConsumerSaleRegDraftSaleLineItemId getConsumerSaleRegDraftSaleLineItemId() {
        return consumerSaleRegDraftSaleLineItemId;
    }

    public ConsumerSaleSimpleLineItem getConsumerSaleSimpleLineItem() {
        return consumerSaleSimpleLineItem;
    }

    public ConsumerSaleCompositeLineItem getConsumerSaleCompositeLineItem() {
        return consumerSaleCompositeLineItem;
    }

    public ConsumerSaleRegDraftSaleLineItem getConsumerSaleRegDraftSaleLineItem() {
        return consumerSaleRegDraftSaleLineItem;
    }

	public FirstName getFirstName() {
		return firstName;
	}

	public LastName getLastName() {
		return lastName;
	}

	public ConsumerPostalAddress getPostalAddress() {
		return postalAddress;
	}

	public ConsumerPhoneNumber getPhoneNumber() {
		return phoneNumber;
	}

	public SellDate getSellDate() {
		return sellDate;
	}

	public List<ConsumerSaleLineItem> getSaleLineItems() {
		return saleLineItems;
	}

	
    
    
}
