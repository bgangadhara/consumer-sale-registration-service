package com.precorconnect.consumersaleregistrationservice.webapi;

import java.net.URI;
import java.net.URISyntaxException;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.CityNameImpl;
import com.precorconnect.FirstName;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.Iso31661Alpha2CodeImpl;
import com.precorconnect.Iso31662CodeImpl;
import com.precorconnect.LastName;
import com.precorconnect.LastNameImpl;
import com.precorconnect.PhoneNumber;
import com.precorconnect.PhoneNumberImpl;
import com.precorconnect.PostalAddress;
import com.precorconnect.PostalAddressImpl;
import com.precorconnect.PostalCodeImpl;
import com.precorconnect.SapVendorNumber;
import com.precorconnect.SapVendorNumberImpl;
import com.precorconnect.StreetAddressImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;

public class Dummy {

    /*
    fields
     */
    private AccountId accountId =
            new AccountIdImpl("111111111111111111");

    private final FirstName firstName =
            new FirstNameImpl("firstName");

    private final LastName lastName =
            new LastNameImpl("lastName");

    
    private final UserId userId =
            new UserIdImpl("userId");

    private final PhoneNumber phoneNumber =
            new PhoneNumberImpl("phoneNumber");

    private final PostalAddress postalAddress =
            new PostalAddressImpl(
            		new StreetAddressImpl("street"),
            		new CityNameImpl("city"),
            		new Iso31662CodeImpl("region"),
            		new PostalCodeImpl("postCode"),
            		new Iso31661Alpha2CodeImpl("US"));

    private final SapVendorNumber sapVendorNumber =
            new SapVendorNumberImpl("0000000000");

    private final URI uri;
    
  

    /*
    constructors
     */
    public Dummy() {
        try {

            uri = new URI("http://dev.precorconnect.com");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }
    }

    /*
    getter methods
     */

    public AccountId getAccountId() {
        return accountId;
    }

    public FirstName getFirstName() {
        return firstName;
    }

    public LastName getLastName() {
        return lastName;
    }

    

    public UserId getUserId() {
        return userId;
    }

    public SapVendorNumber getSapVendorNumber() {
        return sapVendorNumber;
    }

    public URI getUri() {
        return uri;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    
	
    
}
