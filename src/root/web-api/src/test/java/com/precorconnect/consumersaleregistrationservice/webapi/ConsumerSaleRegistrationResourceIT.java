package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({"server.port=0"})
public class ConsumerSaleRegistrationResourceIT {
	
	  /*
    fields
     */
    @Value("${local.server.port}")
    int port;

    private final Dummy dummy = new Dummy();

    private final Config config =
            new ConfigFactory()
                    .construct();

    private final Factory factory =
            new Factory(
                    dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                            config.getIdentityServiceJwtSigningKey()
                    )
            );
    
    /*
    test methods
     */
    @Before
    public void setUp() {
        RestAssured.port = port;
    }
    
    @Test
    public void isEligibleForSpiff_Test() {

        /*
        arrange
         */
    	ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto=
    			new ProductSaleRegistrationRuleEngineWebDto();
    	
    	productSaleRegistrationRuleEngineWebDto.setSellDate("8/5/2016");
    	productSaleRegistrationRuleEngineWebDto.setInstallDate("8/14/2016");
    	productSaleRegistrationRuleEngineWebDto.setFacilityBrand("Hilton");
    	
    	Collection<PartnerSaleSimpleLineItemWebDto> simpleLineItems=new ArrayList<>();
    	
    	PartnerSaleSimpleLineItemWebDto partnerSaleSimpleLineItemWebDto=
    			new PartnerSaleSimpleLineItemWebDto();
    	
    	partnerSaleSimpleLineItemWebDto.setSerialNumber("AXGT");
    	
    	simpleLineItems.add(partnerSaleSimpleLineItemWebDto);
    	
    	productSaleRegistrationRuleEngineWebDto.setSimpleLineItems(simpleLineItems);
    	
    	Collection<PartnerSaleCompositeLineItemWebDto> compositeLineItems=
    			new ArrayList<>();
    	
    	Collection<PartnerSaleSimpleLineItemWebDto> components=
    			new ArrayList<>();
    	
    	PartnerSaleSimpleLineItemWebDto firstComponent=
    			new PartnerSaleSimpleLineItemWebDto();
    	
    	firstComponent.setSerialNumber("AEZH");
    	
    	PartnerSaleSimpleLineItemWebDto secondComponent=
    			new PartnerSaleSimpleLineItemWebDto();
    	
    	secondComponent.setSerialNumber("AJTZ");
    	
    	components.add(firstComponent);
    	components.add(secondComponent);
    	
    	PartnerSaleCompositeLineItemWebDto partnerSaleCompositeLineItemWebDto=
    			new PartnerSaleCompositeLineItemWebDto();
    	
    	partnerSaleCompositeLineItemWebDto.setComponents(components);
    	
    	compositeLineItems.add(partnerSaleCompositeLineItemWebDto);
    	
    	productSaleRegistrationRuleEngineWebDto.setCompositeLineItems(compositeLineItems);
    	
        /*
        act
         */
        given()
                .contentType(ContentType.JSON)
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                factory
                                        .constructValidAppOAuth2AccessToken()
                                        .getValue()
                        )
                )
                .body(productSaleRegistrationRuleEngineWebDto)
                .post("/consumer-sale-registration/isspiffeligible")
                .then()
                .assertThat()
                .statusCode(200);

    }

}
