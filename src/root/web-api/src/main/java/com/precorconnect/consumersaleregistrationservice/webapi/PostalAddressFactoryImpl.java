package com.precorconnect.consumersaleregistrationservice.webapi;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.CityName;
import com.precorconnect.CityNameImpl;
import com.precorconnect.Iso31661Alpha2Code;
import com.precorconnect.Iso31661Alpha2CodeImpl;
import com.precorconnect.Iso31662Code;
import com.precorconnect.Iso31662CodeImpl;
import com.precorconnect.PostalCode;
import com.precorconnect.PostalCodeImpl;
import com.precorconnect.StreetAddress;
import com.precorconnect.StreetAddressImpl;
import com.precorconnect.consumersaleregistrationservice.AddressLine;
import com.precorconnect.consumersaleregistrationservice.AddressLineImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPostalAddress;
import com.precorconnect.consumersaleregistrationservice.ConsumerPostalAddressImpl;

@Singleton
@Component
class PostalAddressFactoryImpl implements
        PostalAddressFactory {

    @Override
    public PostalAddress construct(
            @NonNull ConsumerPostalAddress postalAddress
    ) {

        String addressLine1 =
                postalAddress.getAddressLine1()
                        .getValue();
        
        String addressLine2 =
                postalAddress.getAddressLine2()
                        .getValue();

        String city =
                postalAddress.getCity()
                        .getValue();

        String regionIso31662Code =
                postalAddress.getRegionIso31662Code()
                        .getValue();

        String postalCode =
                postalAddress
                        .getPostalCode()
                        .getValue();

        String countryIso31661Alpha2Code =
                postalAddress.getCountryIso31661Alpha2Code()
                        .getValue();

        return
                new PostalAddress(
                		addressLine1,
                		addressLine2,
                        city,
                        regionIso31662Code,
                        postalCode,
                        countryIso31661Alpha2Code
                );
    }

    @Override
    public ConsumerPostalAddress construct(
            @NonNull PostalAddress postalAddress
    ) {


        StreetAddress line1 =
                new StreetAddressImpl(
                        postalAddress.getAddressLine1()
                );
        
        AddressLine line2 =
                new AddressLineImpl(
                        postalAddress.getAddressLine2()
                );

        CityName cityName =
                new CityNameImpl(
                        postalAddress.getCity()
                );

        Iso31662Code regionIso31662Code =
                new Iso31662CodeImpl(
                        postalAddress.getRegionIso31662Code()
                );

        PostalCode postalCode =
                new PostalCodeImpl(
                        postalAddress.getPostalCode()
                );

        Iso31661Alpha2Code countryIso31661Alpha2Code =
                new Iso31661Alpha2CodeImpl(
                        postalAddress.getCountryIso31661Alpha2Code()
                );

        return
                new ConsumerPostalAddressImpl(
                        line1,
                        line2,
                        cityName,
                        regionIso31662Code,
                        postalCode,
                        countryIso31661Alpha2Code
                );

    }
}
