package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;

public interface CreateSpiffEntitlementsRequestFactory {

	SpiffEntitlementDto construct(
    		  @NonNull ConsumerPartnerSaleRegDraftView consumerCommercialSaleRegDraftView,
    		  @NonNull SpiffAmount spiffAmount
    );

}
