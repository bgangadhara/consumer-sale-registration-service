package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.stream.Collectors;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegistrationService;
import com.precorconnect.consumersaleregistrationservice.InvoiceUrlImpl;
import com.precorconnect.consumersaleregistrationservice.ProductSaleRegistrationRuleEngineRequestDto;
import com.precorconnect.consumersaleregistrationservice.SubmittedByNameImpl;
import com.precorconnect.consumersaleregistrationservice.UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReqImpl;


@RestController
@RequestMapping("/consumer-sale-registration")
@Api(value = "/consumer-sale-registration", description = "Operations on consumer sale registration")
public class ConsumerSaleRegistrationResource {

	 private final ConsumerSaleRegistrationService consumerSaleRegistrationService;

	 private final OAuth2AccessTokenFactory oAuth2AccessTokenFactory;
	 
	 private final AddConsumerSaleRegDraftReqFactory addConsumerSaleRegDraftReqFactory ;
	 
	 private final UpdateConsumerSaleRegDraftReqFactory updateConsumerSaleRegDraftReqFactory;

	 private final ConsumerSaleRegSynopsisViewFactory consumerSaleRegSynopsisViewFactory;
	 
	 private final SpiffRuleEngineWebRequestFactory spiffRuleEngineWebRequestFactory;
	

	@Inject
	public ConsumerSaleRegistrationResource(
			@NonNull final ConsumerSaleRegistrationService consumerSaleRegistrationService,
			@NonNull final  OAuth2AccessTokenFactory oAuth2AccessTokenFactory,
			@NonNull final AddConsumerSaleRegDraftReqFactory addConsumerSaleRegDraftReqFactory,
			@NonNull final UpdateConsumerSaleRegDraftReqFactory updateConsumerSaleRegDraftReqFactory,
			@NonNull final ConsumerSaleRegSynopsisViewFactory consumerSaleRegSynopsisViewFactory,
			@NonNull final SpiffRuleEngineWebRequestFactory spiffRuleEngineWebRequestFactory
			
	) {

		this.consumerSaleRegistrationService =
                guardThat(
                        "consumerSaleRegistrationService",
                        consumerSaleRegistrationService
                )
                        .isNotNull()
                        .thenGetValue();

		this.oAuth2AccessTokenFactory =
                guardThat(
                        "oAuth2AccessTokenFactory",
                        oAuth2AccessTokenFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.addConsumerSaleRegDraftReqFactory =
                guardThat(
                        "addConsumerSaleRegDraftReqFactory",
                        addConsumerSaleRegDraftReqFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.updateConsumerSaleRegDraftReqFactory =
                guardThat(
                        "updateConsumerSaleRegDraftReqFactory",
                        updateConsumerSaleRegDraftReqFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.consumerSaleRegSynopsisViewFactory =
                guardThat(
                        "consumerSaleRegSynopsisViewFactory",
                        consumerSaleRegSynopsisViewFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.spiffRuleEngineWebRequestFactory=
				 guardThat(
	                        "spiffRuleEngineWebRequestFactory",
	                        spiffRuleEngineWebRequestFactory
	                )
	                        .isNotNull()
	                        .thenGetValue();
	
	}


	@RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "create/add consumer-sale-registrion-draft")
	public Long addConsumerSaleRegDraft(
			@RequestBody AddConsumerSaleRegDraftReq addConsumerSaleRegDraftReq,
			 @RequestHeader("Authorization") String authorizationHeader
			) throws AuthenticationException, AuthorizationException{

		OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		ConsumerSaleRegDraftId consumerSaleRegDraftId=	consumerSaleRegistrationService.
		                    addConsumerSaleRegDraft(
		                    		addConsumerSaleRegDraftReqFactory
		                    		.construct(
		                    				addConsumerSaleRegDraftReq
		                    			),

		                    		accessToken
		                    		);

		return consumerSaleRegDraftId.getValue();

	}
	
	@RequestMapping(value="/draftId/{consumerSaleRegDraftId}",method = RequestMethod.GET)
    @ApiOperation(value = "get consumer-sale-registrion-draft")
	public ConsumerSaleRegSynopsisView getConsumerSaleRegistrationDraftWithDraftId(
			@PathVariable("consumerSaleRegDraftId") Long consumerSaleRegDraftId,
			@RequestHeader("Authorization") String authorizationHeader
			) throws AuthenticationException, AuthorizationException{

		OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		ConsumerPartnerSaleRegDraftView consumerPartnerSaleRegDraftView=	consumerSaleRegistrationService
		                           .getConsumerSaleRegDraftWithId(
		                        		   new ConsumerSaleRegDraftIdImpl(consumerSaleRegDraftId),
		                        		   accessToken
		                        		  );

		ConsumerSaleRegSynopsisView consumerSaleRegSynopsisView = 
				consumerSaleRegSynopsisViewFactory
					.construct(consumerPartnerSaleRegDraftView);
		return consumerSaleRegSynopsisView;
	
	}
	
	
	@RequestMapping(value="/accountId/{partnerAccountId}",method = RequestMethod.GET)
    @ApiOperation(value = "get consumer-sale-registrion-drafts with accountId")
	public Collection<ConsumerSaleRegSynopsisView> getConsumerSaleRegDraftWithAccountId(
			@PathVariable("partnerAccountId") String partnerAccountId,
			@RequestHeader("Authorization") String authorizationHeader
			) throws AuthenticationException, AuthorizationException{

		OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		Collection<ConsumerPartnerSaleRegDraftView> consumerPartnerSaleRegDraftViews =	
				consumerSaleRegistrationService
		                           .getConsumerSaleRegDraftWithAccountId(
		                        		   new AccountIdImpl(partnerAccountId),
		                        		   accessToken
		                        		  );
		
		Collection<ConsumerSaleRegSynopsisView> consumerSaleRegSynopsisViews
						= consumerPartnerSaleRegDraftViews
								.stream()
								.map(consumerSaleRegSynopsisViewFactory::construct)
								.collect(Collectors.toList());

		return consumerSaleRegSynopsisViews;

	}

	
	@RequestMapping(value="/updateconsumersaleregdraft/{consumerSaleRegDraftId}",method = RequestMethod.POST)
    @ApiOperation(value = "update consumer-sale-registrion-draft")
	public ConsumerSaleRegSynopsisView updateConsumerSaleRegDraft(
			@RequestBody UpdateConsumerSaleRegDraftReq updateConsumerSaleRegDraftReq,
			@PathVariable("consumerSaleRegDraftId") Long partnerSaleRegDraftId,
			 @RequestHeader("Authorization") String authorizationHeader
			) throws AuthenticationException, AuthorizationException{
		
		OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		ConsumerPartnerSaleRegDraftView	consumerPartnerSaleRegDraftView = 
				consumerSaleRegistrationService
					.updateConsumerSaleRegDraft(
							updateConsumerSaleRegDraftReqFactory
						    	.construct(updateConsumerSaleRegDraftReq),
						    new ConsumerSaleRegDraftIdImpl(partnerSaleRegDraftId),
						    accessToken
							);

		ConsumerSaleRegSynopsisView consumerSaleRegSynopsisView = 
				consumerSaleRegSynopsisViewFactory
					.construct(consumerPartnerSaleRegDraftView);

		return consumerSaleRegSynopsisView;

	}
	
	@RequestMapping(value="/deleteConsumerSaleRegDraft/{consumerSaleRegDraftId}", method=RequestMethod.DELETE)
	@ApiOperation(value = "delete consumer sale registrion draft with Id ")
	public String deleteConsumerSaleRegDraftWithId(
			@PathVariable("consumerSaleRegDraftId") Long consumerSaleRegDraftId,
			@RequestHeader("Authorization") String authorizationHeader
			) throws AuthenticationException, AuthorizationException {
		
		OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		return consumerSaleRegistrationService
		                           .deleteConsumerSaleRegDraftWithId(
		                        		   new ConsumerSaleRegDraftIdImpl(consumerSaleRegDraftId),
		                        		   accessToken
		                        		  );
		
	}
	
	@RequestMapping(value="{consumerSaleRegDraftId}/updateInvoiceUrl",method = RequestMethod.POST)
    @ApiOperation(value = "update invoice url of consumers sale registration draft")
	public String updateSaleInvoiceUrlOfConsumerSaleRegDraft(
			@RequestBody String invoiceUrl,
			@PathVariable("consumerSaleRegDraftId") Long consumerSaleRegDraftId,
			@RequestHeader("Authorization") String authorizationHeader
			) throws AuthenticationException, AuthorizationException{

		OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		consumerSaleRegistrationService
		                .updateSaleInvoiceUrlOfConsumerSaleRegDraft(
		                		new UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReqImpl(
		                				new ConsumerSaleRegDraftIdImpl(consumerSaleRegDraftId),
		                				new InvoiceUrlImpl(invoiceUrl)
		                		),
		                		accessToken
		                		);

		return "Invoice Url updated successfully";

	}

	
	@RequestMapping(value="/removeSaleLineItem/{consumerSaleRegDraftSaleLineItemId}",method = RequestMethod.DELETE)
    @ApiOperation(value = "remove consumer sale line item")
	 public String removeSaleLineItemFromPartnerSaleRegDraft(	            
	            @PathVariable("consumerSaleRegDraftSaleLineItemId") Long consumerSaleRegDraftSaleLineItemId,
				@RequestHeader("Authorization") String authorizationHeader
	    ) throws AuthenticationException, AuthorizationException{
		
		OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		consumerSaleRegistrationService
			.removeSaleLineItemFromConsumerSaleRegDraft(
					new ConsumerSaleRegDraftSaleLineItemIdImpl(
							consumerSaleRegDraftSaleLineItemId
							),
					accessToken
					);
		
		return "partner sale line item removed successfully";
		 
	 }
	
	@RequestMapping(value="/submitconsumersaleregdraft/{consumerSaleRegDraftId}",method = RequestMethod.POST)
    @ApiOperation(value = "submit partner-sale-registrion-draft")
	public ConsumerSaleRegSynopsisView submitConsumerSaleRegDraft(
			@PathVariable("consumerSaleRegDraftId") Long consumerSaleRegDraftId,
			@RequestBody String submittedByName,
			@RequestHeader("Authorization") String authorizationHeader
			) throws AuthenticationException, AuthorizationException{

		OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		ConsumerPartnerSaleRegDraftView consumerPartnerSaleRegDraftView=
				consumerSaleRegistrationService
					.submitConsumerSaleRegDraft(
							new ConsumerSaleRegDraftIdImpl(consumerSaleRegDraftId),
							new SubmittedByNameImpl(submittedByName),
		                    accessToken
		                    );

		return
				consumerSaleRegSynopsisViewFactory
							.construct(
									consumerPartnerSaleRegDraftView
									);

	}

	@RequestMapping(value="/isspiffeligible",method = RequestMethod.POST)
    @ApiOperation(value = "is given products  eliglible for spiff")
	public boolean isEligibleForSpiff(
			@RequestBody ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto,
			@RequestHeader("Authorization") String authorizationHeader
			) throws AuthenticationException, AuthorizationException{

		OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

		ProductSaleRegistrationRuleEngineRequestDto productSaleRegistrationRuleEngineRequestDto = 
				spiffRuleEngineWebRequestFactory
					.construct(
							productSaleRegistrationRuleEngineWebDto
							);
				
		return 
				consumerSaleRegistrationService
					.isEligibleForSpiff(
							productSaleRegistrationRuleEngineRequestDto,
							accessToken
							);

	}

}
