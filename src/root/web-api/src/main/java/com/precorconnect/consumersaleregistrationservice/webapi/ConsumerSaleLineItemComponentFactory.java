package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

interface ConsumerSaleLineItemComponentFactory {

    ConsumerSaleLineItemComponent construct(
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleLineItemComponent partnerSaleLineItemComponent
    );

    	com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponent construct(
            @NonNull ConsumerSaleLineItemComponent partnerSaleLineItemComponent
    );

}
