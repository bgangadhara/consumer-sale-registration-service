package com.precorconnect.consumersaleregistrationservice.webapi;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.AssetIdImpl;
import com.precorconnect.AssetSerialNumberImpl;
import com.precorconnect.ProductGroupIdImpl;
import com.precorconnect.ProductGroupNameImpl;
import com.precorconnect.ProductLineIdImpl;
import com.precorconnect.ProductLineNameImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleCompositeLineItemImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItemImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;

@Singleton
@Component
public class ConsumerSaleRegDraftSaleCompositeLineItemFactoryImpl
		implements ConsumerSaleRegDraftSaleCompositeLineItemFactory {

	@Override
    public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem construct(
            @NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
    ) {

		return
				new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
						new ConsumerSaleRegDraftSaleLineItemIdImpl(null),
						new ConsumerSaleCompositeLineItemImpl(
							consumerSaleRegDraftSaleCompositeLineItem.getComponents()
							.stream()
							.map(component -> new com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponentImpl(
													new ConsumerSaleRegDraftSaleLineItemIdImpl(component.getId().orElse(null)),
													new AssetIdImpl(component.getAssetId()),
													new AssetSerialNumberImpl(component.getSerialNumber()),
													new ProductLineIdImpl(component.getProductLineId()),
													component.getPrice().orElse(null),
													new ProductLineNameImpl(component.getProductLineName()),
													component.getProductGroupId().isPresent()
													?new ProductGroupIdImpl(component.getProductGroupId().get())
													:null,
													component.getProductGroupName().isPresent()
													?new ProductGroupNameImpl(component.getProductGroupName().get())
													:null
													)
							).collect(Collectors.toList()),
							consumerSaleRegDraftSaleCompositeLineItem.getPrice().orElse(null)
						)
				);



    }

	@Override
	public ConsumerSaleRegDraftSaleCompositeLineItem construct(
			com.precorconnect.consumersaleregistrationservice.
			@NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
    ){

		Long id
				= consumerSaleRegDraftSaleCompositeLineItem.getId().getValue();
		List<ConsumerSaleLineItemComponent> components =
												StreamSupport
												.stream(
														consumerSaleRegDraftSaleCompositeLineItem.getComponents()
												                .spliterator(),
												        false)
												.map(component -> new ConsumerSaleLineItemComponent(
														component.getId().getValue(),
														component.getAssetId().getValue(),
														component.getPrice().orElse(null),
														component.getSerialNumber().getValue(),
														component.getProductLineId().getValue(),
														component.getProductLineName().getValue(),
														component.getProductGroupId().isPresent()
														?component.getProductGroupId().get().getValue()
														:null,
														component.getProductGroupName().isPresent()
														?component.getProductGroupName().get().getValue()
														:null
														)
												).collect(Collectors.toList());
		BigDecimal price = consumerSaleRegDraftSaleCompositeLineItem.getPrice().orElse(null);

		return
				new ConsumerSaleRegDraftSaleCompositeLineItem(id, components, price);
	}

	@Override
	public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem construct(
			@NonNull UpdateConsumerSaleRegDraftSaleCompositeLineItem updateConsumerSaleRegDraftSaleCompositeLineItem) {



			return
					new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
							new ConsumerSaleRegDraftSaleLineItemIdImpl(updateConsumerSaleRegDraftSaleCompositeLineItem.getId()),
							new ConsumerSaleCompositeLineItemImpl(
									updateConsumerSaleRegDraftSaleCompositeLineItem.getComponents()
								.stream()
								.map(component -> new com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponentImpl(
														new ConsumerSaleRegDraftSaleLineItemIdImpl(component.getId().orElse(null)),
														new AssetIdImpl(component.getAssetId()),
														new AssetSerialNumberImpl(component.getSerialNumber()),
														new ProductLineIdImpl(component.getProductLineId()),
										                component.getPrice().orElse(null),
														new ProductLineNameImpl(component.getProductLineName()),
														component.getProductGroupId().isPresent()
														?new ProductGroupIdImpl(component.getProductGroupId().get())
														:null,
														component.getProductGroupName().isPresent()
														?new ProductGroupNameImpl(component.getProductGroupName().get())
														:null)
								).collect(Collectors.toList()),
								updateConsumerSaleRegDraftSaleCompositeLineItem.getPrice()
							)
					);


	}

	@Override
	public UpdateConsumerSaleRegDraftSaleCompositeLineItem constructUpdateSaleRegDraftObj(
			com.precorconnect.consumersaleregistrationservice.@NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
			) {

		Long id = consumerSaleRegDraftSaleCompositeLineItem.getId().getValue();

		List<ConsumerSaleLineItemComponent> components =
										StreamSupport
										.stream(
												consumerSaleRegDraftSaleCompositeLineItem.getComponents()
										                .spliterator(),
										        false)
										.map(component -> new ConsumerSaleLineItemComponent(
												component.getId().getValue(),
												component.getAssetId().getValue(),
												component.getPrice().orElse(null),
												component.getSerialNumber().getValue(),
												component.getProductLineId().getValue(),
												component.getProductLineName().getValue(),
												component.getProductGroupId().isPresent()
												?component.getProductGroupId().get().getValue()
												:null,
												component.getProductGroupName().isPresent()
												?component.getProductGroupName().get().getValue()
												:null
												)
										).collect(Collectors.toList());

		BigDecimal price = consumerSaleRegDraftSaleCompositeLineItem.getPrice().orElse(null);

return
		new UpdateConsumerSaleRegDraftSaleCompositeLineItem(id, components, price);
	}


}
