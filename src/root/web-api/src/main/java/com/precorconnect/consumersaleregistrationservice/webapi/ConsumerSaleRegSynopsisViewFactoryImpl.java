package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;

@Singleton
@Component
public class ConsumerSaleRegSynopsisViewFactoryImpl
		implements ConsumerSaleRegSynopsisViewFactory {

	private final ConsumerSaleRegDraftSaleSimpleLineItemFactory consumerSaleRegDraftSaleSimpleLineItemFactory;

	private final ConsumerSaleRegDraftSaleCompositeLineItemFactory consumerSaleRegDraftSaleCompositeLineItemFactory;
	
	 private final PostalAddressFactory postalAddressFactory;
	 
	 
	@Inject
	public ConsumerSaleRegSynopsisViewFactoryImpl(
			@NonNull PostalAddressFactory postalAddressFactory,
			@NonNull ConsumerSaleRegDraftSaleSimpleLineItemFactory consumerSaleRegDraftSaleSimpleLineItemFactory,
			@NonNull ConsumerSaleRegDraftSaleCompositeLineItemFactory consumerSaleRegDraftSaleCompositeLineItemFactory
			) {

		this.postalAddressFactory = 
					guardThat(
							"postalAddressFactory",
							postalAddressFactory
					)
							.isNotNull()
							.thenGetValue();
		
		this.consumerSaleRegDraftSaleSimpleLineItemFactory = 
				guardThat(
						"consumerSaleRegDraftSaleSimpleLineItemFactory",
						consumerSaleRegDraftSaleSimpleLineItemFactory
				)
						.isNotNull()
						.thenGetValue();
		
		this.consumerSaleRegDraftSaleCompositeLineItemFactory = 
				guardThat(
						"consumerSaleRegDraftSaleCompositeLineItemFactory",
						consumerSaleRegDraftSaleCompositeLineItemFactory
				)
						.isNotNull()
						.thenGetValue();
		
	}



	@Override
    public ConsumerSaleRegSynopsisView construct(
            @NonNull ConsumerPartnerSaleRegDraftView consumerPartnerSaleRegDraftView
    ) {

		Long id =
				consumerPartnerSaleRegDraftView
				.getId()
				.getValue();

		
		String firstName=
						consumerPartnerSaleRegDraftView
						.getFirstName()
						.getValue();
		
		String lastName=	
							consumerPartnerSaleRegDraftView
							.getLastName()
							.getValue();
		
		 PostalAddress address =
		                postalAddressFactory
                        .construct(
                        		consumerPartnerSaleRegDraftView
                        		.getAddress()
                        		);
		 
		 String phoneNumber=
				 consumerPartnerSaleRegDraftView
				 .getPhoneNumber()
				 .isPresent()
				 ? consumerPartnerSaleRegDraftView
	                        .getPhoneNumber()
	                        .get()
	                        .getValue()
	                : null;
		 
		 
		 String personEmail=
				 consumerPartnerSaleRegDraftView
				 .getPersonEmail()
				 .isPresent()
				 ? consumerPartnerSaleRegDraftView
	                        .getPersonEmail()
	                        .get()
	                        .getValue()
	                : null;
		

		 String partnerAccountId =
						 consumerPartnerSaleRegDraftView
						 				.getPartnerAccountId()
						 				.getValue();

	     String invoiceNumber =
	    				 consumerPartnerSaleRegDraftView
	    				 	.getInvoiceNumber()
	    				 	.getValue();

	     String invoiceUrl =
	    		 consumerPartnerSaleRegDraftView
	    				 	.getInvoiceUrl()
	    				 	.isPresent()
                ? consumerPartnerSaleRegDraftView
                        .getInvoiceUrl()
                        .get()
                        .getValue()
                : null;

	     boolean isSubmitted =
	    				 consumerPartnerSaleRegDraftView
	    				 	.isSubmitted()
	    				 	.getValue();


	      String  sellDate =
							 new SimpleDateFormat("MM/dd/yyyy").format(
									 Date.from(consumerPartnerSaleRegDraftView
									 .getSellDate())
							 	);
	      
	      String createDate= null;
	      
	      if( consumerPartnerSaleRegDraftView.getCreateDate()!=null){
	    	  
	    	  createDate=	 new SimpleDateFormat("MM/dd/yyyy").format(
							 Date.from(
									 consumerPartnerSaleRegDraftView
						    		 	.getCreateDate()
						    		 )
					 	);
	      }
	      
	      long actualEpochMilli =
	    		  consumerPartnerSaleRegDraftView
			 		.getSaleCreatedAtTimestamp().toEpochMilli();

	        long jdbcConversionCompensatedEpochMilli =
	                actualEpochMilli - Calendar.getInstance().getTimeZone().getRawOffset();

	        String  draftedDate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(
	        																	new Timestamp(
	        																			jdbcConversionCompensatedEpochMilli
	        																			)
	        																	);

	     String partnerRepUserId =
	    		 consumerPartnerSaleRegDraftView
	    				 	.getSalePartnerRepId()
	    				 	.isPresent()
                ? consumerPartnerSaleRegDraftView
                        .getSalePartnerRepId()
                        .get()
                        .getValue()
                : null;

	     List<ConsumerSaleRegDraftSaleSimpleLineItem> simpleLineItems = new ArrayList<>();

	     List<ConsumerSaleRegDraftSaleCompositeLineItem>  compositeLineItems = new ArrayList<>();

	     for(com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem    lineItem:  consumerPartnerSaleRegDraftView.getSaleLineItems()){

	    	 if(lineItem instanceof com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem){
	    		 simpleLineItems.add(

	    				 consumerSaleRegDraftSaleSimpleLineItemFactory
							.construct(
									(com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem)lineItem
							)
	    				 );
	    	 }else if(lineItem instanceof com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem){
	    		 compositeLineItems.add(

	    				 consumerSaleRegDraftSaleCompositeLineItemFactory
    				 	.construct(
    				 			(com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem)lineItem
    				 	)
    				 );

	    	 }

	     }
	     
	    
	    		

	     return
	    		 new ConsumerSaleRegSynopsisView(
	    				 id,
	    				 firstName,
	    				 lastName,
	    				 address,
	    				 phoneNumber,
	    				 personEmail,
	    				 sellDate,
	    				 draftedDate,
	    				 invoiceNumber,
	    				 partnerRepUserId,
	    				 partnerAccountId,
	    				 invoiceUrl,
	    				 isSubmitted,
	    				 simpleLineItems,
	    				 compositeLineItems,
	    				 createDate
	    		);

    }

}
