package com.precorconnect.consumersaleregistrationservice.webapi;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AssetIdImpl;
import com.precorconnect.AssetSerialNumberImpl;
import com.precorconnect.ProductGroupIdImpl;
import com.precorconnect.ProductGroupNameImpl;
import com.precorconnect.ProductLineIdImpl;
import com.precorconnect.ProductLineNameImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponentImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;

@Singleton
class ConsumerSaleLineItemComponentFactoryImpl
        implements ConsumerSaleLineItemComponentFactory {

    @Override
    public ConsumerSaleLineItemComponent construct(
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleLineItemComponent partnerSaleLineItemComponent
    ) {

        ConsumerSaleLineItemComponent objectUnderConstruction =
                new ConsumerSaleLineItemComponent(
                		partnerSaleLineItemComponent
                			.getId().getValue(),
                		partnerSaleLineItemComponent
                        	.getAssetId().getValue(),
                        partnerSaleLineItemComponent
                        	.getPrice().orElse(null),
                        partnerSaleLineItemComponent
                        	.getSerialNumber().getValue(),
                        partnerSaleLineItemComponent
                        	.getProductLineId().getValue(),
                        partnerSaleLineItemComponent
                         	.getProductLineName().getValue(),
                        partnerSaleLineItemComponent
                         	.getProductGroupId().isPresent()
                         	?partnerSaleLineItemComponent.getProductGroupId().get().getValue()
                         	:null,
                        partnerSaleLineItemComponent
                          	.getProductGroupName().isPresent()
                         	?partnerSaleLineItemComponent.getProductGroupName().get().getValue()
                         	:null
                		);


        return objectUnderConstruction;

    }

    @Override
    public com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponent construct(
            @NonNull ConsumerSaleLineItemComponent consumerSaleLineItemComponent
    ) {

        return
                new ConsumerSaleLineItemComponentImpl(
                		
                		
                		new ConsumerSaleRegDraftSaleLineItemIdImpl(
                		consumerSaleLineItemComponent.getId().get().longValue()
                		),
                		new AssetIdImpl(
                                consumerSaleLineItemComponent
                                        .getAssetId()
                        ),
                		new AssetSerialNumberImpl(
                                consumerSaleLineItemComponent
                                .getSerialNumber()
                                ),
                		new ProductLineIdImpl(
                                consumerSaleLineItemComponent
                                .getProductLineId()
                                ),
                		consumerSaleLineItemComponent
                        .getPrice()
                        .orElse(null),
                        
                        new ProductLineNameImpl(
                                consumerSaleLineItemComponent
                                .getProductLineName()),
                        new ProductGroupIdImpl(
                                consumerSaleLineItemComponent
                                .getProductGroupId().orElse(null)),
                        new ProductGroupNameImpl(
                                consumerSaleLineItemComponent
                                .getProductGroupName().orElse(null))
                                
                );

    }

}
