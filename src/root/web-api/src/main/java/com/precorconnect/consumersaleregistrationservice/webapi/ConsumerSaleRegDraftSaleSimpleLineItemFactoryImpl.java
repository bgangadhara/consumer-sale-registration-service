package com.precorconnect.consumersaleregistrationservice.webapi;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.AssetIdImpl;
import com.precorconnect.AssetSerialNumberImpl;
import com.precorconnect.ProductGroupIdImpl;
import com.precorconnect.ProductGroupNameImpl;
import com.precorconnect.ProductLineIdImpl;
import com.precorconnect.ProductLineNameImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleSimpleLineItemImpl;

@Singleton
@Component
public class ConsumerSaleRegDraftSaleSimpleLineItemFactoryImpl
		implements ConsumerSaleRegDraftSaleSimpleLineItemFactory {

	@Override
    public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem construct(
            @NonNull ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem
    ) {
		return
				new com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItemImpl(
						new ConsumerSaleRegDraftSaleLineItemIdImpl(null),
						new ConsumerSaleSimpleLineItemImpl(
							new AssetIdImpl(consumerSaleRegDraftSaleSimpleLineItem.getAssetId()),
							new AssetSerialNumberImpl(consumerSaleRegDraftSaleSimpleLineItem.getSerialNumber()),
							new ProductLineIdImpl(consumerSaleRegDraftSaleSimpleLineItem.getProductLineId()),
							consumerSaleRegDraftSaleSimpleLineItem.getPrice().orElse(null),
							new ProductLineNameImpl(consumerSaleRegDraftSaleSimpleLineItem.getProductLineName()),
							consumerSaleRegDraftSaleSimpleLineItem.getProductGroupId().isPresent()
							?new ProductGroupIdImpl(consumerSaleRegDraftSaleSimpleLineItem.getProductGroupId().get())
							:null,
							consumerSaleRegDraftSaleSimpleLineItem.getProductGroupName().isPresent()
							?new ProductGroupNameImpl(consumerSaleRegDraftSaleSimpleLineItem.getProductGroupName().get())
							:null
						)
					);

    }

	@Override
	public ConsumerSaleRegDraftSaleSimpleLineItem construct(
			com.precorconnect.consumersaleregistrationservice.
			@NonNull ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem
	) {

		return
				new ConsumerSaleRegDraftSaleSimpleLineItem(
						consumerSaleRegDraftSaleSimpleLineItem.getId().getValue(),
						consumerSaleRegDraftSaleSimpleLineItem.getAssetId().getValue(),
						consumerSaleRegDraftSaleSimpleLineItem.getSerialNumber().getValue(),
						consumerSaleRegDraftSaleSimpleLineItem.getProductLineId().getValue(),
						consumerSaleRegDraftSaleSimpleLineItem.getPrice().orElse(null),
						consumerSaleRegDraftSaleSimpleLineItem.getProductLineName().getValue(),
						consumerSaleRegDraftSaleSimpleLineItem.getProductGroupId().isPresent()
							?consumerSaleRegDraftSaleSimpleLineItem.getProductGroupId().get().getValue():null,
						consumerSaleRegDraftSaleSimpleLineItem.getProductGroupName().isPresent()
							?consumerSaleRegDraftSaleSimpleLineItem.getProductGroupName().get().getValue():null
					);
	}

	@Override
	public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem construct(
			@NonNull UpdateConsumerSaleRegDraftSaleSimpleLineItem updateConsumerSaleRegDraftSaleSimpleLineItemWebReq) {

		return
				new com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItemImpl(
						new ConsumerSaleRegDraftSaleLineItemIdImpl(updateConsumerSaleRegDraftSaleSimpleLineItemWebReq.getId()),
						new ConsumerSaleSimpleLineItemImpl(
							new AssetIdImpl(updateConsumerSaleRegDraftSaleSimpleLineItemWebReq.getAssetId()),
							new AssetSerialNumberImpl(updateConsumerSaleRegDraftSaleSimpleLineItemWebReq.getSerialNumber()),
							new ProductLineIdImpl(updateConsumerSaleRegDraftSaleSimpleLineItemWebReq.getProductLineId()),
							updateConsumerSaleRegDraftSaleSimpleLineItemWebReq.getPrice().orElse(null),
							new ProductLineNameImpl(updateConsumerSaleRegDraftSaleSimpleLineItemWebReq.getProductLineName()),
							updateConsumerSaleRegDraftSaleSimpleLineItemWebReq.getProductGroupId().isPresent()
							?new ProductGroupIdImpl(updateConsumerSaleRegDraftSaleSimpleLineItemWebReq.getProductGroupId().get())
							:null,
							updateConsumerSaleRegDraftSaleSimpleLineItemWebReq.getProductGroupName().isPresent()
							?new ProductGroupNameImpl(updateConsumerSaleRegDraftSaleSimpleLineItemWebReq.getProductGroupName().get())
							:null
						)
					);
	}

	@Override
	public UpdateConsumerSaleRegDraftSaleSimpleLineItem constructUpdateSaleRegDraftObj(
			com.precorconnect.consumersaleregistrationservice.@NonNull ConsumerSaleRegDraftSaleSimpleLineItem partnerSaleRegDraftSaleSimpleLineItem) {

		return
				new UpdateConsumerSaleRegDraftSaleSimpleLineItem(
						partnerSaleRegDraftSaleSimpleLineItem.getId().getValue(),
						partnerSaleRegDraftSaleSimpleLineItem.getAssetId().getValue(),
						partnerSaleRegDraftSaleSimpleLineItem.getSerialNumber().getValue(),
						partnerSaleRegDraftSaleSimpleLineItem.getProductLineId().getValue(),
						partnerSaleRegDraftSaleSimpleLineItem.getPrice().orElse(null),
						partnerSaleRegDraftSaleSimpleLineItem.getProductLineName().getValue(),
						partnerSaleRegDraftSaleSimpleLineItem.getProductGroupId().isPresent()
							?partnerSaleRegDraftSaleSimpleLineItem.getProductGroupId().get().getValue():null,
						partnerSaleRegDraftSaleSimpleLineItem.getProductGroupName().isPresent()
							?partnerSaleRegDraftSaleSimpleLineItem.getProductGroupName().get().getValue():null
					);
	}



}
