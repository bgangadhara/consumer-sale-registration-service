package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.math.BigDecimal;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class ConsumerSaleRegDraftSaleSimpleLineItem extends ConsumerSaleRegDraftSaleLineItem{

	private Long id;

	private String assetId;

	private String serialNumber;

	private int productLineId;

	private BigDecimal price;

	private String productLineName;

	private int productGroupId;

	private String productGroupName;

	public ConsumerSaleRegDraftSaleSimpleLineItem(
			@Nullable Long id,
			@NonNull String assetId,
			@NonNull String serialNumber,
			@NonNull int productLineId,
			@Nullable BigDecimal price,
			@NonNull String productLineName,
			@Nullable int productGroupId,
			@Nullable String productGroupName
	) {

		this.id=id;

		this.assetId =
                guardThat(
                        "assetId",
                        assetId
                		)
                        .isNotNull()
                        .thenGetValue();


		this.serialNumber =
                guardThat(
                        "serialNumber",
                        serialNumber
                		)
                        .isNotNull()
                        .thenGetValue();


		this.productLineId =
                guardThat(
                        "productLineId",
                        productLineId
                		)
                        .isNotNull()
                        .thenGetValue();

		this.price = price;

		this.productLineName =
                guardThat(
                        "productLineName",
                        productLineName
                		)
                        .isNotNull()
                        .thenGetValue();


		this.productGroupId = productGroupId;

		this.productGroupName = productGroupName;


	}

	public Long getId() {
		return id;
	}

	public String getAssetId() {
		return assetId;
	}


	public String getSerialNumber() {
		return serialNumber;
	}


	public int getProductLineId() {
		return productLineId;
	}


	public Optional<BigDecimal> getPrice() {
		return Optional.ofNullable(price);
	}


	public String getProductLineName() {
		return productLineName;
	}


	public Optional<Integer> getProductGroupId() {
		return Optional.ofNullable(productGroupId);
	}

	public Optional<String> getProductGroupName() {
		return Optional.ofNullable(productGroupName);
	}

}
