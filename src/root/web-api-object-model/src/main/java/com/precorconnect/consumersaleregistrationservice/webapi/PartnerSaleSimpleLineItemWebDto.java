package com.precorconnect.consumersaleregistrationservice.webapi;


public class PartnerSaleSimpleLineItemWebDto {
	
	private String serialNumber;

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Override
	public String toString() {
		return "PartnerSaleSimpleLineItemWebDto [serialNumber=" + serialNumber
				+ "]";
	}

	

	
}
