package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.math.BigDecimal;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class ConsumerSaleLineItemComponent{

		private Long id;

	    private String assetId;

	    private BigDecimal price;

	    private String serialNumber ;

	    private Integer productLineId ;

	    private String productLineName ;

	    private Integer productGroupId ;

	    private String productGroupName ;

		public ConsumerSaleLineItemComponent(
				@Nullable Long id,
				@NonNull String assetId,
				@Nullable BigDecimal price,
				@NonNull String serialNumber,
				@NonNull Integer productLineId,
				@NonNull String productLineName,
				@Nullable Integer productGroupId,
				@Nullable String productGroupName
		) {

			this.id = id;

			this.assetId =
	                guardThat(
	                        "assetId",
	                        assetId
	                		)
	                        .isNotNull()
	                        .thenGetValue();


			this.serialNumber =
	                guardThat(
	                        "serialNumber",
	                        serialNumber
	                		)
	                        .isNotNull()
	                        .thenGetValue();


			this.productLineId =
	                guardThat(
	                        "productLineId",
	                        productLineId
	                		)
	                        .isNotNull()
	                        .thenGetValue();

			this.price = price;

			this.productLineName =
	                guardThat(
	                        "productLineName",
	                        productLineName
	                		)
	                        .isNotNull()
	                        .thenGetValue();


			this.productGroupId = productGroupId;

			this.productGroupName = productGroupName;

		}

		public Optional<Long> getId() {
			return Optional.ofNullable(id);
		}

		public String getAssetId() {
			return assetId;
		}

		public void setAssetId(String assetId) {
			this.assetId = assetId;
		}

		public Optional<BigDecimal> getPrice() {
			return Optional.ofNullable(price);
		}

		public void setPrice(BigDecimal price) {
			this.price = price;
		}

		public String getSerialNumber() {
			return serialNumber;
		}

		public void setSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
		}

		public Integer getProductLineId() {
			return productLineId;
		}

		public void setProductLineId(Integer productLineId) {
			this.productLineId = productLineId;
		}

		public String getProductLineName() {
			return productLineName;
		}

		public void setProductLineName(String productLineName) {
			this.productLineName = productLineName;
		}

		public Optional<Integer> getProductGroupId() {
			return Optional.ofNullable(productGroupId);
		}

		public void setProductGroupId(Integer productGroupId) {
			this.productGroupId = productGroupId;
		}

		public Optional<String> getProductGroupName() {
			return Optional.ofNullable(productGroupName);
		}

		public void setProductGroupName(String productGroupName) {
			this.productGroupName = productGroupName;
		}

}
