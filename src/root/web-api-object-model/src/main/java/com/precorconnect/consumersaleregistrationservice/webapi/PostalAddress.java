package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class PostalAddress {
    /*
    fields
     */
    private final String addressLine1;
    
    private final String addressLine2;

    private final String city;

    private final String regionIso31662Code;

    private final String postalCode;

    private final String countryIso31661Alpha2Code;

    /*
    constructors
     */
    public PostalAddress(
            @NonNull String addressLine1,
            @Nullable String addressLine2,
            @NonNull String city,
            @NonNull String regionIso31662Code,
            @NonNull String postalCode,
            @NonNull String countryIso31661Alpha2Code
    ) {

        this.addressLine1 = 
				guardThat(
						"addressLine1",
						addressLine1
				)
						.isNotNull()
						.thenGetValue();
        
        this.addressLine2 =  addressLine2;

        this.city = 
				guardThat(
						"city",
						city
				)
						.isNotNull()
						.thenGetValue();

        this.regionIso31662Code = 
				guardThat(
						"regionIso31662Code",
						regionIso31662Code
				)
						.isNotNull()
						.thenGetValue();

        this.postalCode = 
				guardThat(
						"postalCode",
						postalCode
				)
						.isNotNull()
						.thenGetValue();

        this.countryIso31661Alpha2Code = 
				guardThat(
						"countryIso31661Alpha2Code",
						countryIso31661Alpha2Code
				)
						.isNotNull()
						.thenGetValue();

    }

    /*
    getter methods
    */
    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }
    
    public String getCity() {
        return city;
    }

    public String getRegionIso31662Code() {
        return regionIso31662Code;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCountryIso31661Alpha2Code() {
        return countryIso31661Alpha2Code;
    }

    /*
    equality methods
    */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostalAddress that = (PostalAddress) o;

        if (!addressLine1.equals(that.addressLine1)) return false;
        if (!addressLine2.equals(that.addressLine2)) return false;
        if (!city.equals(that.city)) return false;
        if (!regionIso31662Code.equals(that.regionIso31662Code)) return false;
        if (!postalCode.equals(that.postalCode)) return false;
        return countryIso31661Alpha2Code.equals(that.countryIso31661Alpha2Code);

    }

    @Override
    public int hashCode() {
        int result = addressLine1.hashCode();
        result = 31 * result + addressLine2.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + regionIso31662Code.hashCode();
        result = 31 * result + postalCode.hashCode();
        result = 31 * result + countryIso31661Alpha2Code.hashCode();
        return result;
    }
}
