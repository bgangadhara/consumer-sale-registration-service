package com.precorconnect.consumersaleregistrationservice.masterdataservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.precorconnect.spiffmasterdataservice.sdk.SpiffRuleEngineServiceSdk;
import com.precorconnect.spiffmasterdataservice.sdk.SpiffRuleEngineServiceSdkConfig;
import com.precorconnect.spiffmasterdataservice.sdk.SpiffRuleEngineServiceSdkConfigImpl;
import com.precorconnect.spiffmasterdataservice.sdk.SpiffRuleEngineServiceSdkImpl;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final MasterDataServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull MasterDataServiceAdapterConfig config
    ) {

    	this.config =
                guardThat(
                        "config",
                         config
                )
                        .isNotNull()
                        .thenGetValue();
    }

    @Override
    protected void configure() {

        bind(SpiffRuleEngineServiceSdkConfig.class)
                .toInstance(
                        new SpiffRuleEngineServiceSdkConfigImpl(
                                config.getPrecorConnectApiBaseUrl()
                        )
                );

        bind(SpiffRuleEngineServiceSdk.class)
                .to(SpiffRuleEngineServiceSdkImpl.class)
                .in(Singleton.class);

        bindFeatures();
        
        bindFactories();

    }

    private void bindFeatures() {

        bind(GetSpiffAmountForPartnerCommercialSaleRegistrationFeature.class)
                .to(GetSpiffAmountForPartnerCommercialSaleRegistrationFeatureImpl.class)
                .in(Singleton.class);
        
        bind(ListSpiffSerailExclusionFeature.class)
	        .to(ListSpiffSerailExclusionFeatureImpl.class)
	        .in(Singleton.class);

    }
    
    private void bindFactories(){
  	  bind(SpiffSerialExclusionResponseFactory.class)
        .to(SpiffSerialExclusionResponseFactoryImpl.class);
  }

}
