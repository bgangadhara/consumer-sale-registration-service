package com.precorconnect.consumersaleregistrationservice.masterdataservice;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.SpiffSerialExclusionDto;

public interface ListSpiffSerailExclusionFeature {
	
	Collection<SpiffSerialExclusionDto> execute(
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException;

}
