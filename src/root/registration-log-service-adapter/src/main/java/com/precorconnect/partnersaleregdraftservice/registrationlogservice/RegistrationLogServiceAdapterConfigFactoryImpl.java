package com.precorconnect.partnersaleregdraftservice.registrationlogservice;

import java.net.MalformedURLException;
import java.net.URL;

public class RegistrationLogServiceAdapterConfigFactoryImpl
        implements RegistrationLogServiceAdapterConfigFactory {

    @Override
    public RegistrationLogServiceAdapterConfig construct() {

        return new RegistrationLogServiceAdapterConfigImpl(
                constructPrecorConnectApiBaseUrl()
        );

    }

    private URL constructPrecorConnectApiBaseUrl() {

        String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");

        try {

            return new URL(precorConnectApiBaseUrl);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }
}
