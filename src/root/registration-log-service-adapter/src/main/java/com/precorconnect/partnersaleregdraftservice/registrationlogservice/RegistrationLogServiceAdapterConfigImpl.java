package com.precorconnect.partnersaleregdraftservice.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;
import java.net.URL;

import org.checkerframework.checker.nullness.qual.NonNull;

public class RegistrationLogServiceAdapterConfigImpl 
				implements RegistrationLogServiceAdapterConfig{

    /*
    fields
     */
    private final URL precorConnectApiBaseUrl;

    /*
    constructors
     */
    public RegistrationLogServiceAdapterConfigImpl(
            @NonNull URL precorConnectApiBaseUrl
    ) {

    	this.precorConnectApiBaseUrl =
                guardThat(
                        "precorConnectApiBaseUrl",
                         precorConnectApiBaseUrl
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public URL getPrecorConnectApiBaseUrl() {
        return precorConnectApiBaseUrl;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegistrationLogServiceAdapterConfigImpl that = (RegistrationLogServiceAdapterConfigImpl) o;

        return precorConnectApiBaseUrl.equals(that.precorConnectApiBaseUrl);

    }

    @Override
    public int hashCode() {
        return precorConnectApiBaseUrl.hashCode();
    }
}

