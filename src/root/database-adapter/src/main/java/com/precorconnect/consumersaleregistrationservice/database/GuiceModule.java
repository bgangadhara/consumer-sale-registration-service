package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

class GuiceModule extends AbstractModule {

    private final DatabaseAdapterConfig config;

    public GuiceModule(
            @NonNull DatabaseAdapterConfig config
    ) {
    	this.config =
                guardThat(
                        "config",
                         config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindFactories();

        bindFeatures();

    }

    private void bindFactories() {

        bind(ConsumerSaleRegDraftFactory.class)
                .to(ConsumerSaleRegDraftFactoryImpl.class);

        bind(ConsumerPartnerSaleRegDraftSynopsisViewFactory.class)
                .to(ConsumerCommercialSaleRegDraftSynopsisViewFactoryImpl.class);

        bind(ConsumerSaleRegDraftViewFactory.class)
                .to(ConsumerSaleRegDraftViewFactoryImpl.class);

        bind(ConsumerSaleLineItemComponentFactory.class)
                .to(ConsumerSaleLineItemComponentFactoryImpl.class);

        bind(ConsumerSaleRegDraftSaleLineItemFactory.class)
                .to(ConsumerSaleRegDraftSaleLineItemFactoryImpl.class);

        bind(ConsumerSaleRegDraftSaleCompositeLineItemFactory.class)
                .to(ConsumerSaleRegDraftSaleCompositeLineItemFactoryImpl.class);

        bind(ConsumerSaleRegDraftSaleSimpleLineItemFactory.class)
                .to(ConsumerSaleRegDraftSaleSimpleLineItemFactoryImpl.class);

        bind(ConsumerSaleRegDraftSynopsisViewFactory.class)
                .to(ConsumerSaleRegDraftSynopsisViewFactoryImpl.class);

        bind(UpdateConsumerSaleRegDraftFactory.class)
        	.to(UpdateConsumerSaleRegDraftFactoryImpl.class);

        bind(UpdateConsumerSaleLineItemComponentFactory.class)
        	.to(UpdateConsumerSaleLineItemComponentFactoryImpl.class);

        bind(UpdateConsumerSaleLineItemComponentFactory.class)
        	.to(UpdateConsumerSaleLineItemComponentFactoryImpl.class);

        bind(UpdateConsumerSaleRegDraftSaleCompositeLineItemFactory.class)
       		.to(UpdateConsumerSaleRegDraftSaleCompositeLineItemFactoryImpl.class);

        bind(UpdateConsumerSaleRegDraftSaleLineItemFactory.class)
        	.to(UpdateConsumerSaleRegDraftSaleLineItemFactoryImpl.class);

        bind(UpdateConsumerSaleRegDraftSaleSimpleLineItemFactory.class)
        	.to(UpdateConsumerSaleRegDraftSaleSimpleLineItemFactoryImpl.class);

        bind(UpdateConsumerSaleLineItemComponentReqFactory.class)
        	.to(UpdateConsumerSaleLineItemComponentReqFactoryImpl.class);

        bind(UpdateConsumerSaleRegDraftSaleCompositeLineItemReqFactory.class)
        	.to(UpdateConsumerSaleRegDraftSaleCompositeLineItemReqFactoryImpl.class);

        bind(UpdateConsumerSaleRegDraftSaleSimpleLineItemReqFactory.class)
        	.to(UpdateConsumerSaleRegDraftSaleSimpleLineItemReqFactoryImpl.class);
        
    }

    private void bindFeatures() {

        bind(AddConsumerSaleRegDraftFeature.class)
                .to(AddConsumerSaleRegDraftFeatureImpl.class);

        bind(AddSaleLineItemToConsumerSaleRegDraftFeature.class)
                .to(AddSaleLineItemToConsumerSaleRegDraftFeatureImpl.class);

        bind(DeleteConsumerSaleRegDraftWithIdFeature.class)
                .to(DeleteConsumerSaleRegDraftWithIdFeatureImpl.class);

        bind(GetConsumerSaleRegDraftWithIdFeature.class)
                .to(GetConsumerSaleRegDraftWithIdFeatureImpl.class);

        bind(GetPartnerSaleRegDraftWithAccountIdFeature.class)
        		.to(GetPartnerSaleRegDraftWithAccountIdFeatureImpl.class);

        bind(ListConsumerSaleRegDraftsWithPartnerAccountIdFeature.class)
                .to(ListPartnerSaleRegDraftsWithPartnerAccountIdFeatureImpl.class);

        bind(RemoveSaleLineItemFromConsumerSaleRegDraftFeature.class)
                .to(RemoveSaleLineItemFromConsumerSaleRegDraftFeatureImpl.class);

        bind(SubmitConsumerSaleRegDraftFeature.class)
        		.to(SubmitConsumerSaleRegDraftFeatureImpl.class);
   
        bind(UpdateConsumerSaleRegDraftFeature.class)
        		.to(UpdateConsumerSaleRegDraftFeatureImpl.class);

        bind(UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature.class)
				.to(UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeatureImpl.class);
        
    }
    
    @Provides
    @Singleton
    SessionFactory provideSessionFactory() {

        Configuration configuration = new Configuration();
        configuration.addPackage(getClass().getPackage().getName());
        configuration.addAttributeConverter(InstantPersistenceConverter.class, true);
        configuration.addAnnotatedClass(ConsumerPartnerSaleRegDraft.class);
        configuration.addAnnotatedClass(ConsumerSaleRegDraft.class);
        configuration.addAnnotatedClass(ConsumerSaleRegDraftSaleCompositeLineItem.class);
        configuration.addAnnotatedClass(ConsumerSaleRegDraftSaleLineItem.class);
        configuration.addAnnotatedClass(ConsumerSaleLineItemComponent.class);
        configuration.addAnnotatedClass(ConsumerSaleRegDraftSaleSimpleLineItem.class);
        configuration.configure();

        configuration.setProperty("hibernate.connection.url", config.getUri().toString());
        configuration.setProperty("hibernate.connection.username", config.getUsername().getValue());
        configuration.setProperty("hibernate.connection.password", config.getPassword().getValue());

        StandardServiceRegistryBuilder builder =
                new StandardServiceRegistryBuilder()
                        .applySettings(
                                configuration.getProperties());

        return configuration
                .buildSessionFactory(
                        builder.build());
    }
}
