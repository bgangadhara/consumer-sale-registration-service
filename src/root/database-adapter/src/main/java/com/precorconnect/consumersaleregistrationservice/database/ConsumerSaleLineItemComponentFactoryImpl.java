package com.precorconnect.consumersaleregistrationservice.database;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AssetIdImpl;
import com.precorconnect.AssetSerialNumberImpl;
import com.precorconnect.ProductGroupIdImpl;
import com.precorconnect.ProductGroupNameImpl;
import com.precorconnect.ProductLineIdImpl;
import com.precorconnect.ProductLineNameImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponentImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;


@Singleton
class ConsumerSaleLineItemComponentFactoryImpl
        implements ConsumerSaleLineItemComponentFactory {

    @Override
    public ConsumerSaleLineItemComponent construct(
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleLineItemComponent consumerSaleLineItemComponent
    ) {

        ConsumerSaleLineItemComponent objectUnderConstruction =
                new ConsumerSaleLineItemComponent();

        objectUnderConstruction
                .setAssetId(
                		consumerSaleLineItemComponent
                                .getAssetId()
                                .getValue()
                );
        objectUnderConstruction
	        .setSerialNumber(
	        		consumerSaleLineItemComponent
	                        .getSerialNumber()
	                        .getValue()
	        );
        objectUnderConstruction
	        .setProductGroupId(
	        		consumerSaleLineItemComponent
	                        .getProductGroupId().isPresent()
	                        ?consumerSaleLineItemComponent.getProductGroupId().get().getValue()
	                        :null
	        );
        objectUnderConstruction
        .setProductGroupName(
        		consumerSaleLineItemComponent
                        .getProductGroupName().isPresent()
                        ?consumerSaleLineItemComponent.getProductGroupName().get().getValue()
                        :null
        );
        objectUnderConstruction
	        .setProductLineId(
	        		consumerSaleLineItemComponent
	                        .getProductLineId()
	                        .getValue()
	        );
        objectUnderConstruction
	        .setProductLineName(
	        		consumerSaleLineItemComponent
	                        .getProductLineName()
	                        .getValue()
	        );
        objectUnderConstruction
                .setPrice(
                		consumerSaleLineItemComponent
                                .getPrice()
                                .orElse(null)
                );

        return objectUnderConstruction;

    }

    @Override
    public com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponent construct(
            @NonNull ConsumerSaleLineItemComponent consumerSaleLineItemComponent
    ) {

    	return
    			new ConsumerSaleLineItemComponentImpl(
    					new ConsumerSaleRegDraftSaleLineItemIdImpl(
    							consumerSaleLineItemComponent
    									.getId()
    					),
    					 new AssetIdImpl(
                                 consumerSaleLineItemComponent
                                         .getAssetId()
                         ),
    					 new AssetSerialNumberImpl(
                                 consumerSaleLineItemComponent
                                         .getSerialNumber()
                         ),
    					 new ProductLineIdImpl(
                                 consumerSaleLineItemComponent
                                         .getProductLineId()
                         ),
    					consumerSaleLineItemComponent
	                        .getPrice()
	                        .orElse(null),
    					 new ProductLineNameImpl(
                                 consumerSaleLineItemComponent
                                         .getProductLineName()
                         ),
    					 consumerSaleLineItemComponent.getProductGroupId()!=null
    					 ?new ProductGroupIdImpl(consumerSaleLineItemComponent.getProductGroupId())
    					 :null,
    					 consumerSaleLineItemComponent.getProductGroupName()!= null
    					 ?new ProductGroupNameImpl(consumerSaleLineItemComponent.getProductGroupName())
    					 :null
    			);

    }



}
