package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItem;

@Singleton
public class UpdatePartnerSaleRegDraftSaleLineItemFactoryImpl implements UpdateConsumerSaleRegDraftSaleLineItemFactory {
	
	 /*
    fields
     */
    private final UpdateConsumerSaleRegDraftSaleSimpleLineItemFactory partnerSaleRegDraftSaleSimpleLineItemFactory;

    private final UpdateConsumerSaleRegDraftSaleCompositeLineItemFactory partnerSaleRegDraftSaleCompositeLineItemFactory;
    
    private final UpdateConsumerSaleRegDraftSaleSimpleLineItemReqFactory updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory;
    
    private final UpdateConsumerSaleRegDraftSaleCompositeLineItemReqFactory updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory;
    
    
    

    @Inject
	public UpdatePartnerSaleRegDraftSaleLineItemFactoryImpl(
			UpdateConsumerSaleRegDraftSaleSimpleLineItemFactory partnerSaleRegDraftSaleSimpleLineItemFactory,
			UpdateConsumerSaleRegDraftSaleCompositeLineItemFactory partnerSaleRegDraftSaleCompositeLineItemFactory,
			UpdateConsumerSaleRegDraftSaleSimpleLineItemReqFactory updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory,
			UpdateConsumerSaleRegDraftSaleCompositeLineItemReqFactory updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory) {
		
		this.partnerSaleRegDraftSaleSimpleLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleSimpleLineItemFactory",
                        partnerSaleRegDraftSaleSimpleLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.partnerSaleRegDraftSaleCompositeLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleCompositeLineItemFactory",
                        partnerSaleRegDraftSaleCompositeLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		
		this.updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory =
                guardThat(
                        "updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory",
                        updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory =
                guardThat(
                        "updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory",
                        updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory
                )
                        .isNotNull()
                        .thenGetValue();
				
		
	}

	@Override
	public com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleLineItem construct(@NonNull ConsumerSaleLineItem consumerSaleLineItem) {
		
		
		if (consumerSaleLineItem instanceof
                com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem) {
            return
            		updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory
                            .construct(
                                    (com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem)
                                            consumerSaleLineItem
                            );
        } else if (consumerSaleLineItem instanceof
                com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem) {
            return
            		updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory
                            .construct(
                                    (com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem)
                                            consumerSaleLineItem
                            );
        }
		
		 // handle unexpected implementations
        throw
                new RuntimeException(
                        String.format(
                                "received unsupported %s implementation: %s",
                                com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem.class,
                                consumerSaleLineItem.getClass()
                        )
                );
            
	}

	@Override
	public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem construct(
			com.precorconnect.consumersaleregistrationservice.database.@NonNull ConsumerSaleRegDraftSaleLineItem consumerSaleRegDraftSaleLineItem) {
		
		
		if (consumerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleSimpleLineItem) {

            return
                    partnerSaleRegDraftSaleSimpleLineItemFactory
                            .construct(
                                    (ConsumerSaleRegDraftSaleSimpleLineItem) consumerSaleRegDraftSaleLineItem
                            );

        }
		else if (consumerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleCompositeLineItem) {

            return
                    partnerSaleRegDraftSaleCompositeLineItemFactory
                            .construct(
                                    (ConsumerSaleRegDraftSaleCompositeLineItem) consumerSaleRegDraftSaleLineItem
                            );

        }
        // handle unexpected implementations
        throw
                new RuntimeException(
                        String.format(
                                "received unsupported %s implementation: %s",
                                ConsumerSaleRegDraftSaleLineItem.class,
                                consumerSaleRegDraftSaleLineItem.getClass()
                        )
                );
        
	}

}
