package com.precorconnect.consumersaleregistrationservice.database;


import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;



@Singleton
public class UpdateConsumerSaleRegDraftSaleSimpleLineItemReqFactoryImpl
		implements UpdateConsumerSaleRegDraftSaleSimpleLineItemReqFactory {



	@Override
	public com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleSimpleLineItem construct(
			 com.precorconnect.consumersaleregistrationservice.
			 @NonNull ConsumerSaleRegDraftSaleSimpleLineItem partnerSaleRegDraftSaleSimpleLineItem) {


		try {

		    com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleSimpleLineItem objectUnderConstruction=
		    		new com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleSimpleLineItem();


		    objectUnderConstruction
	        .setId(
	        		partnerSaleRegDraftSaleSimpleLineItem
	        		.getId()
	        		.getValue()
	        	);

	        objectUnderConstruction
	                .setAssetId(
	                        partnerSaleRegDraftSaleSimpleLineItem
	                                .getAssetId()
	                                .getValue()
	                );

	        objectUnderConstruction
	                .setPrice(
	                        partnerSaleRegDraftSaleSimpleLineItem
	                                .getPrice()
	                                .orElse(null)
	                );

	        objectUnderConstruction
	        		.setProductGroupId(
		                partnerSaleRegDraftSaleSimpleLineItem
		                        .getProductGroupId().isPresent()
		                        ?partnerSaleRegDraftSaleSimpleLineItem.getProductGroupId().get().getValue()
		                        :null
		        );

	        objectUnderConstruction
					.setProductGroupName(
							 partnerSaleRegDraftSaleSimpleLineItem
			                    .getProductGroupName().isPresent()
		                        ?partnerSaleRegDraftSaleSimpleLineItem.getProductGroupName().get().getValue()
		                        :null
			    );
	        objectUnderConstruction
			        .setProductLineId(
			                partnerSaleRegDraftSaleSimpleLineItem
			                        .getProductLineId()
			                        .getValue()
			        );

	        objectUnderConstruction
			        .setProductLineName(
			                partnerSaleRegDraftSaleSimpleLineItem
			                        .getProductLineName()
			                        .getValue()
			        );

	        objectUnderConstruction
			        .setSerialNumber(
			                partnerSaleRegDraftSaleSimpleLineItem
			                        .getSerialNumber()
			                        .getValue()
			        );


	           return objectUnderConstruction;

	        } catch(final Exception e) {

	        	throw new RuntimeException("exception while constructing PartnerSaleRegDraftSaleSimpleLineItem:", e);

	        }


	}

}
