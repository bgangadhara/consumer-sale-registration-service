package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.consumersaleregistrationservice.AddConsumerSaleRegDraftReq;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftIdImpl;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class AddConsumerSaleRegDraftFeatureImpl
        implements AddConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final SessionFactory sessionFactory;

    private final ConsumerSaleRegDraftFactory consumerSaleRegDraftFactory;

    /*
    constructors
     */
    @Inject
    public AddConsumerSaleRegDraftFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull ConsumerSaleRegDraftFactory consumerSaleRegDraftFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

        this.consumerSaleRegDraftFactory =
                guardThat(
                        "consumerSaleRegDraftFactory",
                        consumerSaleRegDraftFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public ConsumerSaleRegDraftId execute(
            @NonNull AddConsumerSaleRegDraftReq request
    ) {

        Session session = null;

        try {

            session = sessionFactory.openSession();

            ConsumerPartnerSaleRegDraft consumerSaleRegDraft =
            		consumerSaleRegDraftFactory.construct(request);

            try {

                session.beginTransaction();
                session.save(consumerSaleRegDraft);
                session.getTransaction().commit();

            } catch (HibernateException e) {

                session.getTransaction().rollback();
                throw e;

            }

            return
                    new ConsumerSaleRegDraftIdImpl(
                            consumerSaleRegDraft.getId()
                            );

        } finally {

            if (null != session) {
                session.close();
            }

        }

    }
}
