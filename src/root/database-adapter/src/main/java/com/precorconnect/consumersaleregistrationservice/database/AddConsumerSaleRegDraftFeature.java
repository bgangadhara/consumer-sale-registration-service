package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.consumersaleregistrationservice.AddConsumerSaleRegDraftReq;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import org.checkerframework.checker.nullness.qual.NonNull;

interface AddConsumerSaleRegDraftFeature {

	ConsumerSaleRegDraftId execute(
            @NonNull AddConsumerSaleRegDraftReq request
    );

}
