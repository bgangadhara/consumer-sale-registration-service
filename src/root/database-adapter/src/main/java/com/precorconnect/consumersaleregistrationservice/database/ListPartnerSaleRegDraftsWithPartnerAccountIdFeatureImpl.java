package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.precorconnect.AccountId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSynopsisView;

@Singleton
class ListPartnerSaleRegDraftsWithPartnerAccountIdFeatureImpl
        implements ListConsumerSaleRegDraftsWithPartnerAccountIdFeature {

    /*
    fields
     */
    private final SessionFactory sessionFactory;
    private final ConsumerSaleRegDraftSynopsisViewFactory consumerSaleRegDraftSynopsisViewFactory;

    /*
    constructors
     */
    @Inject
    public ListPartnerSaleRegDraftsWithPartnerAccountIdFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull ConsumerSaleRegDraftSynopsisViewFactory consumerSaleRegDraftSynopsisViewFactory
            ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.consumerSaleRegDraftSynopsisViewFactory =
                guardThat(
                        "consumerSaleRegDraftSynopsisViewFactory",
                        consumerSaleRegDraftSynopsisViewFactory
                )
                        .isNotNull()
                        .thenGetValue();
    }

    @SuppressWarnings("unchecked")
	@Override
    public Collection<ConsumerSaleRegDraftSynopsisView> execute(
            @NonNull AccountId partnerAccountId
    ) {

        Session session = null;
        try {
            session = sessionFactory.openSession();

            List<com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSynopsisView>
                    partnerSaleRegDraftSynopsisViews =
                    session
                            .createCriteria(
                                    com.precorconnect.consumersaleregistrationservice.database.
                                            ConsumerSaleRegDraftSynopsisView.class
                            )
                            .add(
                                    Restrictions.eq(
                                            "accountId",
                                            partnerAccountId.getValue()
                                    )
                            )
                            .list();

            return
                    partnerSaleRegDraftSynopsisViews
                            .stream()
                            .map(consumerSaleRegDraftSynopsisViewFactory::construct)
                            .collect(Collectors.toList());

        } finally {

            if (session != null) {
                session.close();
            }

        }

    }
}
