package com.precorconnect.consumersaleregistrationservice.database;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleLineItem;

@Entity
@Table(name = "ConsumerSaleRegDrafts")
@Inheritance(strategy = InheritanceType.JOINED)
class ConsumerSaleRegDraft {

    /*
    fields
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String partnerAccountId;
    
    @OneToMany(
            mappedBy = "consumerSaleRegDraft",
            cascade = CascadeType.ALL
    )
    private Collection<ConsumerSaleRegDraftSaleLineItem> saleLineItems =
            new ArrayList<>();

    private String saleInvoiceId;

    private Instant saleCreatedAtTimestamp;

    private String salePartnerRepId;

    /*
    getter & setter methods
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPartnerAccountId() {
        return partnerAccountId;
    }

    public void setPartnerAccountId(String partnerAccountId) {
        this.partnerAccountId = partnerAccountId;
    }


    public Collection<ConsumerSaleRegDraftSaleLineItem> getSaleLineItems() {
        return saleLineItems;
    }

    public void setSaleLineItems(Collection<ConsumerSaleRegDraftSaleLineItem> saleLineItems) {

        saleLineItems
                .stream()
                .forEach(
                        partnerSaleRegDraftSaleLineItem ->
                                partnerSaleRegDraftSaleLineItem
                                        .setPartnerSaleRegDraft(this)
                );

        this.saleLineItems = saleLineItems;
    }

    public Optional<String> getSaleInvoiceId() {
        return Optional.ofNullable(saleInvoiceId);
    }

    public void setSaleInvoiceId(String saleInvoiceId) {
        this.saleInvoiceId = saleInvoiceId;
    }

    public Optional<Instant> getSaleCreatedAtTimestamp() {
        return Optional.ofNullable(saleCreatedAtTimestamp);
    }

    public void setSaleCreatedAtTimestamp(Instant saleCreatedAtTimestamp) {
        this.saleCreatedAtTimestamp = saleCreatedAtTimestamp;
    }

    public Optional<String> getSalePartnerRepId() {
        return Optional.ofNullable(salePartnerRepId);
    }

    public void setSalePartnerRepId(String salePartnerRepId) {
        this.salePartnerRepId = salePartnerRepId;
    }
    /*
    equality methods
    */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((partnerAccountId == null) ? 0 : partnerAccountId.hashCode());
		result = prime * result + ((saleCreatedAtTimestamp == null) ? 0 : saleCreatedAtTimestamp.hashCode());
		result = prime * result + ((saleInvoiceId == null) ? 0 : saleInvoiceId.hashCode());
		result = prime * result + ((saleLineItems == null) ? 0 : saleLineItems.hashCode());
		result = prime * result + ((salePartnerRepId == null) ? 0 : salePartnerRepId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsumerSaleRegDraft other = (ConsumerSaleRegDraft) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (partnerAccountId == null) {
			if (other.partnerAccountId != null)
				return false;
		} else if (!partnerAccountId.equals(other.partnerAccountId))
			return false;
		if (saleCreatedAtTimestamp == null) {
			if (other.saleCreatedAtTimestamp != null)
				return false;
		} else if (!saleCreatedAtTimestamp.equals(other.saleCreatedAtTimestamp))
			return false;
		if (saleInvoiceId == null) {
			if (other.saleInvoiceId != null)
				return false;
		} else if (!saleInvoiceId.equals(other.saleInvoiceId))
			return false;
		if (saleLineItems == null) {
			if (other.saleLineItems != null)
				return false;
		} else if (!saleLineItems.equals(other.saleLineItems))
			return false;
		if (salePartnerRepId == null) {
			if (other.salePartnerRepId != null)
				return false;
		} else if (!salePartnerRepId.equals(other.salePartnerRepId))
			return false;
		return true;
	}
    
    
}
