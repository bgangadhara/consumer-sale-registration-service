package com.precorconnect.consumersaleregistrationservice.database;


import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import org.checkerframework.checker.nullness.qual.NonNull;

interface ConsumerSaleRegDraftViewFactory {

    ConsumerPartnerSaleRegDraftView construct(
            @NonNull ConsumerPartnerSaleRegDraft consumerPartnerSaleRegDraft
    );

}
