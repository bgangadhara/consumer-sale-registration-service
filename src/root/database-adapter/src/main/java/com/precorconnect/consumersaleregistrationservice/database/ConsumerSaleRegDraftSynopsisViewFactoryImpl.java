package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftSynopsisView;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class ConsumerSaleRegDraftSynopsisViewFactoryImpl
        implements ConsumerSaleRegDraftSynopsisViewFactory {

    /*
    fields
     */
    private final ConsumerPartnerSaleRegDraftSynopsisViewFactory partnerCommercialSaleRegDraftSynopsisViewFactory;


    /*
    constructors
    */
    @Inject
    public ConsumerSaleRegDraftSynopsisViewFactoryImpl(
            @NonNull ConsumerPartnerSaleRegDraftSynopsisViewFactory partnerCommercialSaleRegDraftSynopsisViewFactory
    ) {

    	this.partnerCommercialSaleRegDraftSynopsisViewFactory =
                guardThat(
                        "partnerCommercialSaleRegDraftSynopsisViewFactory",
                         partnerCommercialSaleRegDraftSynopsisViewFactory
                )
                        .isNotNull()
                        .thenGetValue();
    }


    @Override
    public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSynopsisView construct(
            @NonNull ConsumerSaleRegDraftSynopsisView partnerSaleRegDraftSynopsisView
    ) {

        if (partnerSaleRegDraftSynopsisView instanceof ConsumerPartnerSaleRegDraftSynopsisView) {

            return
                    partnerCommercialSaleRegDraftSynopsisViewFactory
                            .construct(
                                    (ConsumerPartnerSaleRegDraftSynopsisView)
                                            partnerSaleRegDraftSynopsisView
                            );
        }

        // handle unexpected implementations
        throw
                new RuntimeException(
                        String.format(
                                "received unsupported %s implementation: %s",
                                ConsumerSaleRegDraftSynopsisView.class,
                                partnerSaleRegDraftSynopsisView.getClass()
                        )
                );

    }
}
