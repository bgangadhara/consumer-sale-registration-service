package com.precorconnect.consumersaleregistrationservice.database;

import javax.persistence.*;

import com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleLineItem;

@Entity
@Table(name = "ConsumerSaleRegDraftSaleLineItems")
@Inheritance(strategy = InheritanceType.JOINED)
class ConsumerSaleRegDraftSaleLineItem {

    /*
    fields
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(
            name = "ConsumerSaleRegDraftId",
            referencedColumnName = "Id"
    )
    private ConsumerSaleRegDraft consumerSaleRegDraft;

    /*
    getter & setter methods
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConsumerSaleRegDraft getPartnerSaleRegDraft() {
        return consumerSaleRegDraft;
    }

    public void setPartnerSaleRegDraft(ConsumerSaleRegDraft partnerSaleRegDraft) {
        this.consumerSaleRegDraft = partnerSaleRegDraft;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConsumerSaleRegDraftSaleLineItem that = (ConsumerSaleRegDraftSaleLineItem) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return !(consumerSaleRegDraft != null ? !consumerSaleRegDraft.equals(that.consumerSaleRegDraft) : that.consumerSaleRegDraft != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (consumerSaleRegDraft != null ? consumerSaleRegDraft.hashCode() : 0);
        return result;
    }
}

