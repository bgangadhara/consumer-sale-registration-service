package com.precorconnect.consumersaleregistrationservice.database;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AssetIdImpl;
import com.precorconnect.AssetSerialNumberImpl;
import com.precorconnect.ProductGroupIdImpl;
import com.precorconnect.ProductGroupNameImpl;
import com.precorconnect.ProductLineIdImpl;
import com.precorconnect.ProductLineNameImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItemImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleSimpleLineItemImpl;


@Singleton
public class UpdateConsumerSaleRegDraftSaleSimpleLineItemFactoryImpl
		implements UpdateConsumerSaleRegDraftSaleSimpleLineItemFactory {

	@Override
	public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem construct(
			com.precorconnect.consumersaleregistrationservice.database.@NonNull ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem) {
		
		return
                new ConsumerSaleRegDraftSaleSimpleLineItemImpl(
                        new ConsumerSaleRegDraftSaleLineItemIdImpl(
                        		consumerSaleRegDraftSaleSimpleLineItem.getId()
                        ),
                        new ConsumerSaleSimpleLineItemImpl(
                        		
                                new AssetIdImpl(
                                		consumerSaleRegDraftSaleSimpleLineItem
                                                .getAssetId()
                                ),
                                new AssetSerialNumberImpl(
                                		consumerSaleRegDraftSaleSimpleLineItem
                                                .getSerialNumber()
                                ),
                                new ProductLineIdImpl(
                                		consumerSaleRegDraftSaleSimpleLineItem
                                                .getProductLineId()
                                ),
                                consumerSaleRegDraftSaleSimpleLineItem
                                                .getPrice().orElse(null)
                                ,
                                new ProductLineNameImpl(
                                		consumerSaleRegDraftSaleSimpleLineItem
                                				.getProductLineName()
                                ),
                                new ProductGroupIdImpl(
                                		consumerSaleRegDraftSaleSimpleLineItem
                                				.getProductGroupId()
                                ),
                                new ProductGroupNameImpl(
                                		consumerSaleRegDraftSaleSimpleLineItem
                                				.getProductGroupName()
                                )
                        )
                );
	}

}
