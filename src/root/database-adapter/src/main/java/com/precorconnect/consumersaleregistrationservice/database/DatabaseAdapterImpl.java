
package com.precorconnect.consumersaleregistrationservice.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.flywaydb.core.Flyway;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.AddConsumerSaleRegDraftReq;
import com.precorconnect.consumersaleregistrationservice.AddSaleLineItemToConsumerSaleRegDraftReq;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemId;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.DatabaseAdapter;
import com.precorconnect.consumersaleregistrationservice.UpdateConsumerSaleRegDraftReq;
import com.precorconnect.consumersaleregistrationservice.UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq;

public class DatabaseAdapterImpl implements DatabaseAdapter {
	
	 private final Injector injector;

	    public DatabaseAdapterImpl(
	            @NonNull DatabaseAdapterConfig config
	    ) {

	        // ensure database schema up to date
	        Flyway flyway = new Flyway();
	        flyway.setDataSource(
	                config.getUri().toString(),
	                config.getUsername().getValue(),
	                config.getPassword().getValue()
	        );
	        flyway.migrate();

	        GuiceModule guiceModule =
	                new GuiceModule(
	                        config
	                );

	        injector =
	                Guice.createInjector(guiceModule);

	    }

	@Override
	public ConsumerSaleRegDraftId addConsumerSaleRegDraft(
			@NonNull AddConsumerSaleRegDraftReq request
			) {
		
		 return
	                injector
	                        .getInstance(AddConsumerSaleRegDraftFeature.class)
	                        .execute(request);
	}

	@Override
	public ConsumerSaleRegDraftSaleLineItemId addSaleLineItemToConsumerSaleRegDraft(
			@NonNull AddSaleLineItemToConsumerSaleRegDraftReq request
			) {
		return null;
	}

	@Override
	public String deleteConsumerSaleRegDraftWithId(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId) {
		
		return 
				injector
                .getInstance(DeleteConsumerSaleRegDraftWithIdFeature.class)
                .execute(consumerSaleRegDraftId);

	}

	@Override
	public ConsumerPartnerSaleRegDraftView getConsumerSaleRegDraftWithId(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId) {
		
		return 
				injector
                .getInstance(GetConsumerSaleRegDraftWithIdFeature.class)
                .execute(consumerSaleRegDraftId);

	}

	@Override
	public Collection<ConsumerPartnerSaleRegDraftView> getConsumerSaleRegDraftWithAccountId(
			@NonNull AccountId accountId
			) {
		
		return 
				injector
                .getInstance(GetPartnerSaleRegDraftWithAccountIdFeature.class)
                .execute(
                		accountId
                		);
		
	}

	
	@Override
	public void removeSaleLineItemFromConsumerSaleRegDraft(
			@NonNull ConsumerSaleRegDraftSaleLineItemId consumerSaleRegDraftSaleLineItemId) {
		
		injector
		        .getInstance(RemoveSaleLineItemFromConsumerSaleRegDraftFeature.class)
		        .execute(consumerSaleRegDraftSaleLineItemId);

	}

	@Override
	public ConsumerPartnerSaleRegDraftView updateConsumerSaleRegDraft(
			@NonNull UpdateConsumerSaleRegDraftReq request,
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId) {
		
		return 
				 injector
		                 .getInstance(UpdateConsumerSaleRegDraftFeature.class)
		                 .execute(
		            		 request,
		            		 consumerSaleRegDraftId
		                 );
	}

	@Override
	public ConsumerPartnerSaleRegDraftView submitConsumerSaleRegDraft(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull OAuth2AccessToken accessToken
			) {
		
		return 
				injector
		                .getInstance(SubmitConsumerSaleRegDraftFeature.class)
		                .execute(
		           		 consumerSaleRegDraftId,
		           		 accessToken
		               );
	}

	@Override
	public void updateSaleInvoiceUrlOfConsumerSaleRegDraft(
			@NonNull UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq request
			) {
		
		injector
        .getInstance(UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature.class)
        .execute(request);

	}

}
