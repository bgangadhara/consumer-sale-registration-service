package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.UpdateConsumerSaleRegDraftReq;

public interface UpdateConsumerSaleRegDraftFactory {
	
	 ConsumerPartnerSaleRegDraft construct(
	            @NonNull UpdateConsumerSaleRegDraftReq updateConsumerSaleRegDraftReq,
	            @NonNull ConsumerPartnerSaleRegDraft partnerCommercialSaleRegDraft
	            
	    );

}
