package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemId;
import org.checkerframework.checker.nullness.qual.NonNull;

interface RemoveSaleLineItemFromConsumerSaleRegDraftFeature {

    void execute(
            @NonNull ConsumerSaleRegDraftSaleLineItemId partnerSaleRegDraftSaleLineItemId
    );

}
