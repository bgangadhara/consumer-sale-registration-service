package com.precorconnect.consumersaleregistrationservice.database;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AssetIdImpl;
import com.precorconnect.AssetSerialNumberImpl;
import com.precorconnect.ProductGroupIdImpl;
import com.precorconnect.ProductGroupNameImpl;
import com.precorconnect.ProductLineIdImpl;
import com.precorconnect.ProductLineNameImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponentImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;

@Singleton
public class UpdateConsumerSaleLineItemComponentFactoryImpl implements UpdateConsumerSaleLineItemComponentFactory {

	@Override
	public com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponent construct(
			com.precorconnect.consumersaleregistrationservice.database.@NonNull ConsumerSaleLineItemComponent partnerSaleLineItemComponent) {
		
		
		return
    			new ConsumerSaleLineItemComponentImpl(
    					new ConsumerSaleRegDraftSaleLineItemIdImpl(
    							partnerSaleLineItemComponent
    									.getId()
    					),
    					 new AssetIdImpl(
                                 partnerSaleLineItemComponent
                                         .getAssetId()
                         ),
    					 new AssetSerialNumberImpl(
                                 partnerSaleLineItemComponent
                                         .getSerialNumber()
                         ),
    					 new ProductLineIdImpl(
                                 partnerSaleLineItemComponent
                                         .getProductLineId()
                         ),
    					partnerSaleLineItemComponent
	                        .getPrice()
	                        .orElse(null),
    					 new ProductLineNameImpl(
                                 partnerSaleLineItemComponent
                                         .getProductLineName()
                         ),
    					 new ProductGroupIdImpl(
                                 partnerSaleLineItemComponent
                                         .getProductGroupId()
                         ),
    					 new ProductGroupNameImpl(
                                 partnerSaleLineItemComponent
                                         .getProductGroupName()
                         )
    			);
	}

}
