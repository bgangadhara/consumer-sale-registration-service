package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.sql.Timestamp;
import java.util.Date;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;

public class SubmitConsumerSaleRegDraftFeatureImpl implements
		SubmitConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final SessionFactory sessionFactory;

    private final ConsumerSaleRegDraftViewFactory consumerSaleRegDraftViewFactory;
    
 
    /*
    constructors
     */
    @Inject
    public SubmitConsumerSaleRegDraftFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull ConsumerSaleRegDraftViewFactory consumerSaleRegDraftViewFactory
           
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.consumerSaleRegDraftViewFactory =
                guardThat(
                        "consumerSaleRegDraftViewFactory",
                         consumerSaleRegDraftViewFactory
                )
                        .isNotNull()
                        .thenGetValue();
    }

	@Override
	public ConsumerPartnerSaleRegDraftView execute(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			 @NonNull OAuth2AccessToken accessToken
			) {


        Session session = null;

        try {

            session = sessionFactory.openSession();

            Transaction tx = session.beginTransaction();

            try {

                Query query = session.createQuery("update ConsumerPartnerSaleRegDraft set isSubmitted = :submitStatus , saleCreatedAtTimestamp = :submitTime where id = :draftId");

                query.setBoolean("submitStatus", true);

                query.setLong("draftId", consumerSaleRegDraftId.getValue());

                query.setParameter("submitTime", new InstantPersistenceConverter()
        								.convertToEntityAttribute(new Timestamp(new Date().getTime())));

                query.executeUpdate();

                tx.commit();

            } catch (HibernateException e) {

            	tx.rollback();
                throw e;

            }

            ConsumerPartnerSaleRegDraft consumerSaleRegDraft =
                    (ConsumerPartnerSaleRegDraft) session.get(
                    		ConsumerPartnerSaleRegDraft.class,
                            consumerSaleRegDraftId.getValue()
                    );
           
            return
            		consumerSaleRegDraftViewFactory
            					.construct(consumerSaleRegDraft);
        } finally {

            if (null != session) {
                session.close();
            }

        }

	}

}
