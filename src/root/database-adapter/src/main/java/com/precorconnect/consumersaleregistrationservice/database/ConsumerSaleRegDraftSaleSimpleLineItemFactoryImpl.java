package com.precorconnect.consumersaleregistrationservice.database;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AssetIdImpl;
import com.precorconnect.AssetSerialNumberImpl;
import com.precorconnect.ProductGroupIdImpl;
import com.precorconnect.ProductGroupNameImpl;
import com.precorconnect.ProductLineIdImpl;
import com.precorconnect.ProductLineNameImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItemImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleSimpleLineItemImpl;

@Singleton
class ConsumerSaleRegDraftSaleSimpleLineItemFactoryImpl
        implements ConsumerSaleRegDraftSaleSimpleLineItemFactory {

    @Override
    public ConsumerSaleRegDraftSaleSimpleLineItem construct(
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem
    ) {

    	ConsumerSaleRegDraftSaleSimpleLineItem objectUnderConstruction =
                new ConsumerSaleRegDraftSaleSimpleLineItem();

        objectUnderConstruction
        .setId(
        		consumerSaleRegDraftSaleSimpleLineItem
        		.getId()
        		.getValue()
        	);

        objectUnderConstruction
                .setAssetId(
                        consumerSaleRegDraftSaleSimpleLineItem
                                .getAssetId()
                                .getValue()
                );

        objectUnderConstruction
                .setPrice(
                        consumerSaleRegDraftSaleSimpleLineItem
                                .getPrice()
                                .orElse(null)
                );

        objectUnderConstruction
        		.setProductGroupId(
	                consumerSaleRegDraftSaleSimpleLineItem
	                        .getProductGroupId().isPresent()
	                        ?consumerSaleRegDraftSaleSimpleLineItem.getProductGroupId().get().getValue()
	                        :null
	        );

        objectUnderConstruction
				.setProductGroupName(
		            consumerSaleRegDraftSaleSimpleLineItem
		                    .getProductGroupName().isPresent()
	                        ?consumerSaleRegDraftSaleSimpleLineItem.getProductGroupName().get().getValue()
	                        :null
		    );
        objectUnderConstruction
		        .setProductLineId(
		                consumerSaleRegDraftSaleSimpleLineItem
		                        .getProductLineId()
		                        .getValue()
		        );

        objectUnderConstruction
		        .setProductLineName(
		                consumerSaleRegDraftSaleSimpleLineItem
		                        .getProductLineName()
		                        .getValue()
		        );

        objectUnderConstruction
		        .setSerialNumber(
		                consumerSaleRegDraftSaleSimpleLineItem
		                        .getSerialNumber()
		                        .getValue()
		        );

        return objectUnderConstruction;

    }

    @Override
    public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem construct(
            @NonNull ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem
    ) {

        return
                new ConsumerSaleRegDraftSaleSimpleLineItemImpl(
                        new ConsumerSaleRegDraftSaleLineItemIdImpl(
                                consumerSaleRegDraftSaleSimpleLineItem.getId()
                        ),
                        new ConsumerSaleSimpleLineItemImpl(
                                new AssetIdImpl(
                                        consumerSaleRegDraftSaleSimpleLineItem
                                                .getAssetId()
                                ),
                                new AssetSerialNumberImpl(
                                        consumerSaleRegDraftSaleSimpleLineItem
                                                .getSerialNumber()
                                ),
                                new ProductLineIdImpl(
                                        consumerSaleRegDraftSaleSimpleLineItem
                                                .getProductLineId()
                                ),
                                        consumerSaleRegDraftSaleSimpleLineItem
                                                .getPrice().orElse(null)
                                ,
                                new ProductLineNameImpl(
                                		consumerSaleRegDraftSaleSimpleLineItem
                                				.getProductLineName()
                                ),
                                consumerSaleRegDraftSaleSimpleLineItem.getProductGroupId()!=null
	           					?new ProductGroupIdImpl(consumerSaleRegDraftSaleSimpleLineItem.getProductGroupId())
	           					:null,
	           					consumerSaleRegDraftSaleSimpleLineItem.getProductGroupName()!= null
	           					?new ProductGroupNameImpl(consumerSaleRegDraftSaleSimpleLineItem.getProductGroupName())
	           					:null
                        )
                );

    }

}
