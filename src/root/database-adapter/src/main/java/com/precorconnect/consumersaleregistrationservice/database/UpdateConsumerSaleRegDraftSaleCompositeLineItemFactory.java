package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateConsumerSaleRegDraftSaleCompositeLineItemFactory {
	
	com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem construct(
            @NonNull ConsumerSaleRegDraftSaleCompositeLineItem partnerSaleRegDraftSaleCompositeLineItem
    );

}
