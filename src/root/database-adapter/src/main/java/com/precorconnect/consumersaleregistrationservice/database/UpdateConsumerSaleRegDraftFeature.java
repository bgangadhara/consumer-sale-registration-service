package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.UpdateConsumerSaleRegDraftReq;

public interface UpdateConsumerSaleRegDraftFeature {
	
	ConsumerPartnerSaleRegDraftView execute(
            @NonNull UpdateConsumerSaleRegDraftReq request,
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId
    );


}
