package com.precorconnect.consumersaleregistrationservice.database;

import javax.persistence.AttributeConverter;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Calendar;

public class InstantPersistenceConverter implements
        AttributeConverter<Instant, Timestamp> {

    public Timestamp convertToDatabaseColumn(
            Instant instant
    ) {
        long actualEpochMilli =
                instant.toEpochMilli();

        // we need to subtract what jdbc automatically adds
        long jdbcConversionCompensatedEpochMilli =
                actualEpochMilli - getCurrentRawOffsetMilli();

        return new Timestamp(
                jdbcConversionCompensatedEpochMilli
        );
    }

    public Instant convertToEntityAttribute(
            Timestamp timestamp) {

        if (null != timestamp) {
            long jdbcConversionCompensatedEpochMilli =
                    timestamp.getTime();

            // we need to add what jdbc automatically subtracts
            long actualEpochMilli =
                    jdbcConversionCompensatedEpochMilli + getCurrentRawOffsetMilli();

            return Instant.ofEpochMilli(actualEpochMilli);
        } else {
            return
                    null;
        }
    }

    private long getCurrentRawOffsetMilli() {
        return Calendar.getInstance().getTimeZone().getRawOffset();
    }
}
