package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateConsumerSaleLineItemComponentFactory {
	
	com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponent construct(
            @NonNull ConsumerSaleLineItemComponent partnerSaleLineItemComponent
    );

}
