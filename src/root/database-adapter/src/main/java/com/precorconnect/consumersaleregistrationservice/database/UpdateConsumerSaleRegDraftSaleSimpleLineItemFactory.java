package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateConsumerSaleRegDraftSaleSimpleLineItemFactory {
	
	com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem construct(
            com.precorconnect.consumersaleregistrationservice.database.
            @NonNull ConsumerSaleRegDraftSaleSimpleLineItem partnerSaleRegDraftSaleSimpleLineItem
    );


}
