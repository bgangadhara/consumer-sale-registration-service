package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.consumersaleregistrationservice.AddSaleLineItemToConsumerSaleRegDraftReq;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemId;
import org.checkerframework.checker.nullness.qual.NonNull;

interface AddSaleLineItemToConsumerSaleRegDraftFeature {

    ConsumerSaleRegDraftSaleLineItemId execute(
            @NonNull AddSaleLineItemToConsumerSaleRegDraftReq request
    );

}
