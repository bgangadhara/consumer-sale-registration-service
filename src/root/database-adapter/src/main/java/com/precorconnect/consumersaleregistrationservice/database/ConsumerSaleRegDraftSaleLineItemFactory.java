package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItem;

interface ConsumerSaleRegDraftSaleLineItemFactory {

    ConsumerSaleRegDraftSaleLineItem construct(
            @NonNull ConsumerSaleLineItem partnerSaleLineItem
    );

    com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem construct(
            @NonNull ConsumerSaleRegDraftSaleLineItem partnerSaleRegDraftSaleLineItem
    );
       

}
