package com.precorconnect.consumersaleregistrationservice.identityservice;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.AppAccessContext;
import com.precorconnect.AuthenticationException;
import org.checkerframework.checker.nullness.qual.NonNull;

interface GetAppAccessContextFeature {

    AppAccessContext execute(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;

}
