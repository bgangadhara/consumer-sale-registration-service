package com.precorconnect.consumersaleregistrationservice.identityservice;

import java.net.MalformedURLException;
import java.net.URL;

public class IdentityServiceAdapterConfigFactoryImpl
        implements IdentityServiceAdapterConfigFactory {

    @Override
    public IdentityServiceAdapterConfig construct() {

        return new IdentityServiceAdapterConfigImpl(
                constructPrecorConnectApiBaseUrl()
        );

    }

    private URL constructPrecorConnectApiBaseUrl() {

        String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");

        try {

            return new URL(precorConnectApiBaseUrl);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }
}
