package com.precorconnect.consumersaleregistrationservice.identityservice;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.AuthenticationException;
import com.precorconnect.PartnerRepAccessContext;
import org.checkerframework.checker.nullness.qual.NonNull;

interface GetPartnerRepAccessContextFeature {

    PartnerRepAccessContext execute(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;

}
