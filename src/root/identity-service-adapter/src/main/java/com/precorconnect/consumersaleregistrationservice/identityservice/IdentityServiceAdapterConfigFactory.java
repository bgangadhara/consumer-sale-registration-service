package com.precorconnect.consumersaleregistrationservice.identityservice;

public interface IdentityServiceAdapterConfigFactory {

	IdentityServiceAdapterConfig construct();

}
