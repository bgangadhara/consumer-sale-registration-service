package com.precorconnect.consumersaleregistrationservice.identityservice;

import com.google.inject.AbstractModule;
import com.precorconnect.identityservice.sdk.IdentityServiceSdk;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkConfig;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkConfigImpl;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkImpl;
import org.checkerframework.checker.nullness.qual.NonNull;

import javax.inject.Singleton;

import static com.precorconnect.guardclauses.Guards.guardThat;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final IdentityServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull IdentityServiceAdapterConfig config
    ) {

    	this.config =
                guardThat(
                        "config",
                         config
                )
                        .isNotNull()
                        .thenGetValue();
    }

    @Override
    protected void configure() {

        bind(IdentityServiceSdkConfig.class)
                .toInstance(
                        new IdentityServiceSdkConfigImpl(
                                config.getPrecorConnectApiBaseUrl()
                        )
                );

        bind(IdentityServiceSdk.class)
                .to(IdentityServiceSdkImpl.class)
                .in(Singleton.class);

        bindFeatures();

    }

    private void bindFeatures() {

        bind(GetAccessContextFeature.class)
                .to(GetAccessContextFeatureImpl.class)
                .in(Singleton.class);

        bind(GetAppAccessContextFeature.class)
                .to(GetAppAccessContextFeatureImpl.class)
                .in(Singleton.class);

        bind(GetPartnerRepAccessContextFeature.class)
                .to(GetPartnerRepAccessContextFeatureImpl.class)
                .in(Singleton.class);
        
        bind(GetEmployeeAccessContextFeature.class)
				.to(GetEmployeeAccessContextFeatureImpl.class)
				.in(Singleton.class);

    }

}
