package com.precorconnect.consumersaleregistrationservice.claimspiffservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdk;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdkConfig;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdkConfigImpl;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdkImpl;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final ClaimSpiffServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull ClaimSpiffServiceAdapterConfig config
    ) {

    	this.config =
                guardThat(
                        "config",
                         config
                )
                        .isNotNull()
                        .thenGetValue();
    }

    @Override
    protected void configure() {

        bind(ClaimSpiffServiceSdkConfig.class)
                .toInstance(
                        new ClaimSpiffServiceSdkConfigImpl(
                                config.getPrecorConnectApiBaseUrl()
                        )
                );

        bind(ClaimSpiffServiceSdk.class)
                .to(ClaimSpiffServiceSdkImpl.class)
                .in(Singleton.class);

        bindFeatures();

    }

    private void bindFeatures() {

        bind(CreateSpiffEntitlementFeature.class)
                .to(CreateSpiffEntitlementFeatureImpl.class)
                .in(Singleton.class);

    }

}
