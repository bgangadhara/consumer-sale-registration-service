package com.precorconnect.consumersaleregistrationservice.claimspiffservice;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.consumersaleregistrationservice.ClaimSpiffServiceAdapter;

@Singleton
public class ClaimSpiffServiceAdapterImpl implements
		ClaimSpiffServiceAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public ClaimSpiffServiceAdapterImpl(
            @NonNull ClaimSpiffServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }

    @Override
    public Collection<Long> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlements,
			@NonNull OAuth2AccessToken accessToken
	) throws AuthenticationException, AuthorizationException {

        return
                injector
                        .getInstance(CreateSpiffEntitlementFeature.class)
                        .createSpiffEntitlements(spiffEntitlements,accessToken);

    }


}
