package com.precorconnect.consumersaleregistrationservice;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.AssetId;
import com.precorconnect.AssetIdImpl;
import com.precorconnect.AssetSerialNumber;
import com.precorconnect.AssetSerialNumberImpl;
import com.precorconnect.CityName;
import com.precorconnect.CityNameImpl;
import com.precorconnect.EmailAddress;
import com.precorconnect.EmailAddressImpl;
import com.precorconnect.FirstName;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.Iso31661Alpha2Code;
import com.precorconnect.Iso31661Alpha2CodeImpl;
import com.precorconnect.Iso31662Code;
import com.precorconnect.Iso31662CodeImpl;
import com.precorconnect.LastName;
import com.precorconnect.LastNameImpl;
import com.precorconnect.PostalCode;
import com.precorconnect.PostalCodeImpl;
import com.precorconnect.ProductGroupId;
import com.precorconnect.ProductGroupIdImpl;
import com.precorconnect.ProductGroupName;
import com.precorconnect.ProductGroupNameImpl;
import com.precorconnect.ProductLineId;
import com.precorconnect.ProductLineIdImpl;
import com.precorconnect.ProductLineName;
import com.precorconnect.ProductLineNameImpl;
import com.precorconnect.SapVendorNumber;
import com.precorconnect.SapVendorNumberImpl;
import com.precorconnect.StreetAddress;
import com.precorconnect.StreetAddressImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;

public class Dummy {
	
	private AccountId accountId =
            new AccountIdImpl("001A0000010mTyjIAE");
	
	private InvoiceUrl  invoiceUrl = new InvoiceUrlImpl(
			"https://s3.amazonaws.com/partner-sale-invoice-service-dev.precorconnect.com/bed1a83a-64fe-477d-8f43-681c412afa10.txt");
	
	private ConsumerSaleRegDraftId consumerSaleRegDraftId = new ConsumerSaleRegDraftIdImpl(Long.valueOf(214));
	
	private final FirstName firstName =
            new FirstNameImpl("Asra");
    
    private final FirstName updFirstName =
            new FirstNameImpl("Asrar");

    private final LastName lastName =
            new LastNameImpl("J");
    
    private final LastName updLastName =
            new LastNameImpl("MA");

    private final EmailAddress emailAddress =
            new EmailAddressImpl("jasra@mailinator.com");
    
    private final UserId userId =
            new UserIdImpl("00u633pyn3pff3Oi90h7");	
    
    private final SapVendorNumber sapVendorNumber =
            new SapVendorNumberImpl("1568711");
    
    private final ConsumerPhoneNumber phoneNumber = 
    		new ConsumerPhoneNumberImpl("123123123");
    
    private final PersonEmail personEmail = 
    		new PersonEmailImpl("bbb@gmail.com");
    
    private final InvoiceNumber invoiceNumber =
    		new InvoiceNumberImpl("123123");
    
    private final IsSubmit isSubmit = 
    		new IsSubmitImpl(false);
    
    private final SellDate sellDate =
    		new SellDateImpl(Instant.now());
    
    private final CreateDate createDate =
    		new CreateDateImpl(Instant.now());
    
    private final BigDecimal price = new BigDecimal(100);
    
    private final Iso31661Alpha2Code iso31661Alpha2Code = new Iso31661Alpha2CodeImpl("US");

    private final Iso31662Code iso31662Code = new Iso31662CodeImpl("WA");
    
    private final StreetAddress streetAddress = new StreetAddressImpl("James Area");
    
    private final AddressLine addressLine = new AddressLineImpl("James Area");
    
    private final CityName cityName = new CityNameImpl("NewJersey");
    
    private final PostalCode postalCode = new PostalCodeImpl("postalCode");
    
    private final ConsumerPostalAddress postalAddress = new ConsumerPostalAddressImpl(streetAddress, addressLine, cityName, iso31662Code, postalCode, iso31661Alpha2Code); 
    
    private final AssetId assetId = new AssetIdImpl("1");
    
    private final AssetSerialNumber assetSerialNumber = new AssetSerialNumberImpl("ANBGB10150109");
    
    private final ProductLineId productLineId = new ProductLineIdImpl(2);
    
    private final ProductLineName productLineName = new ProductLineNameImpl("AMT Display - Commercial");
    
    private final ProductGroupId productGroupId = new ProductGroupIdImpl(3);
    
    private final ProductGroupName productGroupName = new ProductGroupNameImpl("AMT Display - Commercial");
    
    private final SubmittedByName submittedByName = new SubmittedByNameImpl("DummyDealer");
    
    ConsumerPartnerSaleRegDraftView consumerPartnerSaleRegDraftView = 
    		new ConsumerSaleRegDraftViewImpl(
	            new ConsumerSaleRegDraftIdImpl(
	            		1L
	            ),
	            getFirstName(),
	            getLastName(),
	            getPostalAddress(),
	    		getPhoneNumber(),
	    		getPersonEmail(),
	            getAccountId(),
	            buildSaleLineItems(),
	            buildSellDate(),            
	            isSubmitted(),            
	            getInvoiceUrl(),
	            getInvoiceNumber(),
	            buildSaleCreatedTimeStamp(),
	            getUserId(),
	            buildcreateDate()
	    	);
   
    ConsumerPartnerSaleRegDraftView submittedconsumerPartnerSaleRegDraftView = 
    		new ConsumerSaleRegDraftViewImpl(
	            new ConsumerSaleRegDraftIdImpl(325L),
	            new FirstNameImpl("sample"),
	            new LastNameImpl("mytest"),
	            new ConsumerPostalAddressImpl(
	            		new StreetAddressImpl("Jacksonville1"),
	            		new AddressLineImpl("Jacksonville2"),
	            		new CityNameImpl("Florida"),
	            		new Iso31662CodeImpl("WA"),
	            		new PostalCodeImpl("15631456"),
	            		new Iso31661Alpha2CodeImpl("US")
	            	),
	            new ConsumerPhoneNumberImpl("343545612"),
	            new PersonEmailImpl("precor@test.com"),
	            new AccountIdImpl("001A0000010mTyjIAE"),
	            buildSubmitSaleLineItems(),
	            buildSellDate(),
	            new IsSubmitImpl(true),
	            new InvoiceUrlImpl(
	         		   "https://s3.amazonaws.com/partner-sale-invoice-service-dev.precorconnect.com/bed1a83a-64fe-477d-8f43-681c412afa10.txt"
	         		   ),
	            new InvoiceNumberImpl("8564534343"),
	            buildSaleCreatedTimeStamp(),
	            new UserIdImpl("00u612rghqa6Fa98B0h7"),
	            buildcreateDate()
	    	);
    
    ConsumerPartnerSaleRegDraftView savedConsumerPartnerSaleRegDraftView= new ConsumerSaleRegDraftViewImpl(
            new ConsumerSaleRegDraftIdImpl(1L),
            new FirstNameImpl("sample"),
            new LastNameImpl("mytest"),
            new ConsumerPostalAddressImpl(
            		new StreetAddressImpl("Jacksonville"),
            		new AddressLineImpl("streeroad"),
            		new CityNameImpl("Florida"),
            		new Iso31662CodeImpl("WA"),
            		new PostalCodeImpl("15631456"),
            		new Iso31661Alpha2CodeImpl("US")
            	),
            new ConsumerPhoneNumberImpl("343545612"),
            new PersonEmailImpl("precor@test.com"),
            new AccountIdImpl("001A0000010mTyjIAE"),
            buildSubmitSaleLineItems(),
            buildSellDate(),
    		new IsSubmitImpl(true),
            new InvoiceUrlImpl(
         		   "https://s3.amazonaws.com/partner-sale-invoice-service-dev.precorconnect.com/bed1a83a-64fe-477d-8f43-681c412afa10.txt"
         		   ),
            new InvoiceNumberImpl("8564534343"),
            buildSaleCreatedTimeStamp(),
            new UserIdImpl("00u612rghqa6Fa98B0h7"),
            buildcreateDate()
    		);

	private final URI uri;

    /*
    constructors
     */ {
        try {

            uri = new URI("http://dev.precorconnect.com");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }
    }

	public AccountId getAccountId() {
		return accountId;
	}

	public InvoiceUrl getInvoiceUrl() {
		return invoiceUrl;
	}

	public URI getUri() {
		return uri;
	}
     
	public FirstName getFirstName() {
        return firstName;
    }
    
    public FirstName getUpdFirstName() {
        return updFirstName;
    }

    public LastName getLastName() {
        return lastName;
    }
    
    public LastName getUpdLastName() {
        return updLastName;
    }

    public EmailAddress getEmailAddress() {
        return emailAddress;
    }

	public com.precorconnect.UserId getUserId() {
		return userId;
	}

	public SapVendorNumber getSapVendorNumber() {
		return sapVendorNumber;
	}

	public ConsumerSaleRegDraftId getConsumerSaleRegDraftId() {
		return consumerSaleRegDraftId;
	}

	public ConsumerPhoneNumber getPhoneNumber() {
		return phoneNumber;
	}

	public PersonEmail getPersonEmail() {
		return personEmail;
	}

	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

	public IsSubmit isSubmitted() {
		return isSubmit;
	}

	public SellDate getSellDate() {
		return sellDate;
	}

	public ConsumerPostalAddress getPostalAddress() {
		return postalAddress;
	}

	public AssetId getAssetId() {
		return assetId;
	}

	public AssetSerialNumber getAssetSerialNumber() {
		return assetSerialNumber;
	}

	public ProductLineId getProductLineId() {
		return productLineId;
	}

	public ProductLineName getProductLineName() {
		return productLineName;
	}

	public ProductGroupId getProductGroupId() {
		return productGroupId;
	}

	public ProductGroupName getProductGroupName() {
		return productGroupName;
	}

	public CreateDate getCreateDate() {
		return createDate;
	}

	public BigDecimal getPrice() {
		return price;
	}
	
	public SubmittedByName getSubmittedByName() {
		return submittedByName;
	}
	
	public Instant buildSellDate(){
		
		try {
			return new SimpleDateFormat("MM/dd/yyyy").parse(
					"04/12/2016"
			 	).toInstant();
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException("error in parsing sellDate");
		}
	
	}
	
	public Instant buildSaleCreatedTimeStamp(){
			
			try {
				return new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(
							"04/12/2016 18:21:31"
					 	).toInstant();
			} catch (ParseException e) {
				e.printStackTrace();
				throw new RuntimeException("error in parsing salecreate timestamp");
			}
		
		}

	public Instant buildcreateDate(){
		
		try {
			return new SimpleDateFormat("MM/dd/yyyy").parse(
					"04/13/2016"
			 	).toInstant();
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException("error in parsing create Date");
		}
	
	}

	public Collection<ConsumerSaleRegDraftSaleLineItem> buildSaleLineItems(){
	
		Collection<ConsumerSaleRegDraftSaleLineItem> consumerSaleLineItems = new ArrayList<ConsumerSaleRegDraftSaleLineItem>();
		ConsumerSaleSimpleLineItem consumerSaleLineItem = new ConsumerSaleSimpleLineItemImpl(
				getAssetId(), getAssetSerialNumber(), getProductLineId(), 
				BigDecimal.valueOf(100), getProductLineName(), getProductGroupId(), getProductGroupName());
		ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem = 
				new ConsumerSaleRegDraftSaleSimpleLineItemImpl(
						new ConsumerSaleRegDraftSaleLineItemIdImpl(582L),
						consumerSaleLineItem
						);
		Collection<ConsumerSaleLineItemComponent> components=new ArrayList<>();
		ConsumerSaleLineItemComponent firstConsumerSaleLineItemComponent=new ConsumerSaleLineItemComponentImpl(
				 new ConsumerSaleRegDraftSaleLineItemIdImpl(332L),
				 getAssetId(),
				 getAssetSerialNumber(),
				 getProductLineId(),
				 getPrice(),
				 getProductLineName(),
				 getProductGroupId(),
				 getProductGroupName()
			);
		ConsumerSaleLineItemComponent secondConsumerSaleLineItemComponent=new ConsumerSaleLineItemComponentImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(333L),
				 getAssetId(),
				 getAssetSerialNumber(),
				 getProductLineId(),
				 getPrice(),
				 getProductLineName(),
				 getProductGroupId(),
				 getProductGroupName()
			);
		components.add(firstConsumerSaleLineItemComponent);
		components.add(secondConsumerSaleLineItemComponent);
		ConsumerSaleCompositeLineItem firstConsumerSaleCompositeLineItem=new ConsumerSaleCompositeLineItemImpl(components, getPrice());
		
		ConsumerSaleRegDraftSaleCompositeLineItem firstConsumerSaleRegDraftSaleCompositeLineItem=new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(583L),
				firstConsumerSaleCompositeLineItem
		);
		
		
		ConsumerSaleLineItemComponent thirdConsumerSaleLineItemComponent=new ConsumerSaleLineItemComponentImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(334L),
				 getAssetId(),
				 getAssetSerialNumber(),
				 getProductLineId(),
				 getPrice(),
				 getProductLineName(),
				 getProductGroupId(),
				 getProductGroupName()
			);
		ConsumerSaleLineItemComponent fourthConsumerSaleLineItemComponent=new ConsumerSaleLineItemComponentImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(335L),
				 getAssetId(),
				 getAssetSerialNumber(),
				 getProductLineId(),
				 getPrice(),
				 getProductLineName(),
				 getProductGroupId(),
				 getProductGroupName()
			);
		Collection<ConsumerSaleLineItemComponent> consumerComponents=new ArrayList<>();
		consumerComponents.add(thirdConsumerSaleLineItemComponent);
		consumerComponents.add(fourthConsumerSaleLineItemComponent);
		ConsumerSaleCompositeLineItem secondConsumerSaleCompositeLineItem=new ConsumerSaleCompositeLineItemImpl(consumerComponents, getPrice());
		
		ConsumerSaleRegDraftSaleCompositeLineItem secondConsumerSaleRegDraftSaleCompositeLineItem=new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(584L),
				secondConsumerSaleCompositeLineItem
		);
		consumerSaleLineItems.add(consumerSaleRegDraftSaleSimpleLineItem);
		consumerSaleLineItems.add(firstConsumerSaleRegDraftSaleCompositeLineItem);
		consumerSaleLineItems.add(secondConsumerSaleRegDraftSaleCompositeLineItem);
		return consumerSaleLineItems;
	}

	public Collection<ConsumerSaleRegDraftSaleLineItem> buildSubmitSaleLineItems(){
		
		Collection<ConsumerSaleRegDraftSaleLineItem> consumerSaleLineItems = new ArrayList<ConsumerSaleRegDraftSaleLineItem>();
		
		ConsumerSaleSimpleLineItem consumerSaleLineItem = new ConsumerSaleSimpleLineItemImpl(
				getAssetId(),
				getAssetSerialNumber(), 
				getProductLineId(), 
				BigDecimal.valueOf(100.0000).setScale(4, BigDecimal.ROUND_UP), 
				getProductLineName(),
				getProductGroupId(),
				getProductGroupName()
			);
		
		ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem = 
				new ConsumerSaleRegDraftSaleSimpleLineItemImpl(
						new ConsumerSaleRegDraftSaleLineItemIdImpl(582L),
						consumerSaleLineItem
					);
		
		Collection<ConsumerSaleLineItemComponent> components=new ArrayList<>();
		
		ConsumerSaleLineItemComponent firstConsumerSaleLineItemComponent=new ConsumerSaleLineItemComponentImpl(
				 new ConsumerSaleRegDraftSaleLineItemIdImpl(332L),
				 getAssetId(),
				 getAssetSerialNumber(),
				 getProductLineId(),
				 BigDecimal.valueOf(100.0000).setScale(4, BigDecimal.ROUND_UP),
				 getProductLineName(),
				 getProductGroupId(),
				 getProductGroupName()
			);
		
		ConsumerSaleLineItemComponent secondConsumerSaleLineItemComponent=new ConsumerSaleLineItemComponentImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(333L),
				 getAssetId(),
				 getAssetSerialNumber(),
				 getProductLineId(),
				 BigDecimal.valueOf(100.0000).setScale(4, BigDecimal.ROUND_UP),
				 getProductLineName(),
				 getProductGroupId(),
				 getProductGroupName()
			);
		
		components.add(firstConsumerSaleLineItemComponent);
		
		components.add(secondConsumerSaleLineItemComponent);
		
		ConsumerSaleCompositeLineItem firstConsumerSaleCompositeLineItem=new ConsumerSaleCompositeLineItemImpl(
				components, 
				BigDecimal.valueOf(100.0000).setScale(4, BigDecimal.ROUND_UP)
			);
		
		ConsumerSaleRegDraftSaleCompositeLineItem firstConsumerSaleRegDraftSaleCompositeLineItem=
							new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
													new ConsumerSaleRegDraftSaleLineItemIdImpl(583L),
													firstConsumerSaleCompositeLineItem
											);
								
		ConsumerSaleLineItemComponent thirdConsumerSaleLineItemComponent = 
				new ConsumerSaleLineItemComponentImpl(
						new ConsumerSaleRegDraftSaleLineItemIdImpl(334L),
						getAssetId(),
						getAssetSerialNumber(),
						getProductLineId(),
						BigDecimal.valueOf(100.0000).setScale(4, BigDecimal.ROUND_UP),
						getProductLineName(),
						getProductGroupId(),
						getProductGroupName()
						);
		
		ConsumerSaleLineItemComponent fourthConsumerSaleLineItemComponent = 
				new ConsumerSaleLineItemComponentImpl(
						new ConsumerSaleRegDraftSaleLineItemIdImpl(335L),
						getAssetId(),
						getAssetSerialNumber(),
						getProductLineId(),
						BigDecimal.valueOf(100.0000).setScale(4, BigDecimal.ROUND_UP),
						getProductLineName(),
						getProductGroupId(),
						getProductGroupName()
						);
		
		Collection<ConsumerSaleLineItemComponent> consumerComponents = new ArrayList<>();
		
		consumerComponents.add(thirdConsumerSaleLineItemComponent);
		
		consumerComponents.add(fourthConsumerSaleLineItemComponent);
		
		ConsumerSaleCompositeLineItem secondConsumerSaleCompositeLineItem =
				new ConsumerSaleCompositeLineItemImpl(
						consumerComponents,
						BigDecimal.valueOf(100.0000).setScale(4, BigDecimal.ROUND_UP)
						);
		
		ConsumerSaleRegDraftSaleCompositeLineItem secondConsumerSaleRegDraftSaleCompositeLineItem=
				new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
						new ConsumerSaleRegDraftSaleLineItemIdImpl(584L),
						secondConsumerSaleCompositeLineItem
						);
		
		consumerSaleLineItems.add(consumerSaleRegDraftSaleSimpleLineItem);
		
		consumerSaleLineItems.add(firstConsumerSaleRegDraftSaleCompositeLineItem);
		
		consumerSaleLineItems.add(secondConsumerSaleRegDraftSaleCompositeLineItem);
		
		return consumerSaleLineItems;
	
	}


	public ConsumerPartnerSaleRegDraftView getConsumerPartnerSaleRegDraftView() {
		return consumerPartnerSaleRegDraftView;
	}

	public ConsumerPartnerSaleRegDraftView getSubmittedconsumerPartnerSaleRegDraftView() {
		return submittedconsumerPartnerSaleRegDraftView;
	}
	
	public ConsumerPartnerSaleRegDraftView getSavedConsumerPartnerSaleRegDraftView() {
		return savedConsumerPartnerSaleRegDraftView;
	}

}
