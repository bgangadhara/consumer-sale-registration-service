package com.precorconnect.consumersaleregistrationservice;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs {
	
	
	private final Config config = new ConfigFactory().construct();

	private final Dummy dummy = new Dummy();

	private final AccessTokenFactory factory =
	           new AccessTokenFactory(
	           		dummy,
	                   new IdentityServiceIntegrationTestSdkImpl(
	                   			config
	                                     .getIdentityServiceJwtSigningKey()
	                   )
	           );
	
	
	
	private OAuth2AccessToken accessToken;
			
	private AddConsumerSaleRegDraftReq addConsumerSaleRegDraftReq; 
	
	private ConsumerSaleRegDraftId consumerSaleRegDraftId;
	
	private ConsumerPartnerSaleRegDraftView consumerPartnerSaleRegDraftView;
	
	private ConsumerPartnerSaleRegDraftView updatedConsumerPartnerSaleRegDraftView;
	
	private ConsumerPartnerSaleRegDraftView submittedConsumerPartnerSaleRegDraftView;
	
	private UpdateConsumerSaleRegDraftReq updateConsumerSaleRegDraftReq; 
	
	private String deleteSuccessMessage;
	
	private ConsumerSaleRegistrationService objectUnderTest = new ConsumerSaleRegistrationServiceImpl(config.getConsumerSaleRegistrationServiceConfig());
	
	@Given("^AddConsumerSaleRegDraftReq has Attributes$")
	public void addconsumersaleregdraftreq_has_Attributes(DataTable arg1) throws Throwable {
		
		addConsumerSaleRegDraftReq = buildAddConsumerSaleRegDraftReq();
	}

	@Given("^new AddConsumerSaleRegDraftReq object for inserting consumer sale registration details to database$")
	public void new_AddConsumerSaleRegDraftReq_object_for_inserting_consumer_sale_registration_details_to_database() throws Throwable {
	   
	}

	@Given("^I provide an accessToken identifying me as a consumer rep associated with ConsumerSaleRegDrafts\\.partnerAccountId$")
	public void i_provide_an_accessToken_identifying_me_as_a_consumer_rep_associated_with_ConsumerSaleRegDrafts_partnerAccountId() throws Throwable {
		
		accessToken = new OAuth2AccessTokenImpl(
				factory
					.constructValidPartnerRepOAuth2AccessToken(dummy.getAccountId())
					.getValue()
				);
	}

	@When("^I execute AddConsumerSaleRegDraftReq$")
	public void i_execute_AddConsumerSaleRegDraftReq() throws Throwable {
		
		consumerSaleRegDraftId =  objectUnderTest.addConsumerSaleRegDraft(addConsumerSaleRegDraftReq, accessToken);
	}

	@Then("^AddConsumerSaleRegDraftReq record details will be inserted into ConsumerPartnerSaleRegDrafts and ConsumerSaleRegDrafts tables$")
	public void addconsumersaleregdraftreq_record_details_will_be_inserted_into_ConsumerPartnerSaleRegDrafts_and_ConsumerSaleRegDrafts_tables() throws Throwable {
		
		assertNotNull(consumerSaleRegDraftId.getValue());
		
	}
	
	 @When("^I execute getConsumerSaleRegDraft$")
	    public void iExecuteGetConsumerSaleRegDraft(
	    ) throws Throwable {

		 consumerPartnerSaleRegDraftView =
	                objectUnderTest
	                        .getConsumerSaleRegDraftWithId(
	                        		consumerSaleRegDraftId, accessToken
	                        		);
	                        		

	    }
	 
	 @Then("^consumerSaleRegDraft is returned$")
	    public void consumerSaleRegDraftIsReturned(
	    ) throws Throwable {


	        assertThat(consumerPartnerSaleRegDraftView)
	                .isEqualTo(consumerPartnerSaleRegDraftView);

	    }
	 
	 
	 @When("^I execute UpdateConsumerSaleRegDraftReq$")
		public void i_execute_UpdateConsumerSaleRegDraftReq() throws Throwable {
		 
		 updateConsumerSaleRegDraftReq = buildUpdateConsumerSaleRegDraftReq();

		 updatedConsumerPartnerSaleRegDraftView =  objectUnderTest.updateConsumerSaleRegDraft(updateConsumerSaleRegDraftReq,consumerSaleRegDraftId, accessToken);
		}

		@Then("^UpdateConsumerSaleRegDraftReq record details will be updated into ConsumerPartnerSaleRegDrafts and ConsumerSaleRegDrafts tables$")
		public void updateConsumersaleregdraftreq_record_details_will_be_updated_into_ConsumerPartnerSaleRegDrafts_and_ConsumerSaleRegDrafts_tables() throws Throwable {
			ConsumerPartnerSaleRegDraftView expectedUpdateConsumerDraftViewObj=buildExpectedUpdateConsumerDraftViewObj();
			assertNotNull(updatedConsumerPartnerSaleRegDraftView);
			assertEquals(expectedUpdateConsumerDraftViewObj, updatedConsumerPartnerSaleRegDraftView);
			
		}

		@When("^I execute SubmitConsumerSaleRegDraft$")
		public void i_execute_SubmitConsumerSaleRegDraft() throws Throwable {

			submittedConsumerPartnerSaleRegDraftView =  objectUnderTest.submitConsumerSaleRegDraft(consumerSaleRegDraftId,dummy.getSubmittedByName(), accessToken);
		}
		
		@Then("^dbConsumerSaleRegDraft\\.isSubmitted is updated to true$")
		public void dbConsumerSaleRegDraft_isSubmitted_is_updated_to_true() throws Throwable {
			assertNotNull(submittedConsumerPartnerSaleRegDraftView);
			ConsumerPartnerSaleRegDraftView expectedSubmittedConsumerDraftViewObj= buildExpectedSubmittedConsumerDraftViewObj();
			assertEquals(expectedSubmittedConsumerDraftViewObj, submittedConsumerPartnerSaleRegDraftView);
		}
	
		@When("^I execute deleteConsumerSaleRegDraftWithId$")
		public void i_execute_deleteConsumerSaleRegDraftWithId() throws Throwable {
			
			deleteSuccessMessage = objectUnderTest.deleteConsumerSaleRegDraftWithId(consumerSaleRegDraftId, accessToken);
		   
		}

		@Then("^ConsumerSaleRegDrafts\\.Id record will be deleted from ConsumerSaleRegDrafts table$")
		public void consumersaleregdrafts_Id_record_will_be_deleted_from_ConsumerSaleRegDrafts_table() throws Throwable {
			assertEquals(deleteSuccessMessage, ConsumerSaleRegistrationConstants.DELETE_CONSUMER_DRAFT_SUCCESS);
		}
	
	private AddConsumerSaleRegDraftReq buildAddConsumerSaleRegDraftReq() {
		AddConsumerSaleRegDraftReq addConsumerSaleRegDraftReq = new AddConsumerSaleRegDraftReqImpl(
				dummy.getFirstName(), dummy.getLastName(), dummy.getPostalAddress(), dummy.getPhoneNumber(), 
				dummy.getPersonEmail(), dummy.getAccountId(), dummy.getInvoiceNumber(), dummy.getInvoiceUrl(), 
				dummy.isSubmitted(), dummy.getSellDate(), dummy.getUserId(), buildConsumerSaleLineItems());
		return addConsumerSaleRegDraftReq;
	}
	
	private UpdateConsumerSaleRegDraftReq buildUpdateConsumerSaleRegDraftReq() {
		UpdateConsumerSaleRegDraftReq updateConsumerSaleRegDraftReq = new UpdateConsumerSaleRegDraftReqImpl(
				consumerSaleRegDraftId,
				dummy.getFirstName(),
				dummy.getLastName(),
				dummy.getPostalAddress(),
				dummy.getPhoneNumber(), 
				dummy.getPersonEmail(), 
				dummy.getAccountId(),
				dummy.getInvoiceNumber(),
				dummy.getInvoiceUrl(), 
				dummy.isSubmitted(),
				dummy.getSellDate(),
				dummy.getUserId(),
				buildUpdateConsumerSaleLineItems(),
				dummy.getCreateDate()
			);
		return updateConsumerSaleRegDraftReq;
	}
	
	private Collection<ConsumerSaleLineItem> buildUpdateConsumerSaleLineItems(){
		Collection<ConsumerSaleLineItem> consumerSaleLineItems = new ArrayList<ConsumerSaleLineItem>();
		 Collection<ConsumerSaleRegDraftSaleLineItem> consumerSaleRegDraftSaleLineItems=consumerPartnerSaleRegDraftView.getSaleLineItems();
		 for(ConsumerSaleRegDraftSaleLineItem consumerSaleRegDraftSaleLineItem:consumerSaleRegDraftSaleLineItems){
			 
			 if (consumerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleSimpleLineItem) {
				
				 ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem=(ConsumerSaleRegDraftSaleSimpleLineItem) consumerSaleRegDraftSaleLineItem;
				 consumerSaleLineItems.add(consumerSaleRegDraftSaleSimpleLineItem);
			 }
			 else if (consumerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleCompositeLineItem) {
				
				 ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem=(ConsumerSaleRegDraftSaleCompositeLineItem)consumerSaleRegDraftSaleLineItem;
				 consumerSaleLineItems.add(consumerSaleRegDraftSaleCompositeLineItem);
			 }
		 }
		return consumerSaleLineItems;
	}
	
	private Collection<ConsumerSaleLineItem> buildConsumerSaleLineItems() {
		Collection<ConsumerSaleLineItem> consumerSaleLineItems = new ArrayList<ConsumerSaleLineItem>();
		ConsumerSaleSimpleLineItem consumerSaleLineItem = new ConsumerSaleSimpleLineItemImpl(dummy.getAssetId(), dummy.getAssetSerialNumber(), dummy.getProductLineId(), 
				BigDecimal.valueOf(100), dummy.getProductLineName(), dummy.getProductGroupId(), dummy.getProductGroupName());
		ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem = 
				new ConsumerSaleRegDraftSaleSimpleLineItemImpl(new ConsumerSaleRegDraftSaleLineItemIdImpl(null), consumerSaleLineItem);
		Collection<ConsumerSaleLineItemComponent> components=new ArrayList<>();
		ConsumerSaleLineItemComponent firstConsumerSaleLineItemComponent=new ConsumerSaleLineItemComponentImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(null),
				 dummy.getAssetId(),
				 dummy.getAssetSerialNumber(),
				 dummy.getProductLineId(),
				 dummy.getPrice(),
				 dummy.getProductLineName(),
				 dummy.getProductGroupId(),
				 dummy.getProductGroupName()
			);
		ConsumerSaleLineItemComponent secondConsumerSaleLineItemComponent=new ConsumerSaleLineItemComponentImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(null),
				 dummy.getAssetId(),
				 dummy.getAssetSerialNumber(),
				 dummy.getProductLineId(),
				 dummy.getPrice(),
				 dummy.getProductLineName(),
				 dummy.getProductGroupId(),
				 dummy.getProductGroupName()
			);
		components.add(firstConsumerSaleLineItemComponent);
		components.add(secondConsumerSaleLineItemComponent);
		ConsumerSaleCompositeLineItem firstConsumerSaleCompositeLineItem=new ConsumerSaleCompositeLineItemImpl(components, dummy.getPrice());
		
		ConsumerSaleRegDraftSaleCompositeLineItem firstConsumerSaleRegDraftSaleCompositeLineItem=new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(null),
				firstConsumerSaleCompositeLineItem
		);
		
		
		ConsumerSaleLineItemComponent thirdConsumerSaleLineItemComponent=new ConsumerSaleLineItemComponentImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(null),
				 dummy.getAssetId(),
				 dummy.getAssetSerialNumber(),
				 dummy.getProductLineId(),
				 dummy.getPrice(),
				 dummy.getProductLineName(),
				 dummy.getProductGroupId(),
				 dummy.getProductGroupName()
			);
		
		ConsumerSaleLineItemComponent fourthConsumerSaleLineItemComponent=new ConsumerSaleLineItemComponentImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(null),
				 dummy.getAssetId(),
				 dummy.getAssetSerialNumber(),
				 dummy.getProductLineId(),
				 dummy.getPrice(),
				 dummy.getProductLineName(),
				 dummy.getProductGroupId(),
				 dummy.getProductGroupName()
			);
		
		Collection<ConsumerSaleLineItemComponent> consumerComponents=new ArrayList<>();
		consumerComponents.add(thirdConsumerSaleLineItemComponent);
		consumerComponents.add(fourthConsumerSaleLineItemComponent);
		ConsumerSaleCompositeLineItem secondConsumerSaleCompositeLineItem=new ConsumerSaleCompositeLineItemImpl(consumerComponents, dummy.getPrice());
		
		ConsumerSaleRegDraftSaleCompositeLineItem secondConsumerSaleRegDraftSaleCompositeLineItem=new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
				new ConsumerSaleRegDraftSaleLineItemIdImpl(null),
				secondConsumerSaleCompositeLineItem
		);
		consumerSaleLineItems.add(consumerSaleRegDraftSaleSimpleLineItem);
		consumerSaleLineItems.add(firstConsumerSaleRegDraftSaleCompositeLineItem);
		consumerSaleLineItems.add(secondConsumerSaleRegDraftSaleCompositeLineItem);
		return consumerSaleLineItems;
	}
	
	
	private ConsumerPartnerSaleRegDraftView buildExpectedUpdateConsumerDraftViewObj(){
		
		return 
				new ConsumerSaleRegDraftViewImpl(
                		consumerSaleRegDraftId,
                		dummy.getFirstName(),
                		dummy.getLastName(),
                		dummy.getPostalAddress(),
                		dummy.getPhoneNumber(),
                		dummy.getPersonEmail(),
                		dummy.getAccountId(),
                		consumerPartnerSaleRegDraftView.getSaleLineItems(),
                		dummy.getSellDate().getValue(),
                		new IsSubmitImpl(
    		                	false
    		                ),
                	   dummy.getInvoiceUrl(),
 		               dummy.getInvoiceNumber(),
 		               dummy.buildSaleCreatedTimeStamp(),
 		               dummy.getUserId(),
		               dummy.getCreateDate().getValue()
        		);
	}
	
private ConsumerPartnerSaleRegDraftView buildExpectedSubmittedConsumerDraftViewObj(){
		
		return 
				new ConsumerSaleRegDraftViewImpl(
                		consumerSaleRegDraftId,
                		dummy.getFirstName(),
                		dummy.getLastName(),
                		dummy.getPostalAddress(),
                		dummy.getPhoneNumber(),
                		dummy.getPersonEmail(),
                		dummy.getAccountId(),
                		consumerPartnerSaleRegDraftView.getSaleLineItems(),
                		dummy.getSellDate().getValue(),
                		new IsSubmitImpl(
    		                	true
    		                ),
                	   dummy.getInvoiceUrl(),
 		               dummy.getInvoiceNumber(),
 		               dummy.buildSaleCreatedTimeStamp(),
 		               dummy.getUserId(),
		               dummy.getCreateDate().getValue()
        		);
	}


}
