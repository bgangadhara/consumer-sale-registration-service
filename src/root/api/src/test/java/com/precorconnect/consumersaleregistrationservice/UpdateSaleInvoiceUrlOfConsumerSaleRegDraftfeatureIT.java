package com.precorconnect.consumersaleregistrationservice;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(
		features={"features/updateSaleInvoiceUrlOfConsumerSaleRegDraft.feature"},
		glue = {"com.precorconnect.consumersaleregistrationservice.updatesaleinvoiceurlofconsumersaleregdraftfeature"}
)
public class UpdateSaleInvoiceUrlOfConsumerSaleRegDraftfeatureIT {

}
