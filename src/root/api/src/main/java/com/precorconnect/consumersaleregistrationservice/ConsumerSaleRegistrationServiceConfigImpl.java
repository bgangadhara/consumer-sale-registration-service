package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.claimspiffservice.ClaimSpiffServiceAdapterConfig;
import com.precorconnect.consumersaleregistrationservice.database.DatabaseAdapterConfig;
import com.precorconnect.consumersaleregistrationservice.identityservice.IdentityServiceAdapterConfig;
import com.precorconnect.consumersaleregistrationservice.masterdataservice.MasterDataServiceAdapterConfig;
import com.precorconnect.consumersaleregistrationservice.partnerrepservice.PartnerRepServiceAdapterConfig;
import com.precorconnect.partnersaleregdraftservice.registrationlogservice.RegistrationLogServiceAdapterConfig;



public class ConsumerSaleRegistrationServiceConfigImpl
        implements ConsumerSaleRegistrationServiceConfig {

    /*
    fields
     */
    private final IdentityServiceAdapterConfig identityServiceAdapterConfig;

    private final DatabaseAdapterConfig databaseAdapterConfig;

    private final ClaimSpiffServiceAdapterConfig spiffEntitlementServiceAdapterConfig;

    private final MasterDataServiceAdapterConfig masterDataServiceAdapterConfig;
    
    private final RegistrationLogServiceAdapterConfig registrationLogServiceAdapterConfig;
    
    private final PartnerRepServiceAdapterConfig partnerRepServiceAdapterConfig;
    
    /*
    constructors
    */
    public ConsumerSaleRegistrationServiceConfigImpl(
            @NonNull IdentityServiceAdapterConfig identityServiceAdapterConfig,
            @NonNull DatabaseAdapterConfig databaseAdapterConfig,
            @NonNull ClaimSpiffServiceAdapterConfig spiffEntitlementServiceAdapterConfig,
            @NonNull MasterDataServiceAdapterConfig masterDataServiceAdapterConfig,
            @NonNull RegistrationLogServiceAdapterConfig registrationLogServiceAdapterConfig,
            @NonNull PartnerRepServiceAdapterConfig partnerRepServiceAdapterConfig
    ) {

    	this.identityServiceAdapterConfig =
                guardThat(
                        "identityServiceAdapterConfig",
                         identityServiceAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();

    	this.databaseAdapterConfig =
                guardThat(
                        "databaseAdapterConfig",
                        databaseAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffEntitlementServiceAdapterConfig =
                guardThat(
                        "spiffEntitlementServiceAdapterConfig",
                        spiffEntitlementServiceAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();

    	this.masterDataServiceAdapterConfig =
                guardThat(
                        "masterDataServiceAdapterConfig",
                        masterDataServiceAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.registrationLogServiceAdapterConfig =
                guardThat(
                        "registrationLogServiceAdapterConfig",
                        registrationLogServiceAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.partnerRepServiceAdapterConfig =
                guardThat(
                        "partnerRepServiceAdapterConfig",
                        partnerRepServiceAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public IdentityServiceAdapterConfig getIdentityServiceAdapterConfig() {
        return identityServiceAdapterConfig;
    }

    @Override
    public DatabaseAdapterConfig getDatabaseAdapterConfig() {
        return databaseAdapterConfig;
    }

    @Override
    public ClaimSpiffServiceAdapterConfig getSpiffEntitlementServiceAdapterConfig() {
        return spiffEntitlementServiceAdapterConfig;
    }

    @Override
    public MasterDataServiceAdapterConfig getMasterDataServiceAdapterConfig() {
        return masterDataServiceAdapterConfig;
    }

    @Override
    public RegistrationLogServiceAdapterConfig getRegistrationLogServiceAdapterConfig() {
        return registrationLogServiceAdapterConfig;
    }
    
    @Override
	public PartnerRepServiceAdapterConfig getPartnerRepServiceAdapterConfig() {
		return partnerRepServiceAdapterConfig;
	}

    /*
    equality methods
    */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        ConsumerSaleRegistrationServiceConfigImpl that = (ConsumerSaleRegistrationServiceConfigImpl) o;

        if (!identityServiceAdapterConfig.equals(that.identityServiceAdapterConfig)) {
			return false;
		}
        if (!spiffEntitlementServiceAdapterConfig.equals(that.spiffEntitlementServiceAdapterConfig)) {
			return false;
		}
        if (!masterDataServiceAdapterConfig.equals(that.masterDataServiceAdapterConfig)) {
			return false;
		}
        if (!registrationLogServiceAdapterConfig.equals(that.registrationLogServiceAdapterConfig)) {
			return false;
		}
        if (!partnerRepServiceAdapterConfig.equals(that.partnerRepServiceAdapterConfig)) {
			return false;
		}
        return databaseAdapterConfig.equals(that.databaseAdapterConfig);

    }

    @Override
    public int hashCode() {
        int result = identityServiceAdapterConfig.hashCode();
        result = 31 * result + databaseAdapterConfig.hashCode();
        result = 31 * result + spiffEntitlementServiceAdapterConfig.hashCode();
        result = 31 * result + masterDataServiceAdapterConfig.hashCode();
        result = 31 * result + registrationLogServiceAdapterConfig.hashCode();
        result = 31 * result + partnerRepServiceAdapterConfig.hashCode();
        return result;
    }

}
