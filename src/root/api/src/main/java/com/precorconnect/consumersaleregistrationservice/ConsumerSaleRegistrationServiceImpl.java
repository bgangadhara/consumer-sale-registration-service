
package com.precorconnect.consumersaleregistrationservice;

import java.util.Collection;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.claimspiffservice.ClaimSpiffServiceAdapterImpl;
import com.precorconnect.consumersaleregistrationservice.database.DatabaseAdapterImpl;
import com.precorconnect.consumersaleregistrationservice.identityservice.IdentityServiceAdapterImpl;
import com.precorconnect.consumersaleregistrationservice.masterdataservice.MasterDataServiceAdapterImpl;
import com.precorconnect.consumersaleregistrationservice.partnerrepservice.PartnerRepServiceAdapterImpl;
import com.precorconnect.partnersaleregdraftservice.registrationlogservice.RegistrationLogServiceAdapterImpl;

public class ConsumerSaleRegistrationServiceImpl 
				implements ConsumerSaleRegistrationService {
	
	 /*
    fields
     */
    private final Core core;
    
    
    /*
    constructors
     */
	 @Inject
	    public ConsumerSaleRegistrationServiceImpl(
	            @NonNull ConsumerSaleRegistrationServiceConfig config
	    ) {

	        core =
	                new CoreImpl(
	                        new IdentityServiceAdapterImpl(
	                                config.getIdentityServiceAdapterConfig()
	                        ),
	                        new DatabaseAdapterImpl(
	                                config.getDatabaseAdapterConfig()
	                        ),
	                        new ClaimSpiffServiceAdapterImpl(
	                                config.getSpiffEntitlementServiceAdapterConfig()
	                        ),
	                        new MasterDataServiceAdapterImpl(
	                                config.getMasterDataServiceAdapterConfig()
	                        ),
	                        new RegistrationLogServiceAdapterImpl(
	                        		config.getRegistrationLogServiceAdapterConfig()
	                        		),
	                        new PartnerRepServiceAdapterImpl(
	     	                        		config.getPartnerRepServiceAdapterConfig()
	     	                        		) 		
	                );

	    }

	@Override
	public ConsumerSaleRegDraftId addConsumerSaleRegDraft(
			@NonNull AddConsumerSaleRegDraftReq request, 
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		 return
	            core.addConsumerSaleRegDraft(
	                        request,
	                        accessToken
	                );
	
	}


	@Override
	public String deleteConsumerSaleRegDraftWithId(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		return 
				core.deleteConsumerSaleRegDraftWithId(
						consumerSaleRegDraftId,
						accessToken
				);

	}

	@Override
	public ConsumerPartnerSaleRegDraftView getConsumerSaleRegDraftWithId(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException {
		
		return
				core.getConsumerSaleRegDraftWithId(
							consumerSaleRegDraftId,
							accessToken
					);
	
	}

	@Override
	public Collection<ConsumerPartnerSaleRegDraftView> getConsumerSaleRegDraftWithAccountId(
			@NonNull AccountId accountId, 
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		return
				core.getConsumerSaleRegDraftWithAccountId(
						accountId,
						accessToken
				);
	}

	

	@Override
	public void removeSaleLineItemFromConsumerSaleRegDraft(
			@NonNull ConsumerSaleRegDraftSaleLineItemId consumerSaleRegDraftSaleLineItemId,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		core.removeSaleLineItemFromConsumerSaleRegDraft(
				consumerSaleRegDraftSaleLineItemId, 
				accessToken
		);

	}

	@Override
	public ConsumerPartnerSaleRegDraftView submitConsumerSaleRegDraft(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull SubmittedByName submittedByName,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException {
		
		return 
				core.submitConsumerSaleRegDraft(
						consumerSaleRegDraftId, 
						submittedByName,
						accessToken
						);
	}

	@Override
	public ConsumerPartnerSaleRegDraftView updateConsumerSaleRegDraft(
			@NonNull UpdateConsumerSaleRegDraftReq request,
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId, 
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		return 
				core.updateConsumerSaleRegDraft(
						request,
			            consumerSaleRegDraftId,
			            accessToken
			            );
	}

	@Override
	public void updateSaleInvoiceUrlOfConsumerSaleRegDraft(
			@NonNull UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq request, 
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		core
			.updateSaleInvoiceUrlOfConsumerSaleRegDraft(
					request,
					accessToken
					);

	}

	@Override
	public boolean isEligibleForSpiff(
			@NonNull ProductSaleRegistrationRuleEngineRequestDto productSaleRegistrationRuleEngineRequestDto,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {
		
		return
				core
					  .isEligibleForSpiff(
							  productSaleRegistrationRuleEngineRequestDto,
							  accessToken
							);
	}

}

