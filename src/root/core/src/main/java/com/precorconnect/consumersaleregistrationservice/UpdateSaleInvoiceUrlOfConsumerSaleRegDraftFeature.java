package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.OAuth2AccessToken;

interface UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature {

    void execute(
            @NonNull UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq request,
            @NonNull OAuth2AccessToken accessToken
    );

}
