package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

public interface ProductEligibleSpiffReqFactory {
	
	ProductSaleRegistrationRuleEngineWebDto construct(
			@NonNull ProductSaleRegistrationRuleEngineRequestDto productSaleRegistrationRuleEngineRequestDto
		);

}
