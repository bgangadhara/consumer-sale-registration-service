package com.precorconnect.consumersaleregistrationservice;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.OAuth2AccessToken;

public interface DatabaseAdapter {

    ConsumerSaleRegDraftId addConsumerSaleRegDraft(
            @NonNull AddConsumerSaleRegDraftReq request
    );

    ConsumerSaleRegDraftSaleLineItemId addSaleLineItemToConsumerSaleRegDraft(
            @NonNull AddSaleLineItemToConsumerSaleRegDraftReq request
    );

    String deleteConsumerSaleRegDraftWithId(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId
    );

    ConsumerPartnerSaleRegDraftView getConsumerSaleRegDraftWithId(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId
    );

    Collection<ConsumerPartnerSaleRegDraftView> getConsumerSaleRegDraftWithAccountId(
            @NonNull AccountId accountId
    );

    void removeSaleLineItemFromConsumerSaleRegDraft(
            @NonNull ConsumerSaleRegDraftSaleLineItemId consumerSaleRegDraftSaleLineItemId);

    ConsumerPartnerSaleRegDraftView updateConsumerSaleRegDraft(
            @NonNull UpdateConsumerSaleRegDraftReq request,
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId
    );

    ConsumerPartnerSaleRegDraftView submitConsumerSaleRegDraft(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
            @NonNull OAuth2AccessToken accessToken
    );

    void updateSaleInvoiceUrlOfConsumerSaleRegDraft(
            @NonNull UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq request
    );
    

}
