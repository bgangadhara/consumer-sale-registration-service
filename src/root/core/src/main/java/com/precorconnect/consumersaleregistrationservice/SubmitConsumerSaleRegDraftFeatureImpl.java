package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.partnerrepservice.PartnerRepView;
import com.precorconnect.registrationlogservice.webapi.UpdateRegistrationLog;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineResponseWebView;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

@Singleton
class SubmitConsumerSaleRegDraftFeatureImpl
        implements SubmitConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    private final ClaimSpiffServiceAdapter spiffEntitlementServiceAdapter;

    private final MasterDataServiceAdapter masterDataServiceAdapter;
    
    private final PartnerRepServiceAdapter partnerRepServiceAdapter;
    
    private final RegistrationLogServiceAdapter registrationLogServiceAdapter;

    private final CreateSpiffEntitlementsRequestFactory createSpiffEntitlementsRequestFactory;

    private final GetSpiffAmountRequestFactory getSpiffAmountRequestFactory;
    
    private final AddRegistrationLogRequestFactory addRegistrationLogRequestFactory;

    /*
    constructors
     */
    @Inject
    public SubmitConsumerSaleRegDraftFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter,
            @NonNull ClaimSpiffServiceAdapter spiffEntitlementServiceAdapter,
            @NonNull MasterDataServiceAdapter masterDataServiceAdapter,
            @NonNull PartnerRepServiceAdapter partnerRepServiceAdapter,
            @NonNull RegistrationLogServiceAdapter registrationLogServiceAdapter,
            @NonNull CreateSpiffEntitlementsRequestFactory createSpiffEntitlementsRequestFactory,
            @NonNull GetSpiffAmountRequestFactory getSpiffAmountRequestFactory,
            @NonNull AddRegistrationLogRequestFactory addRegistrationLogRequestFactory
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffEntitlementServiceAdapter =
                guardThat(
                        "spiffEntitlementServiceAdapter",
                        spiffEntitlementServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.masterDataServiceAdapter =
                guardThat(
                        "masterDataServiceAdapter",
                        masterDataServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.partnerRepServiceAdapter =
                guardThat(
                        "partnerRepServiceAdapter",
                        partnerRepServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.registrationLogServiceAdapter =
                guardThat(
                        "registrationLogServiceAdapter",
                        registrationLogServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.createSpiffEntitlementsRequestFactory =
                guardThat(
                        "createSpiffEntitlementsRequestFactory",
                        createSpiffEntitlementsRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.getSpiffAmountRequestFactory =
                guardThat(
                        "getSpiffAmountRequestFactory",
                        getSpiffAmountRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.addRegistrationLogRequestFactory =
                guardThat(
                        "addRegistrationLogRequestFactory",
                        addRegistrationLogRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public ConsumerPartnerSaleRegDraftView execute(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
            @NonNull SubmittedByName submittedByName,
            @NonNull OAuth2AccessToken accessToken
    		) throws AuthenticationException, AuthorizationException {
    	
    	boolean flag = false;
    	
    	ConsumerPartnerSaleRegDraftView consumerPartnerSaleRegDraftView
			    	= databaseAdapter
			        			.submitConsumerSaleRegDraft(
			        					consumerSaleRegDraftId,
			        					accessToken
			        					);
    	
    	if(consumerPartnerSaleRegDraftView!=null) {
    		
    		Collection<SpiffSerialExclusionDto> spiffSerialExclusionList = 
    				masterDataServiceAdapter
						.listSpiffSerialExclusion(accessToken);
    		
    		Collection<SerialNumber> serialNumberList=new ArrayList<>();
          	
       	   for(SpiffSerialExclusionDto spiffSerialExclusionDto:spiffSerialExclusionList){
       		     serialNumberList.add(spiffSerialExclusionDto.getSerialNumber());
       	   }
     	  
     	  Collection<ConsumerSaleRegDraftSaleLineItem> consumerSaleRegDraftSaleLineItems=consumerPartnerSaleRegDraftView
     			                                                                                                 .getSaleLineItems();
     	  
     	  for(ConsumerSaleRegDraftSaleLineItem consumerSaleRegDraftSaleLineItem:consumerSaleRegDraftSaleLineItems){
     	    	
     		    if (consumerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleSimpleLineItem) {
     		    	
     		    	ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem= 
     		    												(ConsumerSaleRegDraftSaleSimpleLineItem) consumerSaleRegDraftSaleLineItem;
     		    	
     		    	SerialNumber serialNumber = new SerialNumberImpl(
     		    													consumerSaleRegDraftSaleSimpleLineItem
     		    													.getSerialNumber()
     		    													.getValue()
     		    												);
     		    	
     		    	if(serialNumberList.contains(serialNumber)){
     		    		flag=true;
     		    	}

     		    } else if (consumerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleCompositeLineItem) {
     		    	
     		    	ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem=
     		    												(ConsumerSaleRegDraftSaleCompositeLineItem) consumerSaleRegDraftSaleLineItem;
     		    	
     		    	Collection<ConsumerSaleLineItemComponent> consumerSaleLineItemComponentList=
     		    												(Collection<ConsumerSaleLineItemComponent>) consumerSaleRegDraftSaleCompositeLineItem
     		    																															.getComponents();
     		    	
     		    	for(ConsumerSaleLineItemComponent consumerSaleLineItemComponent:consumerSaleLineItemComponentList){
     		    		
     		    		SerialNumber serialNumber = 
     		    				new SerialNumberImpl(
     		    						consumerSaleLineItemComponent
     		    							.getSerialNumber()
     		    							.getValue()
     		    							);
     		    		
     		    		if(serialNumberList.contains(serialNumber)){
     		    			flag=true;
     		        	}
     		    		
     		    	}

     		    }
     	  }

     	  PartnerRepView partnerRepView = null;
     	 
     	  if(consumerPartnerSaleRegDraftView.getSalePartnerRepId().isPresent()) {
     		 partnerRepView = partnerRepServiceAdapter.getPartnerRepWithId(
     				 consumerPartnerSaleRegDraftView
     				 	.getSalePartnerRepId()
     				 	.get(), 
     				 	accessToken
     				 	);
     	  
     	  }
    	
     	  registrationLogServiceAdapter
    				.addRegistrationLog(
    						addRegistrationLogRequestFactory
    						.construct(
    								consumerPartnerSaleRegDraftView, 
    								partnerRepView,
    								consumerPartnerSaleRegDraftView
									.getSalePartnerRepId().isPresent()?
											consumerPartnerSaleRegDraftView
											.getSalePartnerRepId()
											.get()
											.getValue():null,
											submittedByName
    						),
    						accessToken
    					);
    	    	
    	ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto = 
    			getSpiffAmountRequestFactory
					.construct(consumerPartnerSaleRegDraftView);
    	
		ProductSaleRegistrationRuleEngineResponseWebView productSaleRegistrationRuleEngineResponseWebView
					= masterDataServiceAdapter
				    				.getSpiffAmountForPartnerCommercialSaleRegistration(
				    						productSaleRegistrationRuleEngineWebDto,
				    						accessToken
    		    				);
    	
		if(null != productSaleRegistrationRuleEngineResponseWebView.getAmount()
			&& productSaleRegistrationRuleEngineResponseWebView.getAmount() > 0){

	    	List<SpiffEntitlementDto> entitlements = new ArrayList<SpiffEntitlementDto>();
	    	
	    	Double priceValue=0.0;
	    	
	    	if(!flag)
	    	priceValue=productSaleRegistrationRuleEngineResponseWebView.getAmount();

	    	SpiffEntitlementDto dto =
							    	createSpiffEntitlementsRequestFactory
									    	.construct(
									    			consumerPartnerSaleRegDraftView,
									    			new SpiffAmountImpl(
									    					priceValue
									    					)
									    			);
	    	entitlements.add(dto);

	    	spiffEntitlementServiceAdapter
					                        .createSpiffEntitlements(
					                        		entitlements,
					                        		accessToken
					                        );
	    	if(flag){
	    		
	    		List<UpdateRegistrationLog> updateRegLogList = 
						new ArrayList<UpdateRegistrationLog>();
				
				updateRegLogList
					.add(
							new UpdateRegistrationLog(
									consumerPartnerSaleRegDraftView
									.getId()
									.getValue(),
									"Not Applicable"
								)
						);
				
				registrationLogServiceAdapter
					.updateRegistrationLog(
							updateRegLogList, 
							accessToken
							);
				
	    	}
	    	
		}else {
			
			List<UpdateRegistrationLog> updateRegLogList = 
					new ArrayList<UpdateRegistrationLog>();
			
			updateRegLogList
				.add(
						new UpdateRegistrationLog(
								consumerPartnerSaleRegDraftView
								.getId()
								.getValue(),
								"Not Applicable"
							)
					);
			
			registrationLogServiceAdapter
				.updateRegistrationLog(
						updateRegLogList, 
						accessToken
						);
			}
		
    	}
    	
    	return consumerPartnerSaleRegDraftView;
    }
}
