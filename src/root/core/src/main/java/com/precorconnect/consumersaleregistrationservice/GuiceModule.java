package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;

class GuiceModule extends
        AbstractModule {

    /*
    fields
     */
    private final IdentityServiceAdapter identityServiceAdapter;

    private final DatabaseAdapter databaseAdapter;

    private final ClaimSpiffServiceAdapter spiffEntitlementServiceAdapter;

    private final MasterDataServiceAdapter masterDataServiceAdapter;

    private final RegistrationLogServiceAdapter registrationLogServiceAdapter;
    
    private final PartnerRepServiceAdapter partnerRepServiceAdapter;
    
    /*
    constructors
     */
    public GuiceModule(
            @NonNull IdentityServiceAdapter identityServiceAdapter,
            @NonNull DatabaseAdapter databaseAdapter,
            @NonNull ClaimSpiffServiceAdapter spiffEntitlementServiceAdapter,
            @NonNull MasterDataServiceAdapter masterDataServiceAdapter,
            @NonNull RegistrationLogServiceAdapter registrationLogServiceAdapter,
            @NonNull PartnerRepServiceAdapter partnerRepServiceAdapter
    ) {

    	this.identityServiceAdapter =
                guardThat(
                        "identityServiceAdapter",
                         identityServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffEntitlementServiceAdapter =
                guardThat(
                        "spiffEntitlementServiceAdapter",
                        spiffEntitlementServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.masterDataServiceAdapter =
                guardThat(
                        "masterDataServiceAdapter",
                        masterDataServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.registrationLogServiceAdapter =
                guardThat(
                        "registrationLogServiceAdapter",
                        registrationLogServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.partnerRepServiceAdapter =
                guardThat(
                        "partnerRepServiceAdapter",
                        partnerRepServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindAdapters();

        bindFeatures();

        bindFactories();

    }

    private void bindAdapters() {

        bind(IdentityServiceAdapter.class)
                .toInstance(identityServiceAdapter);

        bind(DatabaseAdapter.class)
                .toInstance(databaseAdapter);

        bind(ClaimSpiffServiceAdapter.class)
        		.toInstance(spiffEntitlementServiceAdapter);

        bind(MasterDataServiceAdapter.class)
				.toInstance(masterDataServiceAdapter);
        
        bind(RegistrationLogServiceAdapter.class)
        		.toInstance(registrationLogServiceAdapter);
        
        bind(PartnerRepServiceAdapter.class)
			.toInstance(partnerRepServiceAdapter);

    }

    private void bindFeatures() {

        bind(AddConsumerSaleRegDraftFeature.class)
                .to(AddConsumerSaleRegDraftFeatureImpl.class);

        bind(AddSaleLineItemToConsumerSaleRegDraftFeature.class)
                .to(AddSaleLineItemToConsumerSaleRegDraftFeatureImpl.class);

        bind(DeleteConsumerSaleRegDraftWithIdFeature.class)
                .to(DeleteConsumerSaleRegDraftWithIdFeatureImpl.class);

        bind(GetConsumerSaleRegDraftWithIdFeature.class)
                .to(GetConsumerSaleRegDraftWithIdFeatureImpl.class);

        bind(GetConsumerSaleRegDraftWithAccountIdFeature.class)
        		.to(GetConsumerSaleRegDraftWithAccountIdFeatureImpl.class);

        bind(RemoveSaleLineItemFromConsumerSaleRegDraftFeature.class)
                .to(RemoveSaleLineItemFromConsumerSaleRegDraftFeatureImpl.class);

        bind(SubmitConsumerSaleRegDraftFeature.class)
                .to(SubmitConsumerSaleRegDraftFeatureImpl.class);

        bind(UpdateConsumerSaleRegDraftFeature.class)
    			.to(UpdateConsumerSaleRegDraftFeatureImpl.class);

        bind(UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature.class)
    			.to(UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeatureImpl.class);
        
        bind(IsEligibleForSpiffFeature.class)
		.to(IsEligibleForSpiffFeatureImpl.class);
        
    }

    private void bindFactories(){

    	bind(CreateSpiffEntitlementsRequestFactory.class)
        	.to(CreateSpiffEntitlementsRequestFactoryImpl.class);

    	bind(GetSpiffAmountRequestFactory.class)
    		.to(GetSpiffAmountRequestFactoryImpl.class);
    	
    	bind(AddRegistrationLogRequestFactory.class)
			.to(AddRegistrationLogRequestFactoryImpl.class);
    	
    	bind(ProductEligibleSpiffReqFactory.class)
		.to(ProductEligibleSpiffReqFactoryImpl.class);

    }
}
