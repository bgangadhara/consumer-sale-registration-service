package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

public interface GetSpiffAmountRequestFactory {


	ProductSaleRegistrationRuleEngineWebDto construct(
  		  @NonNull ConsumerPartnerSaleRegDraftView consumerCommercialSaleRegDraftView
	);
}
