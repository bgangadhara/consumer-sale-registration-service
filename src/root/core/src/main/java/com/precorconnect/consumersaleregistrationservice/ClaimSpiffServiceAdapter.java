package com.precorconnect.consumersaleregistrationservice;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;


public interface ClaimSpiffServiceAdapter {

	Collection<Long> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlements,
			@NonNull OAuth2AccessToken accessToken
	) throws AuthenticationException, AuthorizationException;
}
