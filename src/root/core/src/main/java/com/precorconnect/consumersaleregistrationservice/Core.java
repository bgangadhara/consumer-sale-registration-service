package com.precorconnect.consumersaleregistrationservice;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;


public interface Core {
	
	 ConsumerSaleRegDraftId addConsumerSaleRegDraft(
	            @NonNull AddConsumerSaleRegDraftReq request,
	            @NonNull OAuth2AccessToken accessToken
			 ) throws AuthenticationException;
	 
	 ConsumerSaleRegDraftSaleLineItemId addSaleLineItemToConsumerSaleRegDraft(
	            @NonNull AddSaleLineItemToConsumerSaleRegDraftReq request,
	            @NonNull OAuth2AccessToken accessToken
			 ) throws AuthenticationException;
	 
	 String deleteConsumerSaleRegDraftWithId(
	            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
	            @NonNull OAuth2AccessToken accessToken
			 ) throws AuthenticationException;

	    ConsumerPartnerSaleRegDraftView getConsumerSaleRegDraftWithId(
	            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
	            @NonNull OAuth2AccessToken accessToken
	    		) throws AuthenticationException;

	    Collection<ConsumerPartnerSaleRegDraftView> getConsumerSaleRegDraftWithAccountId(
	            @NonNull AccountId accountId,
	            @NonNull OAuth2AccessToken accessToken
	    		) throws AuthenticationException;

	    Collection<ConsumerSaleRegDraftSynopsisView> listConsumerSaleRegDraftsWithPartnerAccountId(
	            @NonNull AccountId partnerAccountId,
	            @NonNull OAuth2AccessToken accessToken
	    		) throws AuthenticationException;

	    void removeSaleLineItemFromConsumerSaleRegDraft(
	            @NonNull ConsumerSaleRegDraftSaleLineItemId consumerSaleRegDraftSaleLineItemId,
	            @NonNull OAuth2AccessToken accessToken
	    		) throws AuthenticationException;

	    ConsumerPartnerSaleRegDraftView submitConsumerSaleRegDraft(
	            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
	            @NonNull SubmittedByName submittedByName,
	            @NonNull OAuth2AccessToken accessToken
	    		) throws AuthenticationException, AuthorizationException;

	    ConsumerPartnerSaleRegDraftView updateConsumerSaleRegDraft(
	            @NonNull UpdateConsumerSaleRegDraftReq request,
	            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
	            @NonNull OAuth2AccessToken accessToken
	    		) throws AuthenticationException;

	    void updateSaleInvoiceUrlOfConsumerSaleRegDraft(
	            @NonNull UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq request,
	            @NonNull OAuth2AccessToken accessToken
	    		) throws AuthenticationException;
	    
	    boolean isEligibleForSpiff(
	    		@NonNull ProductSaleRegistrationRuleEngineRequestDto productSaleRegistrationRuleEngineRequestDto,
	    		@NonNull OAuth2AccessToken accessToken
	    		)throws AuthenticationException, AuthorizationException;

}
