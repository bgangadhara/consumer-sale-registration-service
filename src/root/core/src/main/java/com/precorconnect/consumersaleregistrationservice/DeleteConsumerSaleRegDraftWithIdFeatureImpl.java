package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.OAuth2AccessToken;
import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;

class DeleteConsumerSaleRegDraftWithIdFeatureImpl
        implements DeleteConsumerSaleRegDraftWithIdFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public DeleteConsumerSaleRegDraftWithIdFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public String execute(
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId,
            @NonNull OAuth2AccessToken accessToken
    ) {

      return  databaseAdapter
                .deleteConsumerSaleRegDraftWithId(
                        partnerSaleRegDraftId
                );

    }
}
