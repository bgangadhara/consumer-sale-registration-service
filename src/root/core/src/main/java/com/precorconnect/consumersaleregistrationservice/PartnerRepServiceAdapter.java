package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.partnerrepservice.PartnerRepView;

public interface PartnerRepServiceAdapter {
	
	PartnerRepView getPartnerRepWithId(
            @NonNull UserId id,
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException, AuthorizationException;

}
