package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.partnerrepservice.PartnerRepView;
import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;

public interface AddRegistrationLogRequestFactory {

	AddRegistrationLog construct(
			@NonNull ConsumerSaleRegDraftView consumerSaleRegDraftView,
			@Nullable PartnerRepView partnerRepView,
			@Nullable String partnerRepId,
			@NonNull SubmittedByName submittedByName
			);
}
