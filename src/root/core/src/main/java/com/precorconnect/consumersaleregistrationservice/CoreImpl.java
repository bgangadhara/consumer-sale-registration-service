
package com.precorconnect.consumersaleregistrationservice;

import java.util.Collection;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;


public class CoreImpl implements Core {
	
	/*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public CoreImpl(
            @NonNull IdentityServiceAdapter identityServiceAdapter,
            @NonNull DatabaseAdapter databaseAdapter,
            @NonNull ClaimSpiffServiceAdapter spiffEntitlementServiceAdapter,
            @NonNull MasterDataServiceAdapter masterDataServiceAdapter,
            @NonNull RegistrationLogServiceAdapter registrationLogServiceAdapter,
            @NonNull PartnerRepServiceAdapter partnerRepServiceAdapter
    ) {

        GuiceModule guiceModule =
                new GuiceModule(
                        identityServiceAdapter,
                        databaseAdapter,
                        spiffEntitlementServiceAdapter,
                        masterDataServiceAdapter,
                        registrationLogServiceAdapter,
                        partnerRepServiceAdapter
                );

        injector = Guice.createInjector(guiceModule);

    }

	@Override
	public ConsumerSaleRegDraftId addConsumerSaleRegDraft(
			@NonNull AddConsumerSaleRegDraftReq request,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
					
		
		injector
			.getInstance(IdentityServiceAdapter.class)
			.getAccessContext(accessToken);

		return
		        injector
		                .getInstance(AddConsumerSaleRegDraftFeature.class)
		                .execute(
		                        request,
		                        accessToken
		                );
	
	}

	@Override
	public ConsumerSaleRegDraftSaleLineItemId addSaleLineItemToConsumerSaleRegDraft(
			@NonNull AddSaleLineItemToConsumerSaleRegDraftReq request, 
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
					
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteConsumerSaleRegDraftWithId(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		injector
			.getInstance(IdentityServiceAdapter.class)
				.getAccessContext(accessToken);
		
		return
				injector
					.getInstance(DeleteConsumerSaleRegDraftWithIdFeature.class)
		            .execute(
		            		consumerSaleRegDraftId,
		                    accessToken
		            );

	}

	@Override
	public ConsumerPartnerSaleRegDraftView getConsumerSaleRegDraftWithId(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId, 
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		injector
			.getInstance(IdentityServiceAdapter.class)
			.getAccessContext(accessToken);
	
	return
			injector
            .getInstance(GetConsumerSaleRegDraftWithIdFeature.class)
            .execute(
            		consumerSaleRegDraftId,
                    accessToken
            );
	
	}

	@Override
	public Collection<ConsumerPartnerSaleRegDraftView> getConsumerSaleRegDraftWithAccountId(
			@NonNull AccountId accountId, 
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		return
				injector
	            .getInstance(GetConsumerSaleRegDraftWithAccountIdFeature.class)
	            .execute(
	            		accountId,
	                    accessToken
	            		);
	
	}

	@Override
	public Collection<ConsumerSaleRegDraftSynopsisView> listConsumerSaleRegDraftsWithPartnerAccountId(
			@NonNull AccountId partnerAccountId, 
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
					
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeSaleLineItemFromConsumerSaleRegDraft(
			@NonNull ConsumerSaleRegDraftSaleLineItemId consumerSaleRegDraftSaleLineItemId,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		injector
        	.getInstance(RemoveSaleLineItemFromConsumerSaleRegDraftFeature.class)
        	.execute(
        			consumerSaleRegDraftSaleLineItemId,
        			accessToken
        			);

	}

	@Override
	public ConsumerPartnerSaleRegDraftView submitConsumerSaleRegDraft(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull SubmittedByName submittedByName,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException {
					
		injector
			.getInstance(IdentityServiceAdapter.class)
			.getAccessContext(accessToken);
	
	return
			injector
            .getInstance(SubmitConsumerSaleRegDraftFeature.class)
            .execute(
            		consumerSaleRegDraftId,
            		submittedByName,
                    accessToken
            		);
	
	}

	@Override
	public ConsumerPartnerSaleRegDraftView updateConsumerSaleRegDraft(
			@NonNull UpdateConsumerSaleRegDraftReq request,
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
	injector
			.getInstance(IdentityServiceAdapter.class)
			.getAccessContext(accessToken);

	return
	        injector
	                .getInstance(UpdateConsumerSaleRegDraftFeature.class)
	                .execute(
	                        request,
	                        consumerSaleRegDraftId,
	                        accessToken
	                );
	
	}

	@Override
	public void updateSaleInvoiceUrlOfConsumerSaleRegDraft(
			@NonNull UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq request, 
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException  {
		
		injector
			.getInstance(IdentityServiceAdapter.class)
			.getAccessContext(accessToken);
		
		
		injector
		        .getInstance(UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature.class)
		        .execute(
		        		request,
		                accessToken
		        );

	}

	@Override
	public boolean isEligibleForSpiff(
			@NonNull ProductSaleRegistrationRuleEngineRequestDto productSaleRegistrationRuleEngineRequestDto,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException {
		
		return 
				injector
				.getInstance(IsEligibleForSpiffFeature.class)
				.execute(productSaleRegistrationRuleEngineRequestDto, accessToken);
	
	}

}