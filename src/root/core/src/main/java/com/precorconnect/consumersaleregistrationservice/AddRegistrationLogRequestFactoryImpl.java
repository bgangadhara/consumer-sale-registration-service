package com.precorconnect.consumersaleregistrationservice;

import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.partnerrepservice.PartnerRepView;
import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;

@Singleton
public class AddRegistrationLogRequestFactoryImpl 
				implements AddRegistrationLogRequestFactory {

	@Override
	public AddRegistrationLog construct(
			@NonNull ConsumerSaleRegDraftView consumerSaleRegDraftView,
			@Nullable PartnerRepView partnerRepView,
			@Nullable String partnerRepId,
			@NonNull SubmittedByName submittedByName
			) {
		
		Long partnerSaleRegistrationId = 
				consumerSaleRegDraftView
							.getId().getValue();
			
		String accountId = 
				consumerSaleRegDraftView.getPartnerAccountId().getValue();
						
		String accountName = 
				consumerSaleRegDraftView.getFirstName().getValue()
				+" "+ consumerSaleRegDraftView.getLastName().getValue();
		
		String installDate = null;
		
		String sellDate =
				 new SimpleDateFormat("MM/dd/yyyy").format(
						 Date.from(consumerSaleRegDraftView
						 .getSellDate())
				 	);
		
		String submittedDate =
				 new SimpleDateFormat("MM/dd/yyyy").format(
						 Date.from(consumerSaleRegDraftView
						 .getSaleCreatedAtTimestamp())
				 	);
		
		String extendedWarrantyStatus =
						"Not Applicable";
		
		String firstName = null, lastName = null, email = null;
		
		if(partnerRepView!=null)  {		
		 firstName = partnerRepView.getFirstName()==null? null:partnerRepView.getFirstName().getValue();
		
		 lastName = partnerRepView.getLastName()==null? null:partnerRepView.getLastName().getValue();
		
		 email = partnerRepView.getEmailAddress()==null? null:partnerRepView.getEmailAddress().getValue();
		
		 partnerRepId = partnerRepView.getId()==null? null:partnerRepView.getId().getValue();
		 
		}
		
		return 
				new AddRegistrationLog(
						partnerSaleRegistrationId, 
						accountId, 
						accountName, 
						sellDate,
						installDate,
						submittedDate,
						extendedWarrantyStatus,
						firstName,
						lastName,
						email,
						partnerRepId,
						submittedByName.getValue()
						);

	}
}
