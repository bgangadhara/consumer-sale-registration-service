package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;

interface SubmitConsumerSaleRegDraftFeature {

	ConsumerPartnerSaleRegDraftView execute(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
            @NonNull SubmittedByName submittedByName,
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException, AuthorizationException;

}
