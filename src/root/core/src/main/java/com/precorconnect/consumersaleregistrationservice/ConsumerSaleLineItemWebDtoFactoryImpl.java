package com.precorconnect.consumersaleregistrationservice;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleCompositeLineItemWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleSimpleLineItemWebDto;

public class ConsumerSaleLineItemWebDtoFactoryImpl
	implements ConsumerSaleLineItemWebDtoFactory {

	@Override
	public PartnerSaleSimpleLineItemWebDto construct(
			com.precorconnect.consumersaleregistrationservice.
			@NonNull ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem
	) {

		PartnerSaleSimpleLineItemWebDto partnerSaleLineItemWebDto
				= new PartnerSaleSimpleLineItemWebDto();

		partnerSaleLineItemWebDto
				.setSerialNumber(
						consumerSaleRegDraftSaleSimpleLineItem
							.getSerialNumber()
							.getValue().substring(0, 4)
						);

		return partnerSaleLineItemWebDto;
	}

	@Override
	public PartnerSaleCompositeLineItemWebDto construct(
			com.precorconnect.consumersaleregistrationservice.
			@NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
	) {

		PartnerSaleCompositeLineItemWebDto partnerSaleCompositeLineItemWebDto
		= new PartnerSaleCompositeLineItemWebDto();

		partnerSaleCompositeLineItemWebDto.setComponents(StreamSupport
			.stream(
					consumerSaleRegDraftSaleCompositeLineItem
							.getComponents()
			                .spliterator(),
			        false)
			.map(component -> constructWebDto(component))
			.collect(Collectors.toList()));
	return
			partnerSaleCompositeLineItemWebDto;

	}

	private PartnerSaleSimpleLineItemWebDto constructWebDto(
			com.precorconnect.consumersaleregistrationservice.
			@NonNull ConsumerSaleLineItemComponent partnerSaleLineItemComponent
	){

		PartnerSaleSimpleLineItemWebDto partnerSaleLineItemWebDto
				= new PartnerSaleSimpleLineItemWebDto();

		partnerSaleLineItemWebDto
				.setSerialNumber(
						partnerSaleLineItemComponent
							.getSerialNumber()
							.getValue().substring(0,  4)
						);

		return partnerSaleLineItemWebDto;

	}
}
