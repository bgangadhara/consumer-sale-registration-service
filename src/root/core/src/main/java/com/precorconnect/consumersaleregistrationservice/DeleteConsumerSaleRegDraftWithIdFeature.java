package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.OAuth2AccessToken;
import org.checkerframework.checker.nullness.qual.NonNull;

interface DeleteConsumerSaleRegDraftWithIdFeature {

	String execute(
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId,
            @NonNull OAuth2AccessToken accessToken
    );

}
