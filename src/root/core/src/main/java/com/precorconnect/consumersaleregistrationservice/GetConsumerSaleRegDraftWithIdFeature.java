package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.OAuth2AccessToken;
import org.checkerframework.checker.nullness.qual.NonNull;

interface GetConsumerSaleRegDraftWithIdFeature {

    ConsumerPartnerSaleRegDraftView execute(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
            @NonNull OAuth2AccessToken accessToken
    );

}
